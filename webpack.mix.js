const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts('resources/js/lib/jquery.min.js','public/js/jquery.min.js')
        .copy('resources/js/lib/bootstrap.min.js','public/js/bootstrap.min.js')
        .copy('resources/js/lib/flickr.js','public/js/flickr.js')
        .copy('resources/js/lib/flexslider.min.js', 'public/js/flexslider.min.js')
        .copy('resources/js/lib/lightbox.min.js','public/js/lightbox.min.js')
        .copy('resources/js/lib/masonry.min.js','public/js/masonry.min.js')
        .copy('resources/js/lib/twitterfetcher.min.js','public/js/twitterfetcher.min.js')
        .copy('resources/js/lib/countdown.min.js','public/js/countdown.min.js')
        .copy('resources/js/lib/parallax.js','public/js/parallax.js')
        .copy('resources/js/lib/smooth-scroll.min.js','public/js/smooth-scroll.min.js')
        .copy('resources/js/lib/spectragram.min.js','public/js/spectragram.min.js')
        .copy('resources/js/lib/ytplayer.min.js', 'public/js/ytplayer.min.js')
        .js('resources/js/app.js', 'public/js/scripts.js') 
        .copy('resources/css/bootstrap.css', 'public/css/bootstrap.css')
        .copy('resources/css/et-line-icons.css', 'public/css/et-line-icons.css')
        .copy('resources/css/flexslider.css', 'public/css/flexslider.css')
        .copy('resources/css/lightbox.min.css', 'public/css/lightbox.min.css')
        .copy('resources/css/ytplayer.css', 'public/css/ytplayer.css')
        .copy('resources/css/font-awesome.min.css', 'public/css/font-awesome.min.css')
        .copy('resources/css/font-dosis.css', 'public/css/font-dosis.css')
        .copy('resources/css/font-georgia.css', 'public/css/font-georgia.css')
        .copy('resources/css/font-montserrat.css', 'public/css/font-montserrat.css')
        .copy('resources/css/font-opensans.css', 'public/css/font-opensans.css')
        .copy('resources/css/font-oswald.css', 'public/css/font-oswald.css')
        .copy('resources/css/font-pathway.css', 'public/css/font-pathway.css')
        .copy('resources/css/font-poppins.css', 'public/css/font-poppins.css')
        .copy('resources/css/font-roboto.css', 'public/css/font-roboto.css')
        .copy('resources/css/font-robotocondensed.css', 'public/css/font-robotocondensed.css')
        .copy('resources/css/font-titillium.css', 'public/css/font-titillium.css')
        .copy('resources/css/pe-icon-7-stroke.css', 'public/css/pe-icon-7-stroke.css')
        .copy('resources/css/theme-chipotle.css', 'public/css/theme-chipotle.css')
        .copy('resources/css/theme-fire.css', 'public/css/theme-fire.css')
        .copy('resources/css/theme-gunmetal.css', 'public/css/theme-gunmetal.css')
        .copy('resources/css/theme-nature.css', 'public/css/theme-nature.css')
        .copy('resources/css/theme-navy.css', 'public/css/theme-navy.css')
        .copy('resources/css/theme-hyperblue.css', 'public/css/theme-hyperblue.css')
        .copy('resources/css/theme-nearblack.css', 'public/css/theme-nearblack.css')
        .copy('resources/css/theme-offyellow.css', 'public/css/theme-offyellow.css')
        .copy('resources/css/theme-orange.css', 'public/css/theme-orange.css')
        .copy('resources/css/theme-purple.css', 'public/css/theme-purple.css')
        .copy('resources/css/theme-red.css', 'public/css/theme-red.css')
        .copy('resources/css/theme-rose.css', 'public/css/theme-rose.css')
        .copy('resources/css/theme-startup.css', 'public/css/theme-startup.css')
        .copy('resources/css/themify-icons.css', 'public/css/themify-icons.css')
        .copy('resources/css/theme.css', 'public/css/theme.css')
        .sass('resources/sass/app.scss', 'public/css/custom.css');
