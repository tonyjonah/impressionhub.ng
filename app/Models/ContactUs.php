<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ContactUs extends Model
{
    use Notifiable;

    /**
     * Table name
     */
    protected $table = 'contact_us';

    /**
     * Fillable fields
     */
    protected $fillable = [
        'email',
        'name',
        'message',
        'contacted'
    ];

    /**
     * Hidden fields
     */
    protected $hidden = [];

    public $timestamps = false;

    /**
     * Route notifications for the mail channel. (overloaded)
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return array|string
     */
    public function routeNotificationForMail($notification)
    {
        return "info@impressionhub.ng";
    }

}
