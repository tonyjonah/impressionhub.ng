<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $keyType = 'string';

    public $incrementing = false;

    // public $timestamps = false;

    public function subscription()
    {
        if ($this->where('service', 0)->first() != null) {
            return $this->hasOne(Subscription::class, 'payment_reference');
        }
        else return null;
    }

    public function schedule()
    {
        if ($this->where('service', 1)->first() != null) {
            return $this->hasOne(RoomSchedule::class, 'payment_reference');
        }
        else return null;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
