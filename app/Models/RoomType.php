<?php

namespace App\Models;

use App\Models\RoomOption;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    use HasFactory;

    /**
     * 
     */
    protected $table = 'room_type';

    public function rooms()
    {
        return $this->hasMany(RoomOption::class, 'room_type');
    }
}
