<?php

namespace App\Models;

use App\Models\RoomOption;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoomSchedule extends Model
{
    use HasFactory;

    protected $table = 'room_schedule';

    public function roomOption()
    {
        return $this->belongsTo(RoomOption::class, 'room_option');
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class, 'payment_reference');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'convener');
    }
}
