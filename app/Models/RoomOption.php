<?php

namespace App\Models;

use App\Models\RoomType;
use Illuminate\Database\Eloquent\Model;

class RoomOption extends Model
{
    protected $table = 'rooms';

    public function roomType()
    {
        return $this->belongsTo(RoomType::class);
    }
}
