<?php

namespace App\Models;

use App\Models\SubscriptionPlan;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $guarded = [];

    public function subscription_plan()
    {
        return $this->belongsTo(SubscriptionPlan::class);
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class, 'payment_reference');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
