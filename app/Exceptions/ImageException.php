<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Log;

class ImageException extends Exception
{
//   public function __construct($message = null, $code = 0) 
//   {
//         parent::__construct($message, $code);
//         Log::error('Error in '.$this->getFile().
//         ' Line: '.$this->getLine().
//         ' Error: '.$this->getMessage()
//         );
//   }

    /**
     * Report the exception.
     *
     * @return bool|null
     */
    public function report()
    {
        Log::error('Error in ' . $this->getFile() . ' Line: '.$this->getLine() . ' Error: ' . $this->getMessage());
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render()
    {

    }
}
class ImageFileException extends ImageException {}
class ImageFileNotSupportedException extends ImageException {}