<?php

namespace App\Listeners;

use App\Events\FirstTimeCustomer;
use App\Jobs\ResizeProfilePhoto;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserProfilePhotoCheckListener
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(FirstTimeCustomer $event)
    {
        $photoPath = $event->user->photo;
        $locationPrefix = __DIR__ . '/../../storage/app/public/profile_photos/';
        $imgFile = $locationPrefix . $photoPath;
        Log::info('Sending profile photo for ' . $event->user->email . ' at ' . $photoPath . ' to be resized.');
        dispatch((new ResizeProfilePhoto($imgFile))->onQueue('high'));
    }
}
