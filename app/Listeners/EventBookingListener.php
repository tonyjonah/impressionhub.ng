<?php

namespace App\Listeners;

use App\Events\EventBooked;
use Illuminate\Support\Facades\Log;
use App\Notifications\NotifyConvenerOnSuccess;
use Illuminate\Support\Facades\Notification as Notifier;
use App\Notifications\RoomBookingInitiated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class EventBookingListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventBooked  $event
     * @return void
     */
    public function handle(EventBooked $event)
    {
        Log::info($event->user->fname . " " . $event->user->lname . " just paid " . $event->schedule->payment->amount . " for a scheduled meeting / event");

        $event->user->notify(new NotifyConvenerOnSuccess($event->schedule));

        Notifier::route('mail', 'receptionist@impressionhub.ng')
             ->notify(new RoomBookingInitiated($event->user, $event->schedule));
    }
}
