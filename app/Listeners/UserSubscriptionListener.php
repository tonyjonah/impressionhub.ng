<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;
use App\Events\UserSubscribed;
use App\Notifications\NotifySubscriberOnSuccess;
use App\Notifications\SubscriptionInitiated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Notification as Notifier;
use Illuminate\Queue\InteractsWithQueue;

class UserSubscriptionListener implements ShouldQueue
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserSubscribed  $event
     * @return void
     */
    public function handle(UserSubscribed $event)
    {
        Log::info($event->user->fname . " " . $event->user->lname . " just paid " . $event->subscription->payment->amount . " for a subscription!" );

        $event->user->notify(new NotifySubscriberOnSuccess($event->subscription));

        Notifier::route('mail', 'receptionist@impressionhub.ng')
            ->notify(new SubscriptionInitiated($event->user, $event->subscription));
    }
}
