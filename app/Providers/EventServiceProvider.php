<?php

namespace App\Providers;

use App\Events\EventBooked;
use App\Events\UserSubscribed;
use App\Events\FirstTimeCustomer;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use App\Listeners\EventBookingListener;
use App\Listeners\UserSubscriptionListener;
use App\Listeners\UserProfilePhotoCheckListener;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        FirstTimeCustomer::class => [
            UserProfilePhotoCheckListener::class,
        ],
        EventBooked::class => [
            EventBookingListener::class,
        ],
        UserSubscribed::class => [
            UserSubscriptionListener::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
