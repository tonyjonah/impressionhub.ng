<?php

use Illuminate\Support\Facades\Log;

if (! function_exists('exception_render')) {
    /**
     * Log information regarding error
     * @param \Throwable $th
     * @param string $class
     * @return void
     */
    function exception_render(Throwable $th, $class)
    {
        $msg  = $th->getMessage();
        $code = $th->getCode();
        $file = $th->getFile();
        $line = $th->getLine();
        
        Log::error("An Error occured in Class " . $class);
        Log::error("Details: " . $code . ' @ line ' . $line . ' of file ' . $file . ' spewed up ' . $msg );
    }
}