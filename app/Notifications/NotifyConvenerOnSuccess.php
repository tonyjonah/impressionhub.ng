<?php

namespace App\Notifications;

use Carbon\Carbon;
use App\Models\RoomSchedule;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NotifyConvenerOnSuccess extends Notification implements ShouldQueue
{
    use Queueable;

    public $roomSchedule;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(RoomSchedule $roomSchedule)
    {
        $this->roomSchedule = $roomSchedule;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $money = new \NumberFormatter('en_NG', \NumberFormatter::CURRENCY);
        $dt = Carbon::createFromFormat('Y-m-d H:i:s.u', $this->roomSchedule->begin );
        ($dt->dayName == 'Saturday') ? $startTime = '10:00:00.0' : $startTime = '9:00:00.0';

        return (new MailMessage)
                    ->subject('New Room Booking Alert')
                    ->greeting('Hello ' . $notifiable->fname . ',')
                    ->line('Please find your schedule details below')
                    ->line('Room Option : ' . ucwords(str_replace('_', ' ', $this->roomSchedule->roomOption->room_option_name)))
                    ->line('Cost : ' . $money->format(($this->roomSchedule->payment->amount / 100)))
                    ->line('Starts : ' . Carbon::createFromFormat('Y-m-d H:i:s.u', $this->roomSchedule->begin))
                    ->line('Ends : ' . Carbon::createFromFormat('Y-m-d H:i:s.u', $this->roomSchedule->end))
                    ->action('Check Details', url('/user/profile'))
                    ->line('Thanks for your trust and patronage.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
