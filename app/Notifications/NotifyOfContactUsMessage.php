<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NotifyOfContactUsMessage extends Notification
{
    use Queueable;

    /**
     * 
     */
    public $contact_us;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($contact_us)
    {
        $this->contact_us = $contact_us;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->from('postmaster@mg.impressionhub.ng')
                    ->subject('Messge from ' . $this->contact_us->name)
                    ->line($this->contact_us->name . " sent the messages below at " . $this->contact_us->contacted)
                    ->line($this->contact_us->message)
                    ->line($this->contact_us->name . ' can be reached at ' . $this->contact_us->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
