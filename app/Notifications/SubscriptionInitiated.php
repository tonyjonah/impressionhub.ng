<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SubscriptionInitiated extends Notification
{
    use Queueable;

    private $customer;
    private $subscription;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($customer, $subscription)
    {
        $this->customer = $customer;
        $this->subscription = $subscription;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $money = new \NumberFormatter('en_NG', \NumberFormatter::CURRENCY);
        $dt = Carbon::createFromFormat('Y-m-d H:i:s.u', $this->subscription->begin . " 00:00:00.0");
        if ($dt->dayName == 'Saturday') {
            $startTime = '10:00:00.0'; 
            $endTime = '17:00:00.0';
         } else {
             $startTime = '9:00:00.0'; 
             $endTime = '21:00:00.0';
         }

        return (new MailMessage)
                    ->bcc('info@impressionhub.ng')
                    ->subject('New Subscription Alert')
                    ->greeting('Hello Receptionist' . ",")
                    ->line('Great news, the customer named ' . $this->customer->fname . ' ' . $this->customer->lname . ' with email ' . $this->customer->email . '" Subscription was created successfully.')
                    ->line('Here are the details below')
                    ->line('Cost : ' . $money->format($this->subscription->payment->amount / 100))
                    ->line('Starts : ' . Carbon::createFromFormat('Y-m-d H:i:s.u', $this->subscription->begin . " " . $startTime))
                    ->line('Ends : ' . Carbon::createFromFormat('Y-m-d H:i:s.u', $this->subscription->end . " " . $endTime))
                    ->line('Please take note');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
