<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RoomBookingInitiated extends Notification
{
    use Queueable;

    private $customer;
    private $booking;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($customer, $booking)
    {
        $this->customer = $customer;
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $money = new \NumberFormatter('en_NG', \NumberFormatter::CURRENCY);
        $dt = Carbon::createFromFormat('Y-m-d H:i:s.u', $this->booking->begin );
        ($dt->dayName == 'Saturday') ? $startTime = '10:00:00.0' : $startTime = '9:00:00.0';

        return (new MailMessage)
                    ->bcc('info@impressionhub.ng')
                    ->subject('New Room Booking Alert')
                    ->greeting('Hello Receptionist' . ',')
                    ->line('Please find your schedule details below')
                    ->line('Booked by ' . $this->customer->fname . ' ' . $this->customer->lname . ' with email ' . $this->customer->email)
                    ->line('Room Option : ' . ucwords(str_replace('_', ' ', $this->booking->roomOption->room_option_name)))
                    ->line('Cost : ' . $money->format(($this->booking->payment->amount / 100)))
                    ->line('Starts : ' . Carbon::createFromFormat('Y-m-d H:i:s.u', $this->booking->begin))
                    ->line('Ends : ' . Carbon::createFromFormat('Y-m-d H:i:s.u', $this->booking->end))
                    ->line('Please take note.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
