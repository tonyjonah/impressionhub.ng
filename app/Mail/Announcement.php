<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Announcement extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * File name for the announcement
     */
    private $announcement_file;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($annoucement)
    {
        $this->announcement_file = $annoucement;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Announcement')
                ->markdown('emails.notice.' . $this->announcement_file);
    }
}
