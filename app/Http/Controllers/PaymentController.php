<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Payment;
use App\Events\EventBooked;
use Illuminate\Http\Request;
use App\Events\UserSubscribed;

class PaymentController extends Controller
{
    /**
     * Payment gateway instance
     */
    protected $gatewayObj;

    /**
     * Payment model
     */
    protected $paymentRef;

    /**
     * Payment creation
     * @param array $data
     */
    public function createPayment(Array $data)
    {
        $this->paymentRef = new Payment;

        $this->paymentRef->id      = $data['payment_reference'];
        $this->paymentRef->user_id = $data['user_id'];
        $this->paymentRef->service = $data['service'];
        $this->paymentRef->amount  = $data['amount'];

        $this->paymentRef->save();

        return;
    }

    /**
     * Redirect payment request to Payment Gateway
     * @param Illuminate\Http\Request
     */
    public function redirectToGateway(Request $request)
    {
        $this->gatewayObj = new PaystackController();

        try {
            if(request()->ajax()) {
                return response()->json([
                    'SUCCESS' => 1,
                    'LOCATION' => $this->gatewayObj->getAuthorizationUrl()->url
                ], 200);
            }
            return $this->gatewayObj->getAuthorizationUrl()->redirectNow();
        } catch (\Throwable $th) {
            exception_render($th, __CLASS__);

            return redirect()
                    ->back()
                    ->with(['message' => 'An issue occurred. Please try again.'])
                    ->withInput();
        }
    }

    /**
     * Receive feedback from Payment Gateway
     * @return Illuminate\Http\RedirectResponse
     */
    public function handleGatewayCallback()
    {
        $this->gatewayObj = new PaystackController();

        $paymentDetails = $this->gatewayObj->getPaymentData();

        if ($paymentDetails['data']['status'] == 'success') {
            $this->paymentRef = Payment::where('id', $paymentDetails['data']['reference'])->first();

            // Confirm what service this is for 
            // 0 => space subscriptions or
            // 1 => for event schedules
            if ($this->paymentRef->service == 0) {
                return $this->confirmSubscriptionPayment($paymentDetails);
            } elseif ($this->paymentRef->service == 1) {
                return $this->confirmScheduledRoomPayment($paymentDetails);
            } else {
                
            }

        } else {

            if ($this->paymentRef->service == 0) {
                $messg = '<h5>Payment Failed</h5><p> Please try again</p>';
                return redirect()->back()->with('notice', $messg);
            }
        }
    }

    /**
     * 
     */
    public function handleGatewayEventNotification()
    {

    }

    /**
     * Calls Paystack generate transaction reference method
     * @return string
     */
    static function genTranxRef()
    {
        return PaystackController::genTranxRef();
    }

    /**
     * Process confirmation for space subscription
     * @param Array $paymentDetails
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function confirmSubscriptionPayment($paymentDetails)
    {
        if ($this->paymentRef->subscription != null) {
            $this->paymentRef->payment_successful = 1;
            $this->paymentRef->save();

            $this->paymentRef->subscription->payment_confirmed = 1;
            $this->paymentRef->subscription->save();

            event(new UserSubscribed($this->paymentRef->user, $this->paymentRef->subscription));

            return redirect()->to('/subscription/confirmation/' . $paymentDetails['data']['reference']);
        } else {
            return redirect()
                ->back()
                ->with(['message' => 'An issue occurred. Please try again.']);
        }
    }

    /**
     * Process confirmation for Room booked
     * @param Array $paymentDetails
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function confirmScheduledRoomPayment($paymentDetails)
    {
        if ($this->paymentRef->schedule != null) {
            $this->paymentRef->payment_successful = 1;
            $this->paymentRef->save();

            $this->paymentRef->schedule->payment_confirmed = 1;
            $this->paymentRef->schedule->save();

            event(new EventBooked($this->paymentRef->user, $this->paymentRef->schedule));

            return redirect()->to('/room/confirmation/' . $paymentDetails['data']['reference']);
        } else {
            return redirect()
                ->back()
                ->with(['message' => 'An issue occurred. Please try again.']);
        }
    }
}
