<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\PaymentController;
use Illuminate\Support\Facades\Validator;
use App\Events\FirstTimeCustomer;
use App\Models\SubscriptionPlan;
use Illuminate\Http\Request;
use App\Models\Subscription;
use App\Models\Payment;
use App\Models\User;
use Carbon\Carbon;

class SubscriptionController extends Controller
{
    /**
     * PaymentController instance
     */
    protected $paymentObj;

    /**
     * Subscription model instance
     */
    protected $subscription;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Int $subscription)
    {
        $option = SubscriptionPlan::findOrFail($subscription);
        return view('subscriptions.subscribe', ['option' => $option]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(auth()->user()) {
            $rules = [
                'plan'              => 'required|numeric',
                'reference'         => 'required|string',
                'currency'          => 'required|string',
                'schedule_start'    => 'required|date',
                'schedule_end'      => 'nullable|date',
                'amount'            => 'required|numeric',
            ];

            $validator = Validator::make(request()->all(), $rules);
            $this->checkForValidationFailure($validator);

        } else {
            $rules = [
                'plan'              => 'required|numeric',
                'reference'         => 'required|string',
                'fname'             => 'required|string',
                'lname'             => 'required|string',
                'email'             => 'required|email',
                'phone'             => 'required|digits_between:5,13|starts_with:0,234',
                'gender'            => 'required|numeric',
                'currency'          => 'required|string',
                'schedule_start'    => 'required|date',
                'schedule_end'      => 'nullable|date',
                'amount'            => 'required|numeric',
                'photo'             => 'required|image|max:2048'
            ];

            $validator = Validator::make(request()->all(), $rules);
            $this->checkForValidationFailure($validator);
        }

        // Confirm if user is returning user or new user
        if(auth()->user()) {
            // if returning user then take user's credentials
            // and create subscription for user

            // Check to confirm that schedules don't overlap
            if ($overlapInstances = $this->checkForScheduleOverlap(request()->schedule_start, request()->schedule_end)) {
                $messg = '<h5>Your chosen schedule overlaps with one or more of your previous subscription schedules</h5> <br/>';
                $tz = 'Africa/Lagos';
                $messg .= $overlapInstances->reduce(function($carry, $instance) use($tz) {
                    $start  = Carbon::createFromFormat('Y-m-d H', $instance->begin . '00', $tz);
                    $end    = Carbon::createFromFormat('Y-m-d H', $instance->end   . '00', $tz);

                    $carry .= '<p>Between <strong>' . $start->toFormattedDateString() . "</strong> and <strong>" . $end->toFormattedDateString() . '</strong> </p>';
                    $carry .= '<p>Please pick a start date that does not overlap with an existing schedule. </p>';
                    
                    return $carry;
                });

                if (request()->ajax()) {
                    return response($messg, 400);
                }
                
                return redirect()->back()->with('notice', $messg);
            };

            $this->createPaymentReference(auth()->id());
            $this->createSubscription($request, auth()->id());
            return $this->paymentObj->redirectToGateway($request);

        } elseif (User::where('email', request('email'))->first() != null) {

            $messg = "<h5>There's a problem with your email</h5>";
            $messg .= "<p>That email account already exists in our system. If you own that email, please refresh and login to schedule for the use of the resource or use another email account.</p>";


            if (request()->ajax()) {
                return response($messg, 400);
            }

            return redirect()->back()->with('notice', $messg);

        } else {
            $register = new RegisterController;
            $user_id = $register->registerSubscriber(request());
            
            $this->createPaymentReference($user_id);
            $this->createSubscription($request, $user_id);
            return $this->paymentObj->redirectToGateway($request);
        }
    }

    /**
     * Check for validation failures
     */
    private function checkForValidationFailure($validator)
    {
        if ($validator->fails()) {
            if (request()->ajax()) {
                return response()->json([
                    'validation' => $validator->errors()
                ], 422);
            }
            return back()
                ->withErrors($validator)
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Create the subscription entry for the user
     * @param Illuminate\Http\Request $request
     * @param int $user_id
     * @return App\Http\Controllers\SubscriptionController
     */
    protected function createSubscription(Request $request, $user_id)
    {
        // Create Subscription Object
        $this->subscription = new Subscription;
        $this->subscription->user_id = auth()->id() ?? $user_id;
        $this->subscription->subscription_plan_id = request()->plan;
        $this->subscription->begin = request()->schedule_start;

        if (SubscriptionPlan::find(request()->plan)->tenure > 1) {
            $this->subscription->end = request()->schedule_end;
        } else $this->subscription->end = request()->schedule_start;
        
        $this->subscription->payment_reference = request()->reference;

        $this->subscription->save();

        return $this;
    }

    /**
     * Creates payment reference for the subscription
     * @return App\Http\Controllers\SubscriptionController
     */
    protected function createPaymentReference(Int $id)
    {
        // Create Payment Object but don't confirm payment
        $this->paymentObj = new PaymentController;
        $this->paymentObj->createPayment([
            'payment_reference' => request()->reference,
            'user_id'           => $id,
            'service'           => 0,
            'amount'            => request()->amount
        ]);

        return $this;
    }

    /**
     * Directs the action the application should
     * take upon the successful subscription 
     * made by a user (new or old)
     * @return Illuminate\Http\Redirect| Illuminate\Http\View
     */
    public function subscriptionConfirmed(string $paymentId)
    {
        $payment = Payment::where('id', $paymentId)->firstOrFail();
        $paymentConfirmedAt = $payment->updated_at;

        $currentTimeStamp = Carbon::now();
        $comparedTimestamp = Carbon::now()->addMinutes(5);

        if ($currentTimeStamp->betweenIncluded($paymentConfirmedAt, $comparedTimestamp)) {
            // If user is new, log user in and redirect user to confirmation page
            if ((auth()->user()->subscription->count() == 1) && (auth()->user()->events->count() == 0)) {
                event(new FirstTimeCustomer(request()->user()));
                return view(
                    'confirmation',
                    ['context' => 'subscription']
                );
            } else {
                // If it's an old user redirect the user to the list of subscriptions and meeting schedules
                return redirect()->route('serviceListing');
            }
        } else {
            return redirect()->route('serviceListing');
        }
    }

    /**
     * Checks to see if schedule overlaps exist
     * @param string $start
     * @param string $end
     * @return mixed
     */
    protected function checkForScheduleOverlap($start, $end = null)
    {
        $tz = "Africa/Lagos";
        $startDateArr                   = explode('-', $start);
        $endDateArr                     = ($end != null) ? explode('-', $end) : explode('-', $start);

        $newSubscriptionStartDate       = Carbon::now();
        $newSubscriptionStartDate->year = $startDateArr[0];
        $newSubscriptionStartDate->month= $startDateArr[1];
        $newSubscriptionStartDate->day  = $startDateArr[2];
        $newSubscriptionEndDate         = Carbon::now();
        $newSubscriptionEndDate->year   = $endDateArr[0];
        $newSubscriptionEndDate->month  = $endDateArr[1];
        $newSubscriptionEndDate->day    = $endDateArr[2];

        $scheduleConflict               = Subscription::where('user_id', auth()->id())
                                            ->where('payment_confirmed', 1)
                                            ->get()
                                            ->filter(function($subscription) use( $newSubscriptionStartDate, $newSubscriptionEndDate, $tz) {
            $checkBegin = Carbon::createFromFormat('Y-m-d H', $subscription->begin . "00", $tz);
            $checkEnd   = Carbon::createFromFormat('Y-m-d H', $subscription->end . "00", $tz);
            return  $checkBegin->between($newSubscriptionStartDate, $newSubscriptionEndDate) ||
                    $checkEnd->between($newSubscriptionStartDate, $newSubscriptionEndDate) ||
                    $newSubscriptionStartDate->between($checkBegin, $checkEnd) ||
                    $newSubscriptionEndDate->between($checkBegin, $checkEnd);
        });

        if($scheduleConflict->count() > 0) {
            return $scheduleConflict;
        } else {
            return false;
        }
    }
}
