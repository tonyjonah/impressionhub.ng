<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\PaymentController;
use App\Events\FirstTimeCustomer;
use Illuminate\Http\Request;
use App\Models\RoomSchedule;
use App\Models\RoomOption;
use App\Models\Payment;
use App\Models\User;
use Carbon\Carbon;

class RoomScheduleController extends Controller
{
    /**
     * PaymentController instance
     */
    protected $paymentObj;

    /**
     * RoomSchedule model instance
     */
    protected $schedule;

    public function index()
    {
    }

    public function create()
    {
    }

    /**
     * Stores an event's schedule in the database.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth()->user()) {
            request()->validate([
                'room_option'       => 'required|numeric',
                'unitCost'          => 'required|numeric',
                'reference'         => 'required|string',
                'amount'            => 'required|numeric',
                'date'              => 'required|date',
                'scheduleStart'     => 'required|string',
                'scheduleEnd'       => 'required|string',
            ]);
        } else {
            request()->validate([
                'room_option'       => 'required|numeric',
                'unitCost'          => 'required|numeric',
                'reference'         => 'required|string',
                'amount'            => 'required|numeric',
                'date'              => 'required|date',
                'scheduleStart'     => 'required|string',
                'scheduleEnd'       => 'required|string',
                'fname'             => 'required|string',
                'lname'             => 'required|string',
                'email'             => 'required|email',
                'phone'             => 'required|digits_between:5,13|starts_with:0,234',
                'gender'            => 'required|numeric',
                'currency'          => 'required|string',
                'photo'             => 'required|image|max:2048'
            ]);
        }

        // Confirm if user is returning user or new user
        if(auth()->user()) {
            // if returning user then take user's credentials
            // and create subscription for

            $this->createPaymentReference(auth()->id());
            $this->createSchedule(request(), auth()->id());
            return $this->paymentObj->redirectToGateway($request);

        } elseif(User::where('email', request('email'))->first() != null) {

            $messg = "<h5>There's a problem with your email</h5>";
            $messg .= "<p>That email account already exists in our system. If you own that email, please refresh and login to schedule for the use of the resource or use another email account.</p>";

            if (request()->ajax()) {
                return response($messg, 400);
            }

            return redirect()->back()->with('notice', $messg);

        } else {
            // If the user is new,
            // create an account for the new user
            $register   = new RegisterController;
            $user_id    = $register->registerScheduler($request);

            // fire another event for notification on account creation

            $this->createPaymentReference($user_id);
            $this->createSchedule($request, $user_id);
            return $this->paymentObj->redirectToGateWay($request);
        }
    }

    /**
     * Returns the schedule for the given
     * room
     * @param App\Models\RoomOption $option
     * @param String $data
     * @return Illuminate\Responses
     */
    public function show(RoomOption $option, String $date)
    {
        $date_array     = str_split($date, 2);
        $year           = $date_array[0] . $date_array[1];
        $month          = $date_array[2];
        $day            = $date_array[3];
        $dateFormated   = $year . '-' . $month . '-' . $day;

        $scheduleQuery  = RoomSchedule::whereDate('begin', $dateFormated)
            ->whereTime('begin', '<=', '23:59:59')
            ->whereTime('begin', '>=', '00:00:00')
            ->where('payment_confirmed', 1)
            ->where(function ($query) use ($option) {
                $query->where('room_option', $option->id )
                    ->orWhere('room_option', $option->option_pairing);
            })->get();

        $schedule_end       = $scheduleQuery->pluck('end');
        $schedule_begin     = $scheduleQuery->pluck('begin');

        if ($scheduleQuery != null) {
            return response()->json([
                'BEGIN' => $schedule_begin->toArray(),
                'END'   => $schedule_end->toArray(),
            ], 200);
        } else {
            return response()->json([
                'FAILURE'   => 0
            ], 500);
        }
    }

    public function edit()
    {
    }

    public function update()
    {
    }

    public function destroy()
    {
    }

    /**
     * Creates payment reference for the scheduled event
     * @return App\Http\Controllers\RoomScheduleController
     */
    protected function createPaymentReference(Int $id)
    {
        // Create Payment Object but don't confirm payment
        $this->paymentObj = new PaymentController;
        $this->paymentObj->createPayment([
            'payment_reference' => request()->reference,
            'user_id'           => $id,
            'service'           => 1,
            'amount'            => request()->amount
        ]);

        return $this;
    }

    /**
     * Create the schedule entry for the user
     * @param Illuminate\Http\Request $request
     * @param int $user_id
     * @return App\Models\RoomSchedule
     */
    protected function createSchedule(Request $request, $user_id)
    {
        $tz = 'Africa/Lagos';
        // Create schedule Object
        $this->schedule                     = new RoomSchedule();
        $this->schedule->convener           = auth()->id() ?? $user_id;
        $this->schedule->room_option        = request()->room_option;
        $this->schedule->begin              = Carbon::createFromFormat('Y-m-d H:i:s', request()->date . request()->scheduleStart, $tz);
        $this->schedule->end                = Carbon::createFromFormat('Y-m-d H:i:s', request()->date . request()->scheduleEnd, $tz);  
        $this->schedule->payment_reference  = request()->reference;
        $this->schedule->save();

        return $this;
    }

    /**
     * Directs the action the application should
     * take upon the successful event schedule 
     * made by a user (new or old)
     * @return \Illuminate\Http\Response|\Illuminate\Http\Response
     */
    public function bookingConfirmed(string $paymentId)
    {
        $payment = Payment::where('id', $paymentId)->firstOrFail();
        $paymentConfirmedAt = $payment->updated_at;

        $currentTimeStamp = Carbon::now();
        $comparedTimestamp = Carbon::now()->addMinutes(5);

        if ($currentTimeStamp->betweenIncluded($paymentConfirmedAt, $comparedTimestamp)) {
            // If user is new, log user in and redirect user to confirmation page
            if ((auth()->user()->events->count() == 1) && (auth()->user()->subscription->count() == null)) {
                event(new FirstTimeCustomer(request()->user()));
                return view('confirmation',
                    ['context' => 'schedule']
                );
            } else {
                // If it's an old user redirect the user to the list of subscriptions and meeting schedules
                return redirect()->route('serviceListing');
            }
        } else {
            return redirect()->route('serviceListing');
        }
    }
}
