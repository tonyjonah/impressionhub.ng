<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class LegalController extends Controller
{
    /**
     * Call up the latest Terms of Agreement for view
     */
    public function getAgreement() 
    {
        return View::make('legal.01_Jan_2022')->render();
    }

}
