<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Get the account information for the account
     * @return \Illuminate\Http\Response
     */
    public function accountInfo()
    {
        $user = request()->user();

        $subscriptions = $user->subscription->where('payment_confirmed', 1);
        $roomBookings  = $user->events->where('payment_confirmed', 1);

        return view('account', compact('subscriptions', 'roomBookings'));
    }

    public function showForm()
    {

    }

    public function profile()
    {

    }

    public function updateProfile()
    {
        
    }

    /**
     * Gets details for the completion of the users
     * profile in the database
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function completeProfile(Request $request)
    {
        request()->validate([
            'birth_date'    => 'required|date',
            'profession'    => 'string|sometimes',
            'company'       => 'required|string',
            'addr_line1'    => 'required|string',
            'addr_line2'    => 'nullable|string',
            'town'          => 'required|string',
            // 'profession_other'  => 'string|sometimes',
            // 'state'             => 'required|string',
        ]);
        
        $user               = request()->user();

        $user->dob          = $request->birth_date;
        $user->profession   = $request->profession ?? (ctype_alpha($request->profession_other) ? $request->profession_other : '');
        $user->company      = $request->company;
        $user->addr_line1   = $request->addr_line1;
        $user->addr_line2   = $request->addr_line2;
        $user->town         = $request->town;
        $user->state        = "Lagos";

        $user->save();

        // redirect to Account Page
        return redirect()->route('serviceListing');
    }
}
