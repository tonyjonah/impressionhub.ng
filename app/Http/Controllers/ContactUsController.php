<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Notifications\NotifyOfContactUsMessage;
use App\Models\ContactUs;
// use App\Events\ContactMessageCreated;

class ContactUsController extends Controller
{
    public function store(Request $request)
    {
        // Save the message in the db
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'name' => 'required|string',
            'message' => 'required|string',
        ]);

        if($validator->fails()) {
            
            if($request->ajax()){
                return response()->json(0, 400);
            }

            return redirect()
                    ->withErrors($validator)
                    ->withInput();
        }

        $contact_obj = new ContactUs;
        $contact_obj->email = $request->email;
        $contact_obj->name = $request->name;
        $contact_obj->message = $request->message;
        $contact_obj->contacted = now();

        $contact_obj->save();

        $contact_obj->notify(new NotifyOfContactUsMessage($contact_obj));
        Log::notice("New Contact Us Message was sent from " . $contact_obj->email);

        return response()->json(1, 200);
    }
}
