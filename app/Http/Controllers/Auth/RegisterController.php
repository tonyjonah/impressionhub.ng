<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Validator;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use \Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\User;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }     
    
    /**
     * Show the application registration form. (overloaded)
     *
     * @return \Illuminate\View\View
     */
    public function showRegistrationForm()
    {
        return view('register');
    }

    /**
     * Handle a registration request for the application. (overloaded)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    // public function register(Request $request)
    // {
    //     $this->validator($request->all())->validate();
    //     event(new Registered($user = $this->create($request->all())));

    //     $this->guard()->login($user);

    //     if ($response = $this->registered($request, $user)) {
    //         return $response;
    //     }

    //     return $request->wantsJson()
    //                 ? new JsonResponse([], 201)
    //                 : redirect($this->redirectPath());

    // }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'photo' => ['required', 'image', 'max:2048'],
            'password' => ['required_with:password_confirmation', Password::defaults(), 'confirmed'],
            'phone' => ['required', 'digits_between:5,13']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  \Illuminate\Http\Request
     * @return \App\Models\User
     */
    protected function create(Request $request)
    {
        return User::create([
            'fname' => $request->fname,
            'lname' => $request->lname,
            'email' => $request->email,
            'phone' => $request->phone,
            'photo' => explode('/', $request->photo->store('public/profile_photos'))[2],
            'password' => Hash::make($request->password,),
        ]);
    }

    /**
     * Handle a registration request for subscribers.
     * @param  \Illuminate\Http\Request  $request
     * @return Int
     */
    public function registerSubscriber(Request $request)
    {
        return $this->registerUser($request);
    }

    /**
     * Register event's scheduler as user
     * @param \Illuminate\Http\Request $request
     * @return Int
     */
    public function registerScheduler(Request $request) 
    {
        return $this->registerUser($request);
    }

    /**
     * Register Hub customer
     * @param \Illuminate\Http\Request $request
     * @return int $id|\Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse       
     */
    protected function registerUser(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = $this->create(request());

        event(new Registered($user));

        $this->guard()->login($user);

        if ($response = $this->registered($request, $user)) {
            return $response;
        }

        return $user->id;
    }
}
