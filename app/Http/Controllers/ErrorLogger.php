<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ErrorLogger extends Controller
{
    public function getFromBrowser(Request $request)
    {
        $this->info = request()->msg;
        $this->url  = request()->url;
        $this->lineNo = request()->lineNo;
        $this->colNo = request()->columNo;
        $this->error = request()->error;

        Log::error("Context : Browser ; url : " . $this->url . " ; Message : " . $this->info . " ; JavaScript Line Number : " . $this->lineNo);
        return response()->json("SUCCESS", 200);
    }
}
