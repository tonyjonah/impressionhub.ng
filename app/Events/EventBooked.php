<?php

namespace App\Events;

use App\Models\User;
use App\Models\RoomSchedule;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class EventBooked
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The schedule booked for
     * 
     * @var \App\Models\RoomSchedule
     */
    public $schedule;

    /**
     * The user for the the schedule
     * 
     * @var \App\Models\User
     */
    public $user;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, RoomSchedule $schedule)
    {
        $this->user = $user;
        $this->schedule = $schedule;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
