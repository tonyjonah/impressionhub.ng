<?php

namespace App\Utilities;

use App\Exceptions\ImageException;
use App\Exceptions\ImageFileException;
use App\Exceptions\ImageFileNotSupportedException;

class ImageResizer 
{
    private $maxWidth;
    private $maxHeight;
    private $scale;
    private $inflate;
    private $types;
    private $imgLoaders;
    private $imgCreators;
    private $source;
    private $sourceWidth;
    private $sourceHeight;
    private $sourceMime;
    private $resized;
    private $resizedWidth;
    private $resizedHeight;

    public function __construct($maxWidth = 500, $maxHeight = 500, $scale = true, $inflate = true)
    {
        $this->maxWidth = $maxWidth;
        $this->maxHeight = $maxHeight;
        $this->scale = $scale;
        $this->inflate = $inflate;

        $this->types = [
            'image/jpeg',
            'image/png',
            'image/webp'
        ];

        $this->imgLoaders = [
            'image/jpeg' => 'imagecreatefromjpeg',
            'image/png'  => 'imagecreatefrompng',
            'image/webp' => 'imagecreatefromwebp'
        ];

        $this->imgCreators = [
            'image/jpeg' => 'imagejpeg',
            'image/png'  => 'imagepng',
            'image/webp' => 'imagewebp'
        ];
    }

    public function loadFile($image)
    {
        if (!$dims = @getimagesize($image)) {
            throw new ImageException('Could not find image ' . $image);
        }

        if (in_array($dims['mime'], $this->types)) {
            $loader = $this->imgLoaders[$dims['mime']];
            $this->source = $loader($image);
            $this->sourceWidth = $dims[0];
            $this->sourceHeight = $dims[1];
            $this->sourceMime = $dims['mime'];
            $this->initResize();
            return true;
        } else {
            throw new ImageFileNotSupportedException('Image MIME type ' . $dims['mime'] . ' not supported');
        }
    }

    public function loadData($image, $mime)
    {
        if (in_array($mime, $this->types)) {
            if ($this->source = @imagecreatefromstring($image)) {
                $this->sourceWidth = imagesx($this->source);
                $this->sourceHeight = imagesy($this->source);
                $this->sourceMime = $mime;
                $this->initResize();
                return true;
            } else {
                throw new ImageFileException('Could not load image from string');
            }
        } else {
            throw new ImageFileNotSupportedException('Image MIME type ' . $mime . ' not supported');
        }
    }

    public function buildImage($file = null)
    {
        // $creator = $this->imgCreators[$this->sourceMime];
        // Default all images to JPEG
        $creator = $this->imgCreators['image/jpeg'];
        if (isset($file)) {
            return $creator($this->resized, $file);
        } else {
            return $creator($this->resized);
        }
    }

    public function getMime()
    {
        return $this->sourceMime;
    }

    public function getWidth()
    {
        return $this->resizedWidth;
    }

    public function getHeight()
    {
        return $this->resizedHeight;
    }

    private function initResize()
    {
        if ($this->scale) {
            if ($this->sourceWidth > $this->sourceHeight) {
                $this->resizedWidth = $this->maxWidth;
                $this->resizedHeight = floor($this->sourceHeight * ($this->maxWidth / $this->sourceWidth));
            } else if ($this->sourceWidth < $this->sourceHeight) {
                $this->resizedHeight = $this->maxHeight;
                $this->resizedWidth = floor($this->sourceWidth * ($this->maxHeight / $this->sourceHeight));
            } else {
                $this->resizedWidth = $this->maxWidth;
                $this->resizedHeight = $this->maxHeight;
            }
        } else {
            $this->resizedWidth = $this->maxWidth;
            $this->resizedHeight = $this->maxHeight;
        }

        $this->resized = imagecreatetruecolor($this->resizedWidth, $this->resizedHeight);

        if ($this->sourceWidth <= $this->maxWidth && $this->sourceHeight <= $this->maxHeight && $this->inflate == false) {
            $this->resized = $this->source;
        } else {
            imagecopyresampled($this->resized, $this->source, 0, 0, 0, 0, $this->resizedWidth, $this->resizedHeight, $this->sourceWidth, $this->sourceHeight);
        }
    }

}