<?php

namespace App\Utilities;

use DateTime;
use App\Jobs\ResizeProfilePhoto;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ProfilePhotoResizeLauncher
{
    private $imgFiles;
    private $imgLocation;
    private $currentTimeStamp;

    public function __construct(string $imgLocation)
    {
        $this->currentTimeStamp = new DateTime('now');

        $this->imgLocation = 'public/profile_photos';
    }

    public function resizeImages()
    {
        $this->imgFiles = Storage::files($this->imgLocation);

        foreach($this->imgFiles as $file) {
            $timestamp = DateTime::createFromFormat("U\.u", sprintf('%1.6F', Storage::lastModified($file)));
            $age = date_diff($timestamp, $this->currentTimeStamp);

            $locationPrefix = __DIR__ . '/../../storage/app/';

            $mimeType = mime_content_type($locationPrefix . $file);

            if ((int) $age->format('%d') <= 1 && str_contains($mimeType, 'image')) {
                $imgFile = $locationPrefix . $file;
                Log::info("Sending file " . $file . ' to be resized.');
                dispatch((new ResizeProfilePhoto($imgFile))->onQueue('high'));
            } else {
                continue;
            }
        }
    }
}