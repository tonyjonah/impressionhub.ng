<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Mail\Announcement;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class MakeAnnouncement extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:announcement {announcement}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send announcements to all coworkers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::all();

        $announcement = $this->argument('announcement');

        foreach($users as $user) {
            Mail::to($user)->send(new Announcement($announcement));

            $this->info('Email has been sent successfully to ' . $user->email);
        }
        return 0;
    }
}
