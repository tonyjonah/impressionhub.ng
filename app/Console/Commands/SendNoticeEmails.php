<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use App\Mail\NoticeOnPriceChange;
use Illuminate\Support\Facades\Mail;

class SendNoticeEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:notice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Price Change Notice to clients';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::all();

        foreach($users as $user) {
            Mail::to($user)->send(new NoticeOnPriceChange);

            $this->info('Email has been sent successfully to ' . $user->email);
        }
    }
}
