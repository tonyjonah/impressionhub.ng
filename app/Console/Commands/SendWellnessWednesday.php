<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Jobs\SendNewsLetter;
use App\Mail\WellnessWednesday;
use Illuminate\Console\Command;

class SendWellnessWednesday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:wellnesswednesday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send wellness Wednesday newsletter';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::all();

        foreach($users as $user) {
            dispatch((new SendNewsLetter($user, new WellnessWednesday()))->onQueue("low"));
        }
        return 0;
    }
}
