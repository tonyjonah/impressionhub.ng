<?php

namespace App\Console\Commands;

use App\Jobs\SendNewsLetter;
use Illuminate\Support\Facades\Mail;
use Illuminate\Console\Command;
use App\Mail\TurnUpFriday;
use App\Models\User;

class SendTurnUpFriday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:sendturnup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Friday Turnup emails to all subscribers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::all();

        foreach($users as $user) {
            dispatch((new SendNewsLetter($user, new TurnUpFriday()))->onQueue("low"));
        }
        return 0;
    }
}
