<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Mail\MondayImpulse;
use App\Jobs\SendNewsLetter;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendMondayImpulse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:sendimpulse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Monday Impulse to all subscribers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::all();

        foreach($users as $user) {
            dispatch((new SendNewsLetter($user, (new MondayImpulse())))->onQueue("low"));
        }
        return 0;
    }
}
