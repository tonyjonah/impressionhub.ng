<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use App\Utilities\ImageResizer;

class ResizeProfilePhoto implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $imgSource;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $imgSource)
    {
        $this->imgSource = $imgSource;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $resizer = new ImageResizer(800, 800);
        $resizer->loadFile($this->imgSource);
        $resizer->buildImage($this->imgSource);
        Log::info('Resizer operation on profile photo ' . $this->imgSource . ' has been completed successfully.');
    }
}
