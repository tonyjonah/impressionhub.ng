<?php

namespace Database\Seeders;

use App\Models\SubscriptionPlan;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubscriptionPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(DB::table('subscription_plans')->get()->count() == 0) {
            $plans = [
                ['standard_daily','standard', 2300, 1, '09:00:00', '21:00:00', '10:00:00', '17:00:00'],
                ['standard_flexi_monthly', 'standard', 15000, 30, '09:00:00', '21:00:00', '10:00:00', '17:00:00'],
                ['standard_extended_monthly', 'standard', 24000, 30, '09:00:00', '21:00:00', '10:00:00', '17:00:00'],
                ['standard_extended_weekly', 'standard', 9000, 5, '9:00:00', '21:00:00', NULL, NULL],
                ['premium_daily', 'premium', 4000, 1, '9:00:00', '21:00:00', '10:00:00', '17:00:00'],
                ['premium_extended_monthly', 'premium', 30000, 30, '9:00:00', '21:00:00', '10:00:00', '17:00:00'],
                ['premium_extended_weekly', 'premium', 14000, 5, '9:00:00', '21:00:00', NULL, NULL],
            ];

            foreach ($plans as $plan) {
                DB::table('subscription_plans')->insert([
                    'plan_name'         => $plan[0],
                    'type'              => $plan[1],
                    'plan_cost'         => $plan[2],
                    'tenure'            => $plan[3],
                    'starts_at'         => $plan[4],
                    'ends_at'           => $plan[5],
                    'weekend_starts_at' => $plan[6],
                    'weekend_ends_at'   => $plan[7],
                    'created_at'        => now(),
                    'updated_at'        => now(),
                ]);
            }
        }
    }
}
