<?php

namespace Database\Seeders;

use App\Models\RoomOption;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(RoomOption::all()->count() == 0) {
            $room_options = [
                [1, 'meeting_room_standard_hourly', 4000 , false, 2],
                [1, 'meeting_room_members_hourly',  3000, true, 1],
                [1, 'meeting_room_standard_daily',  17000, false, 4],
                [1, 'meeting_room_members_daily',   14000, true, 3],
                [2, 'training_room_standard_hourly',7000, false, 6],
                [2, 'training_room_members_hourly', 5000, true, 5],
                [2, 'training_room_standard_daily', 24000, false, 8],
                [2, 'training_room_members_daily',  19000, true, 7],
            ];

            foreach($room_options as $room_option) {
                DB::table('rooms')->insert([
                    'room_type'        => $room_option[0],
                    'room_option_name' => $room_option[1],
                    'room_option_cost' => $room_option[2],
                    'member_type'      => $room_option[3],
                    'option_pairing'   => $room_option[4]
                ]);
            }
        }
    }
}
