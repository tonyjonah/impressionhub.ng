<?php

namespace Database\Seeders;

use App\Models\RoomType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(RoomType::all()->count() == 0) {
            $room_types = [
                'Meeting',
                'Training',
            ];

            foreach($room_types as $room_type) {
                DB::table('room_type')->insert([
                    'room_type_name' => $room_type
                ]);
            }
        }
    }
}
