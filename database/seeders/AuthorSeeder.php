<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $authors = [
            [
                'fname' => 'Olamide',
                'lname' => 'Adepegba',
                'email' => 'info@impressionhub.ng'
            ],
            [
                'fname' => 'Tony',
                'lname' => 'Jonah',
                'email' => 'tony@impressionhub.ng'
            ]
        ];

        foreach ($authors as $key => $value) {
            DB::table('article_authors')->insert(
                [
                'fname' => $value['fname'],
                'lname' => $value['lname'],
                'email' => $value['email']
                ]
            );
        }
    }
}
