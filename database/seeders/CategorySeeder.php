<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Entrepreneurship',
            'Health',
            'Lifestyle',
            'Wellness',
            'Social',
            'Politics',
            'News',
            'Finance',
            'Legal',
            'Educational',
            'Business',
            'Personal Growth',
        ];

        foreach ($categories as $key => $value) {
            DB::table('categories')->insert([
                'category' => $value
            ]);
        }
    }
}
