<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\RoomOption;
use App\Models\RoomType;
use App\Models\Subscription;
use App\Models\SubscriptionPlan;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        // $this->call(RoomTypeSeeder::class);
        // $this->call(RoomSeeder::class);
        // $this->call(SubscriptionPlanSeeder::class);
        // $this->call(AuthorSeeder::class);
        // $this->call(CategorySeeder::class);
        // $this->call(UserSeeder::class);
        $this->call(ArticleSeeder::class);
    }
}
