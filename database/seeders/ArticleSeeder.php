<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $articles = [
        //     [
        //         'title' => 'Like is Like Riding a Bicycle',
        //         'slug'  => 'life-is-like-riding-a-bicycle',
        //         'img_name' => '090123_1.jpg',
        //         'excerpt' => "The saying 'Life is like a bicycle' suggests that life is a continuous journey that requires",
        //         'body'  => '<p>
        //         The saying "Life is like a bicycle" suggests that life is a continuous journey that requires
        //         balance and forward momentum to stay on track. Just like a bicycle, we must navigate through
        //         various challenges and obstacles that come our way. To stay balanced, we must find a way to stay
        //         grounded and focused, even when the road ahead is uncertain. We must also keep pedaling and moving
        //         forward,
        //         even when it feels difficult or when we encounter setbacks.
        //     </p>

        //     <p>
        //         Life, like a bicycle, also requires effort and determination. We must put in the work to keep moving
        //         forward and
        //         to achieve our goals. Just as a bicycle requires the use of our legs to propel us forward, life requires
        //         us to put
        //         in the effort to move towards our desired destination.

        //     <p>
        //         However, just as a bicycle allows us to explore and experience the world around us, life offers us
        //         endless opportunities
        //         to learn, grow, and discover. Whether we are navigating through rough terrain or cruising on smooth
        //         paths, life is always
        //         full of new experiences and adventures. By embracing these challenges and staying focused on our
        //         journey, we can make the most of the ride.
        //     </p>',
        //         'author'=> 1,
        //     ],
        //     [
        //         'title' => 'Smile, Its the Weekend',
        //         'slug'  => 'smile-its-the-weekend',
        //         'img_name' => '130123_1.jpg',
        //         "excerpt" => "It's the weekend, which means it's time to relax and take a break from a demanding work week.",
        //         'body'  => '<p>
        //         It"s the weekend, which means it"s time to relax and take a break from a demanding work week. Whether it"s 
        //         sleeping late, having a leisurely breakfast, or indulging in a hobby, the weekend provides a chance to unwind 
        //         and recharge. Additionally, you can use it to do things with friends and family that you might not have time 
        //         for during the week. This weekend offers a wide variety of activities that might help you unwind and enjoy your 
        //         free time. Following is a list of a few of the events:
        //     </p>
        // </div>
        // <div class="events">
        //     <div>
        //         <h2 class="events-header">Friday Karaoke Night</h2>
        //         <div class="e-images"><img src="https://impressionhub.ng/img/130123_kar.png" alt="karaoke picture"></div>
        //         <p class="e-graphs">Learn more about this event by <span><a href="https://www.eventbrite.com/e/friday-karaoke-party-tickets-483902975827?aff=ebdssbdestsearch" target="_blank" class="linkie">clicking here</a></span></p>
        //     </div>
        //     <div>
        //         <h2 class="events-header">BCIE Free Visa Summer 2023</h2>
        //         <div class="e-images"><img src="https://impressionhub.ng/img/130123_free-visa.png" alt="Visa seminar"></div>
        //         <p class="e-graphs">Learn more about this event by <span><a href="https://www.eventbrite.com/e/bcie-free-visa-seminar-2023-tickets-506777353707?aff=ebdssbdestsearch" target="_blank" class="linkie">clicking here</a></span></p>
        //     </div>
        //     <div>
        //         <h2 class="events-header">Getting Prepared for the Future of Work</h2>
        //         <div class="e-images"><img src="https://impressionhub.ng/img/130123_future-work.png" alt="Getting prepared picture"></div>
        //         <p class="e-graphs">Learn more about this event by <span><a href="https://www.eventbrite.com/e/getting-prepared-for-the-future-of-work-an-ie-personal-development-workshop-tickets-482917518297?aff=ebdssbdestsearch&keep_tld=1" target="_blank" class="linkie">clicking here</a></span></p>
        //     </div>
        //     <div>
        //         <h2 class="events-header">Premier League Derbys</h2>
        //         <div class="e-images"><img src="https://impressionhub.ng/img/130123_derby.png" alt="Premier league derbys"></div>
        //         <p class="e-derby">The Manchester United vs Manchester City Derby is set to take place this weekend. 
        //             Manchester United are trying to maintain good form, while Manchester City hopes to bounce back from 
        //             their loss against Southampton in the EFL Cup on Wednesday.
        //         </p>
        //         <p class="e-derby">
        //             The North London Derby is also set to take place this weekend, with Arsenal trying to stay on top of the premier 
        //             league table and Tottenham hopes to beat Arsenal after their 4-0 victory against Crystal Palace on Monday.
        //         </p>
        //         <p class="e-derby">
        //             For the football lovers, you really don"t want to miss these fixtures.
        //         </p>
                
        //     </div>',
        //         'author'=> 1,
        //     ],
        //     [
        //         'title' => 'Stay In The Flow',
        //         'slug'  => 'stay-in-the-flow',
        //         'img_name' => '180123_med1.jpg',
        //         'excerpt' => "As the week comes to an end, it's important to stay focused and remain in the flow.",
        //         'body'  => "<div class='paragraphs'>
        //         <p>
        //         As the week comes to an end, it's important to stay focused and remain in the flow. When we're in 
        //         the flow, we're fully engaged in what we're doing and are able to accomplish more in less time. 
        //         This can lead to increased productivity, creativity and overall satisfaction in our work.
        //     </p>

        //     <p>
        //         One way to stay in the flow is to set clear and specific goals for the day or week. Having a 
        //         clear understanding of what needs to be accomplished can help you stay focused and motivated. 
        //         Additionally, it's important to eliminate distractions and create a conducive work environment. 
        //         This can be achieved by turning off notifications on your phone, closing unnecessary tabs on your 
        //         computer and creating a comfortable and organized workspace.

        //     <p>
        //         It's also important to take regular breaks to rest and recharge. This can help to prevent burnout 
        //         and increase your ability to stay in the flow. So, as the week comes to an end, remember to stay 
        //         focused, set specific goals, eliminate distractions, create a conducive work environment and take 
        //         regular breaks. With these tips in mind, you'll be able to make the most of your time and achieve great results.
        //     </p>
        //         </div>",
        //         'author'=> 1,
        //     ],
        //     [
        //         'title' => 'Smile, Its The Weekend',
        //         'slug'  => 'smile-its-the-weekend',
        //         'img_name' => '200123_party2.jpg',
        //         'excerpt' => "It's the weekend, which means it's time to unwind after a long work week. ",
        //         'body'  => "<div class='paragraphs'>
        //         <p>
        //            It's the weekend, which means it's time to unwind after a long work week. 
        //            The weekend offers an opportunity to relax and rejuvenate, whether it's through 
        //            sleeping in late, enjoying a leisurely breakfast, or engaging in a pastime. 
        //            You can also use it to spend time with friends and family on activities that 
        //            you might not have time for during the week. Numerous activities are available 
        //            this weekend that could aid in your relaxation and enjoyment of your free time. 
        //            A few of the events are listed below:
        //         </p>
        //     </div>
        //     <div class='events'>
        //         <div>
        //             <h2 class='events-header'>Vibes & Chill</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/200123_eve3.jpg' alt='karaoke picture'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://turnuplagos.com/21-jan-2023-vibes-chill/' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //         <div>
        //             <h2 class='events-header'>Canada - Study, Work & Migrate</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/200123_eve4.jpg' alt='Visa seminar'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://www.eventbrite.com/e/canada-study-work-migrate-tickets-466595037317?aff=ebdshpsearchautocomplete&keep_tld=1' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //         <div>
        //             <h2 class='events-header'>Sip and Paint</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/200123_eve1.jpg' alt='Getting prepared picture'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://www.eventbrite.com/e/sip-and-paint-tickets-509309938737?aff=ebdssbdestsearch' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //         <div>
        //             <h2 class='events-header'>Arsenal v Manchester United</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/200123_eve5.jpg' alt='Premier league derbys'></div>
        //             <p class='e-derby'>Sunday's match between Arsenal and Manchester United will be a crucial one as the race for the 
        //                 league heats up. Manchester United is currently in 3rd place while Arsenal remains at the top. The game will 
        //                 take place at the Emirates Stadium, with Arsenal looking to secure a win after losing at Old Trafford earlier
        //                 in the season.
        //             </p>
                    
        //         </div>
        //     </div>",
        //         'author'=> 1,
        //     ],
        //     [
        //         'title' => 'Building A Growth Mindset',
        //         'slug'  => 'building-a-growth-mindset',
        //         'img_name' => '230123_growth3.jpg',
        //         'excerpt' => "Building a growth mindset can have numerous benefits for individuals and organizations.",
        //         'body'  => "<div class='paragraphs'>
        //         <p>
        //             Do you have a growth mindset? Would you like to develop a growth mindset?
        //         </p>
        //         <p>
        //             Building a growth mindset can have numerous benefits for individuals and organizations. 
        //             A growth mindset is the belief that one's abilities and intelligence can be developed 
        //             through hard work and dedication. This mindset is in contrast to a fixed mindset, 
        //             which holds that abilities are predetermined and cannot be changed. Carol Dweck explored this 
        //             in great detail in her book titled Mindset: The Psychology of Success.
        //         </p>
    
        //         <p>
        //             Adopting a growth mindset can lead to greater resilience in the face of challenges and obstacles. 
        //             It can also foster a more positive attitude towards learning and personal development. Individuals 
        //             with a growth mindset are more likely to take on new challenges and persist in the face of setbacks. 
        //             They are also more likely to seek out feedback and to view failure as an opportunity for growth.
    
        //         <p>
        //             In organizations, a growth mindset can foster a culture of continuous improvement, leading to increased productivity, 
        //             innovation, and overall success. Additionally, with a growth mindset, employees are more likely to take on new 
        //             challenges, and be more open to learning new skills and taking on new roles, which can lead to career development 
        //             and advancement. Overall, Building a growth mindset can be a powerful tool for personal and professional development, 
        //             and can lead to greater success and fulfillment.
        //         </p>
        //     </div>",
        //         'author'=> 1,
        //     ],
        //     [
        //         'title' => "Don't Force It",
        //         'slug'  => 'dont-force-it',
        //         'img_name' => '270123_force3.jpg',
        //         'excerpt' => "We know it's been a long week, and on this side, we're definitely glad that the weekend is here",
        //         'body'  => "<div class='paragraphs'>
        //         <p>
        //             We know it's been a long week, and on this side, we're definitely glad that the weekend is here.
        //             But, let me guess, you're still racking your head on how to complete that last task your boss just brought to your desk. Kill joy right? 
        //             Fret not, these things do happen and most importantly, you shouldn't try to force things. 
        //             By not trying to force out the solution, we allow ourselves to be more open to new ideas and opportunities. 
                     
        //         </p>
        //         <p>
        //             This can make you more flexible and adaptable, 
        //             instead of trying to force out a solution or outcome on the spot. A much better (and sweeter) approach is to take a step back and look at the bigger picture. Try it for a minute now! Don't worry, we'll wait.
                    
        //         </p>
        //         <p> 
        //             You'll notice that this allows you to come up with more creative and innovative solutions, you're welcome. You can thank us later with drinks at the lounge.
        //         </p>
        //         <p>
        //             All work and no play makes Jack a dull boy, It's the weekend, 
        //             so we have compiled a list of activities you can enjoy. Take a moment to relax, take a deep breath, and enjoy your weekend.
        //         </p>
        //     </div>
        //     <div class='events'>
        //         <div>
        //             <h2 class='events-header'>Color Booth</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/270123_event1.jpg' alt='Color booth'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://turnuplagos.com/29-jan-2023-colour-booth/' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //         <div>
        //             <h2 class='events-header'>Feast N Dive</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/270123_event2.jpg' alt='Pool party'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://turnuplagos.com/29-jan-2023-feast-n-dive/' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //         <div>
        //             <h2 class='events-header'>Kala: For Yourself, For Naija (A Total Theatre Performance)</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/270123_event3.jpg' alt='Getting prepared picture'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://www.eventbrite.com/e/kala-for-yourself-for-naija-a-total-theatre-performance-tickets-521308235967?aff=ebdssbcitybrowse' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //         <div>
        //             <h2 class='events-header'>Lagos Blockchain Conference (MaVie Tsunami Tour Nigeria)</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/270123_event4.jpg' alt='Premier league derbys'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://www.eventbrite.com/e/lagos-blockchain-conference-mavie-tsunami-tour-nigeria-tickets-516388069617?aff=ebdssbdestsearch' target='_blank' class='linkie'>clicking here</a></span></p>
                    
        //         </div>
        //         <div>
        //             <h2 class='events-header'>Mancity - Arsenal</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/270123_man-city-fac-a.png' alt='Football'></div>
        //             <p class='e-derby'>You can kick-start the weekend with this key match-up.</p>
        //         </div>
        //     </div>",
        //         'author'=> 1,
        //     ],
        //     [
        //         'title' => 'Binge Watch This!',
        //         'slug'  => 'binge-watch-this',
        //         'img_name' => '030223_fimg2.jpg',
        //         'excerpt' => "It's weekend already, and I know that you're most likely trying to think of how best you can enjoy the short weekend. Binge watching some series wouldn't be a bad idea at all.",
        //         'body'  => "<div class='paragraphs'>
        //         <p>
        //             It's weekend already, and I know that you're most likely trying to think of how best you can enjoy the short weekend. Binge watching some series wouldn't be
        //              a bad idea at all. 'Silicon Valley' is a popular television show that offers a satirical look at the tech industry and startup culture. It's considered a 
        //              must-watch for those interested in tech and entrepreneurship, as it provides a comedic and exaggerated but often accurate portrayal of the challenges 
        //              and absurdities that come with starting a tech company. The show features a talented cast of actors and has been praised for its clever writing, 
        //              relevant themes, and relatable characters. Whether you're a tech professional or simply enjoy witty, insightful humor, 'Silicon Valley' is a highly 
        //              entertaining and thought-provoking show that's definitely worth checking out.
        //         </p>
        //         <p>
        //             'Rick and Morty' is also a must watch. It is a popular animated science fiction sitcom that has won a large following for its unique blend of humor, action, and existential philosophy. 
        //             Here are some reasons why you should watch 'Rick and Morty': 'Rick and Morty' features a mix of humor, science fiction, and social commentary, making it a unique and captivating show to watch.
        //             The main characters, Rick and Morty, are complex and multi-dimensional, with a wide range of emotions, motivations, and flaws. The animation in 'Rick and Morty' is top-notch, with detailed and imaginative worlds and characters.
        //             Whether you're a fan of science fiction, comedy, or unique and thought-provoking storytelling, 'Rick and Morty' is definitely worth watching. 
        //         </p>
        //         <p>
        //             All work and no play makes Jack a dull boy, It's the weekend,
        //             so we have compiled a list of activities you can enjoy. Take a moment to relax, take a deep breath, and enjoy your weekend.
        //         </p>
        //     </div>
        //     <div class='events'>
        //         <div>
        //             <h2 class='events-header'>Sip And Paint . NG In Lagos</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/030223_fevent.jpg' alt='Color booth'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://turnuplagos.com/04-feb-2023-sip-and-paint-ng-in-lagos/' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //         <div>
        //             <h2 class='events-header'>Jewels Of Takwa Bay</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/030223_fevent1.jpg' alt='Pool party'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://turnuplagos.com/04-feb-2023-jewels-of-takwa-bay/' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //         <div>
        //             <h2 class='events-header'>Burger Games - Yaba</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/030223_fevent3.jpg' alt='Getting prepared picture'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://www.eventbrite.com/e/burger-games-yaba-tickets-529423579177?aff=ebdssbcitybrowse' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //     </div>",
        //         'author'=> 1,
        //     ],
        //     [
        //         'title' => 'We Move',
        //         'slug'  => 'we-move',
        //         'img_name' => '170223_fimg.jpg',
        //         'excerpt' => 'We are aware that this time may be challenging for all, due to the civil unrest, and how it affects our communities, businesses, and everyday lives.',
        //         'body'  => "<div class='paragraphs'>
        //         <p>
        //             We are aware that this time may be challenging for all, due to the civil unrest, and how it 
        //             affects our communities, businesses, and everyday lives.
        //             We hope you're doing well and staying safe at this time.    
        //         </p>

        //         <p>
                    
        //             We understand that it can be tough to stay motivated and productive during times of crisis. 
        //             That's why we want to support our community as much as we can, by offering not just a physical space to collaborate but also 
        //             a community that looks out for one another. 
        //         </p>
               
        //         <p>
        //             Our way of helping is by providing resources and suggestions to keep you occupied in a positive way. 
        //             If you are free this weekend, We have curated a list of events you could attend which might help you 
        //             take your mind off the ongoing crisis.
        //         </p>
        //     </div>
        //     <div class='events'>
        //         <div>
        //             <h2 class='events-header'>Love By Moonlight</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/170223_event1.jpg' alt='Color booth'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://turnuplagos.com/17-feb-2023-love-by-moonlight/' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //         <div>
        //             <h2 class='events-header'>Love Feast (Special Edition)</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/170223_event.jpg' alt='Pool party'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://www.eventbrite.com/e/love-feast-special-edition-tickets-525441187737?aff=ebdssbcitybrowse' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //         <div>
        //             <h2 class='events-header'>Couple's Dinner</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/170223_event2.jpg' alt='Getting prepared picture'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://www.eventbrite.com/e/couples-dinner-tickets-528405453937?aff=ebdssbcitybrowse' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //         <div>
        //             <h2 class='events-header'>DBC Meet and Greet</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/170223_event3.jpg' alt='Color booth'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://www.eventbrite.com/e/dbc-meet-greet-tickets-525158111047?aff=ebdssbcitybrowse' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //     </div>",
        //         'author'=> 1,
        //     ],
        //     [
        //         'title' => 'The Benefits of Exercise and Healthy Eating for a Successful Week',
        //         'slug'  => 'benefits-of-exercise-and-healthy-eating',
        //         'img_name' => '20.02.23.jpg',
        //         'excerpt' => "Exercise and healthy eating habits can significantly impact one's productivity, mood, and overall",
        //         'body'  => "<div class='paragraphs'>
        //         <p>
        //             Do you know that maintaining a healthy lifestyle is not only essential for a fit body? but also for a successful week.
        //         </p>
        //         <p> 
        //             Exercise and healthy eating habits can significantly impact one's productivity, mood, and overall 
        //             well-being. In this article, we will discuss the benefits of exercise and healthy eating for a successful week.
        //         </p>
    
        //         <p>
        //             Regular exercise is proven to increase energy levels and reduce stress. Physical activity stimulates the production 
        //             of endorphins, which are neurotransmitters that boost mood and reduce stress levels. Studies have shown that exercising 
        //             for just 30 minutes a day can improve cognitive function, memory, and concentration. Incorporating exercise into one's 
        //             routine can also lead to better sleep, which is crucial for a productive day.
    
        //         <p>
        //             Healthy eating habits are just as important as exercise for a successful week. Eating a balanced diet that includes fruits, 
        //             vegetables, lean protein, and complex carbohydrates can provide the body with the necessary nutrients to function optimally. 
        //             It is also important to drink plenty of water to stay hydrated throughout the day. When the body is properly fueled, it can 
        //             increase energy levels, improve mood, and promote mental clarity.
        //         </p>
        //         <p>
        //             Conclusively, incorporating exercise and healthy eating habits into one's routine can significantly impact the success of the week. 
        //             Regular physical activity and a balanced diet can increase energy levels, reduce stress, improve mood and mental clarity, and boost 
        //             self-confidence. These habits can also promote overall health and reduce the risk of chronic diseases. Taking care of our bodies is 
        //             not only essential for physical health but also for a productive and successful week.
        //         </p>
        //     </div>",
        //         'author'=> 1,
        //     ],
        //     [
        //         'title' => 'Upping Your Game',
        //         'slug'  => 'upping-your-game',
        //         'img_name' => '03.03.234.jpg',
        //         'excerpt' => 'Personal development is a crucial aspect of self-improvement. There are many ways to develop oneself,',
        //         'body'  => "<div class='paragraphs'>
        //         <p>
        //             Personal development is a crucial aspect of self-improvement. There are many ways to develop oneself, 
        //             such as through reading, meditation, or learning new skills. It's important to take time for personal 
        //             development because it can lead to greater happiness, success, and fulfillment in life.    
        //         </p>

        //         <p>
                    
        //             Reading is a great way to develop oneself because it allows for learning and self-reflection. 
        //             By reading books on personal growth or self-help, one can gain valuable insights and perspectives 
        //             on life. Additionally, reading can provide a form of relaxation and escape from the stresses of everyday life. 
        //         </p>
               
        //         <p>
        //             Meditation is another great way to develop oneself because it allows for mindfulness and inner peace. 
        //             By practicing meditation, one can learn to control their thoughts and emotions, leading to a greater 
        //             sense of calm and focus. Additionally, meditation can help with stress reduction, anxiety, and depression.
        //         </p>
        //         <p>
        //             Learning new skills is also a great way to develop oneself because it allows for personal growth and 
        //             professional advancement. By taking courses or workshops on topics such as coding, public speaking, or 
        //             leadership, one can gain valuable knowledge and skills that can lead to career success and personal fulfillment.
        //         </p>
        //         <p>
        //             It's the weekend again, we have curated a list of events that you can attend this weekend.
        //         </p>
        //     </div>
        //     <div class='events'>
        //         <div>
        //             <h2 class='events-header'>Real Estate Portfolio Management Training</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/03.03.23.jpg' alt='Real estate Image'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://www.eventbrite.com/e/real-estate-portfolio-management-training-tickets-558696916517?aff=ebdssbcitybrowse' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //         <div>
        //             <h2 class='events-header'>Secure your financial future with us by becoming an Investor</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/03.03.232.jpg' alt='Getting prepared picture'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://www.eventbrite.com/e/relaxing-pottery-hangout-tickets-509052940047?aff=ebdssbcitybrowse' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //         <div>
        //             <h2 class='events-header'>Blockchain 4 Business</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/03.03.235.jpg' alt='Color booth'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://www.eventbrite.com/e/blockchain-4-business-tickets-556531218857?aff=ebdssbcitybrowse' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //     </div>",
        //         'author'=> 1,
        //     ],
        //     [
        //         'title' => 'Why Working From Home Can be Dangerous as an Entrepreneur',
        //         'slug'  => 'why-working-from-home-can-be-dangerous-for-entrepreneurs',
        //         'img_name' => '06.03.230.jpg',
        //         'excerpt' => 'Working from home as an entrepreneur can be a tempting option for many, offering flexibility and reduced costs',
        //         'body'  => "<div class='paragraphs'>
        //         <p>
        //             Do you know that working from home can be dangerous as an entrepreneur?
        //         </p>
        //         <p> 
        //             Working from home as an entrepreneur can be a tempting option for many, offering flexibility and reduced costs. However, 
        //             without the structure and accountability of a traditional workplace, it can be easy to become isolated, distracted, and 
        //             unmotivated. It can also be challenging to separate work and personal life, leading to overwork and burnout.
        //         </p>
    
        //         <p>
        //             For example, Mr. A, an entrepreneur seeking funding for his startup, initially tried to work from home. 
        //             However, he found it challenging to focus on his business while being surrounded by the distractions of 
        //             his household. He decided to utilize a co-working space, which not only provided him with a professional 
        //             environment but also enabled him to connect with like-minded individuals who could offer him advice and support. 
        //             Through the connections he made at the co-working space, Mr. A was able to secure the funding he needed to take 
        //             his business to the next level.
    
        //         <p>
        //             A co-working space is an excellent option for entrepreneurs looking to expand their network. These spaces bring together 
        //             individuals from diverse backgrounds and industries, providing opportunities for collaboration and knowledge-sharing. 
        //             Entrepreneurs can connect with potential clients, partners, and investors, as well as gain valuable insights into new 
        //             trends and developments in their field.
        //         </p>
        //         <p>
        //             Conclusively, while working from home may seem like a convenient option for entrepreneurs, it is essential to consider 
        //             the potential dangers and limitations. Utilizing a co-working space can provide entrepreneurs with a structured, 
        //             professional environment, as well as valuable networking opportunities that can help them to achieve their goals.
        //         </p>

        //         <p>
        //             By the way, if you're working from home, you might want to consider visiting Impression Hub 
        //             if you stay in the Gbagada, Shomolu, Yaba or Akoka area. We've seen a number of entrepreneurs who were loosing ground from working at home 
        //             suddenly bounce back with confidence after using our space because of the proximity to their homes and the availability of resources we provide.
        //             They made new friends and some even became business partners after hanging out at our hub. You can check us out by giving us a call or sending 
        //             us an email or a dm.
        //         </p>
        //     </div>",
        //         'author'=> 1,
        //     ],
        //     [
        //         'title' => 'Ready to Dominate?',
        //         'slug'  => 'ready-to-dominate',
        //         'img_name' => '10.03.234.jpg',
        //         'excerpt'=> "Dominating in your career requires a combination of hard work, dedication, and strategic planning. We know It's not an easy task
        //         that's why we are here for you, and we will be sharing you some tips that can help you",
        //         'body'  => "<div class='paragraphs'>
        //         <p>
        //             Dominating in your career requires a combination of hard work, dedication, and strategic planning. We know It's not an easy task
        //             that's why we are here for you, and we will be sharing you some tips that can help you
        //              achieve success and become a dominant force in your chosen field.
        //         </p>

        //         <p>
                    
        //             First, set clear goals and create a plan to achieve them. This plan should include short-term and long-term 
        //             objectives, as well as actionable steps you can take to achieve them. Be specific and realistic with your 
        //             goals and timeline, and regularly reassess and adjust your plan as necessary.
        //         </p>
               
        //         <p>
        //             Second, continuously improve your skills and knowledge. Stay up-to-date with the latest trends and developments 
        //             in your field, and seek out opportunities for professional development and learning. Consider taking courses, 
        //             attending conferences and seminars, and networking with other professionals.
        //         </p>
        //         <p>
        //             Third, cultivate a strong work ethic and commitment to excellence. Consistently produce high-quality work, exceed 
        //             expectations, and go above and beyond in your role. This will demonstrate your value to your employer and make you 
        //             stand out among your peers.
        //         </p>
        //         <p>
        //             Fourth, build strong relationships with colleagues, clients, and industry leaders. This can help you gain valuable insights, 
        //             access new opportunities, and establish a positive reputation in your field.
        //         </p>
        //         <p>
        //             Finally, maintain a positive attitude and mindset, and persevere through challenges and setbacks. Embrace failure as a 
        //             learning opportunity and use it to fuel your growth and development.
        //         </p>
        //         <p>
        //             It's the weekend again, we have curated a list of events that you can attend this weekend.
        //         </p>
        //     </div>
        //     <div class='events'>
        //         <div>
        //             <h2 class='events-header'>The Writer's Hangout</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/10.03.230.jpg' alt='Real estate Image'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://turnuplagos.com/11-mar-2023-the-writers-hangout/' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //         <div>
        //             <h2 class='events-header'>Wine Tasting in Lagos</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/10.03.231.jpg' alt='Getting prepared picture'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://mirkout.com/lagos/wine-tasting-in-lagos-sip-and-paint-ng-event8487249' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //         <div>
        //             <h2 class='events-header'>Women in Tech: Embracing Equity</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/10.03.232.jpg' alt='Color booth'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://mirkout.com/lagos/women-in-tech-embracing-equity-event8464128' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //         <div>
        //             <h2 class='events-header'>Weekly Healthy Buffet</h2>
        //             <div class='e-images'><img src='https://impressionhub.ng/img/10.03.233.jpg' alt='Color booth'></div>
        //             <p class='e-graphs'>Learn more about this event by <span><a href='https://mirkout.com/lagos/weekly-healthy-buffet-event8487263' target='_blank' class='linkie'>clicking here</a></span></p>
        //         </div>
        //     </div>",
        //         'author'=> 1,
        //     ],
            // [
            //     'title' => 'Career Stress Management',
            //     'slug'  => 'career-stress-management',
            //     'img_name' => '13.03.230.jpg',
            //     'excerpt'=> 'The causes of career stress for entrepreneurs can be diverse, ranging from financial pressures and long work hours to the uncertainty of the marketplace and the pressure to succeed.',
            //     'body'  => '<div class="paragraphs">
            //         <p> 
            //             Entrepreneurship can be a highly rewarding career path, but it can also be a source of significant stress. 
            //             The causes of career stress for entrepreneurs can be diverse, ranging from financial pressures and long work 
            //             hours to the uncertainty of the marketplace and the pressure to succeed. In addition, entrepreneurs may experience 
            //             stress from peer pressure, as they may feel pressure to keep up with competitors or to meet the expectations of investors or partners.
            //         </p>
        
            //         <p>
            //             The effects of career stress on entrepreneurs can be significant, and can lead to burnout, mental health issues, and even business failure. 
            //             When entrepreneurs experience stress, it can impact their decision-making abilities, creativity, and overall effectiveness as leaders. 
            //             It can also lead to strained relationships with family and friends, as well as a diminished quality of life.
        
            //         <p>
            //             Effective stress management strategies can help entrepreneurs to cope with the pressures of entrepreneurship and reduce its negative effects. 
            //             These strategies can include things like setting realistic goals, delegating tasks, seeking support from a mentor or coach, taking breaks and 
            //             engaging in self-care activities, and finding ways to manage workload and time effectively. Additionally, entrepreneurs can work on setting 
            //             boundaries and saying no when they feel overwhelmed by peer pressure, which can help to reduce stress and improve their overall well-being.
            //         </p>
            //         <p>
            //         In conclusion, while career stress can be a significant challenge for entrepreneurs, there are strategies that they can use to manage it effectively. 
            //             By focusing on stress management techniques and seeking support when needed, entrepreneurs can reduce the negative effects of career stress and 
            //             enjoy greater well-being and success in their personal and professional lives.
            //         </p>
            //     </div>',
            //     'author'=> 1,
            // ],
            // [
            //     'title' => '2023 Election Weekend!',
            //     'slug'  => 'election-weekend-2023',
            //     'img_name' => '24.02.23.jpg',
            //     'excerpt'=> 'The election season is often a time of heightened tensions and emotions, and it is important that 
            //     everyone take precautions to avoid any potential dangers that may arise.',
            //     'body'  => "<div class='paragraphs'>
            //             <p>
            //                 In light of the upcoming election, we strongly urge everyone to prioritize their safety this weekend. 
            //                 The election season is often a time of heightened tensions and emotions, and it is important that 
            //                 everyone take precautions to avoid any potential dangers that may arise. We recommend avoiding large crowds, 
            //                 staying informed on the latest developments, and being mindful of your surroundings.   
            //             </p>

            //             <p>
                            
            //                 Furthermore, we encourage everybody to exercise their right to vote and make their voices heard. We believe 
            //                 that it is essential for everyone to vote their conscience and support the candidate that they feel is best 
            //                 suited to be governor. However, we also caution against selling one's vote, as this can have serious 
            //                 consequences for the future of our country. 
            //             </p>
                    
            //             <p>
            //                 We pray for a peaceful and fair election and hope that the outcome will be one that is beneficial for all 
            //                 Nigerians. Let us all do our part to ensure that this election is a success and that our country can continue 
            //                 to move forward toward a brighter future.
            //             </p>
            //         </div>",
            //     'author'=> 1,
            // ],
            // [
            //     'title' => 'Go for it!! ',
            //     'slug'  => 'go-for-it',
            //     'img_name' => '23.03.233.jpg',
            //     'excerpt'=> "For those of us 
            //     who have been brewing an idea for a while now, it's time to ask ourselves: have we made any progress towards bringing 
            //     that idea to life?",
            //     'body'  => "<div class='paragraphs'>
            //     <p>As the weekend comes to an end, it's time to reflect on the progress we've made towards our goals. For those of us 
            //         who have been brewing an idea for a while now, it's time to ask ourselves: have we made any progress towards bringing 
            //         that idea to life?
            //     </p>

            //     <p>
                    
            //         It's easy to let our ideas fall by the wayside in the face of day-to-day obligations and distractions. But it's important 
            //         to remember that the things we create can have a profound impact on our lives and the lives of those around us.
            //     </p>
               
            //     <p>
            //         If you're feeling hesitant about pursuing your idea, remember that great things often start with small steps. Take some 
            //         time this weekend to make a plan for how you can move your idea forward. Set realistic goals and identify the resources 
            //         you need to get there.
            //     </p>
            //     <p>
            //         Most importantly, remember that your idea has the power to change your life. Whether it's a new business venture, a creative project, 
            //         or a personal goal, the act of bringing something new into the world can be transformative.
            //     </p>
            //     <p>
            //         So as we head into a new week, let's make a commitment to ourselves to work on our ideas, no matter how small the steps may be. 
            //         With dedication and persistence, we can turn our dreams into reality and create a life that is truly fulfilling.
            //     </p>
            //     <p>
            //         It's the weekend again, we have curated a list of events that you can attend.
            //     </p>
            // </div>
            // <div class='events'>
            //     <div>
            //         <h2 class='events-header'>Hackaholics 4.0</h2>
            //         <div class='e-images'><img src='https://impressionhub.ng/img/wema1.png' alt='Hackaholics'></div>
            //         <p class='holics'>
            //             Hello tech enthusiasts and future tech innovators, Hackaholics 4.0 is here to give you the opportunity to meet mentors and top 
            //             techpreneurs and also a chance to win up to $10,000 to help you develop that great tech idea.
            //         </p>
            //         <p class='e-graphs'>Hurry now to register by <span><a href='http://bit.ly/Hackaholics4' target='_blank' class='linkie'>clicking here</a></span></p>
            //     </div>
            //     <div>
            //         <h2 class='events-header'>Zodiac Party</h2>
            //         <div class='e-images'><img src='https://impressionhub.ng/img/24.03.230.jpg' alt='Real estate Image'></div>
            //         <p class='e-graphs'>Learn more about this event by <span><a href='https://turnuplagos.com/25-mar-2023-zodiac-party/' target='_blank' class='linkie'>clicking here</a></span></p>
            //     </div>
            //     <div>
            //         <h2 class='events-header'>Hay Foundation Mental Wellness Hangout</h2>
            //         <div class='e-images'><img src='https://impressionhub.ng/img/24.03.231.jpg' alt='Getting prepared picture'></div>
            //         <p class='e-graphs'>Learn more about this event by <span><a href='https://turnuplagos.com/26-mar-2023-hay-foundation-mental-wellness-hangout/' target='_blank' class='linkie'>clicking here</a></span></p>
            //     </div>
            //     <div>
            //         <h2 class='events-header'>Xclusive Beach/Pool Hangout</h2>
            //         <div class='e-images'><img src='https://impressionhub.ng/img/24.03.232.jpg' alt='Color booth'></div>
            //         <p class='e-graphs'>Learn more about this event by <span><a href='https://turnuplagos.com/25-mar-2023-xclusive-beach-pool-hangout/' target='_blank' class='linkie'>clicking here</a></span></p>
            //     </div>
            // </div>",
            //     'author'=> 1,
            // ],
            [
                'title' => 'Keep it Moving',
                'slug'  => 'keep-it-moving',
                'img_name' => '27.03.230.jpg',
                'excerpt'=> 'To achieve greatness, we must strive to be better every day and embrace a growth mindset.',
                'body'  => "<div class='paragraphs'>
                <p> 
                    Do you you know that continuous improvement is essential for success in all aspects of life? Whether it's personal growth, career development, or 
                    mastering a new skill, the world rewards excellence. To achieve greatness, we must strive to be better every day and embrace a growth mindset.
                </p>
    
                <p>
                    The world is constantly evolving, and those who can adapt and improve themselves are the ones who succeed. Excellence is recognized and rewarded 
                    in all industries, from sports to business to the arts. Those who stand out from the crowd are those who have put in the time and effort to hone 
                    their skills and knowledge.
                <p>
                    However, the only barrier standing between us and our growth is our attitude towards learning. When we embrace a growth mindset and approach 
                    challenges with a willingness to learn and improve, we open ourselves up to endless possibilities. We become more resilient, adaptable, and 
                    better equipped to handle the challenges that life throws at us.
                </p>
                <p>
                    Moreover, when we keep getting better, we inspire others to do the same. Our excellence can motivate and encourage those around us to strive 
                    for greatness in their own lives. In this way, our personal growth has a ripple effect that can benefit not only ourselves but also those around us.
                </p>
                <p>
                    In conclusion, the world rewards excellence, and to achieve greatness, we must keep getting better. The only barrier standing in our way is our attitude 
                    towards learning. With a growth mindset and a willingness to continuously improve ourselves, we can unlock our full potential and achieve our goals.
                </p>
            </div>
            <div>
                <p class='sponsored-post'>Sponsored post</p>
                <h3 class='hacker'>Hackaholics 4.0</h3>
                <div class='hacker-photo'> <img src='https://impressionhub.ng/img/wema2.jpeg' alt='Wema bank hackerton image'></div>
                <p class='paragraphs'>
                    Hello tech enthusiasts and future tech innovators, Hackaholics 4.0 is here to give you the opportunity to meet mentors and top techpreneurs and 
                    also a chance to win up to $10,000 to help you develop that great tech idea.
                </p>
                <p class='paragraphs'>What are you waiting for? Hurry now to register by <span ><a class='sponsor-link' target='#' href='http://bit.ly/Hackaholics4'>clicking here</a></span></p>
            </div>",
                'author'=> 1,
            ],
            [
                'title' => 'Co-working Space Etiquette',
                'slug'  => 'coworking-space-etiquette',
                'img_name' => '31.03.230.jpeg',
                'excerpt'=> 'Co-working spaces have become increasingly popular over the years, providing professionals with a collaborative and 
                        flexible workspace. However, with different individuals sharing the same space, co-working space etiquette is critical 
                        to ensure everyone has a comfortable and productive environment.',
                'body'  => 
                "<div class='paragraphs'>
                    <p>Co-working spaces have become increasingly popular over the years, providing professionals with a collaborative and 
                        flexible workspace. However, with different individuals sharing the same space, co-working space etiquette is critical 
                        to ensure everyone has a comfortable and productive environment.
                    </p>

                    <p>
                        
                        It is essential to maintain a quiet and professional environment in co-working spaces. Loud phone conversations and music can be 
                        disruptive and disturb others' work. If you need to make a phone call, take it outside or to a designated phone booth.

                    </p>
                
                    <p>
                        Secondly, respect others' privacy and workspace. Avoid using others' equipment or disturbing their workspace without permission. 
                        Keep your workspace clean and tidy, and avoid taking up more space than necessary.
                    </p>
                    <p>
                        Thirdly, co-working spaces are collaborative environments, and networking is encouraged. However, be respectful of others' time and 
                        avoid interrupting their work. If you want to start a conversation, be sure to introduce yourself and ask if it's a good time to chat.
                    </p>
                    <p>
                        Lastly, be mindful of the community guidelines and rules. Each co-working space has its own set of guidelines, and it is essential to 
                        follow them to maintain a professional and productive environment.
                    </p>
                    <p>
                        In summary, co-working space etiquette is about being respectful and considerate of others. By following these simple guidelines, co-working 
                        spaces can be a great environment for collaboration and productivity.
                    </p>
                    <p>
                        It's the weekend again, we have curated a list of events that you can attend this weekend.
                    </p>
                </div>
                <div class='events'>
                    <div>
                        <h2 class='events-header'>The Rooftop XP</h2>
                        <div class='e-images'><img src='https://impressionhub.ng/img/31.03.231.jpg' alt='Rooftop xp picture'></div>
                        <p class='e-graphs'>Learn more about this event by <span><a href='https://turnuplagos.com/01-apr-2023-the-rooftop-xp/' target='_blank' class='linkie'>clicking here</a></span></p>
                    </div>
                    <div>
                        <h2 class='events-header'>Unikorn's Game Night</h2>
                        <div class='e-images'><img src='https://impressionhub.ng/img/31.03.232.jpg' alt='Game night picture'></div>
                        <p class='e-graphs'>Learn more about this event by <span><a href='https://turnuplagos.com/01-apr-2023-unikorns-game-night/' target='_blank' class='linkie'>clicking here</a></span></p>
                    </div>
                    <div>
                        <h2 class='events-header'>Storytelling</h2>
                        <div class='e-images'><img src='https://impressionhub.ng/img/31.03.233.jpg' alt='Story telling picture'></div>
                        <p class='e-graphs'>Learn more about this event by <span><a href='https://www.eventbrite.com/e/storytelling-tickets-598273079887?aff=ebdssbcitybrowse' target='_blank' class='linkie'>clicking here</a></span></p>
                    </div>
                    <div>
                        <h2 class='events-header'>Amma Abena Live</h2>
                        <div class='e-images'><img src='https://impressionhub.ng/img/31.03.234.jpg' alt='Abena Live Picture'></div>
                        <p class='e-graphs'>Learn more about this event by <span><a href='https://turnuplagos.com/01-apr-2023-amma-abena-live/' target='_blank' class='linkie'>clicking here</a></span></p>
                    </div>
                </div>
                <p class='paragraphs'>
                    Impressionhub is a unique co-working space that blends work and play in a fun and lively environment. 
                    We have a vibrant community of entrepreneurs, freelancers, and creatives who share a passion for innovation.
                </p>",
                'author'=> 1,
            ],
            // [
            //     'title' => '',
            //     'slug'  => '',
            //     'img_name' => '',
            //     'excerpt'=> '',
            //     'body'  => '',
            //     'author'=> 1,
            // ],
            // [
            //     'title' => '',
            //     'slug'  => '',
            //     'img_name' => '',
            //     'excerpt'=> '',
            //     'body'  => '',
            //     'author'=> 1,
            // ],
        ];

        foreach ($articles as $key => $value) {
            DB::table('articles')->insert([
                'title' => $value['title'],
                'slug'  => $value['slug'],
                'excerpt' => $value['excerpt'],
                'img_name' => $value['img_name'],
                'body'  => $value['body'],
                'author'=> $value['author']
            ]);
        }
    }
}
