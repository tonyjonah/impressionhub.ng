<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_schedule', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('room_option');
            $table->dateTime('begin',6);
            $table->dateTime('end',6);
            $table->string('payment_reference')->nullable();
            $table->tinyInteger('payment_confirmed')->default(0);
            $table->unsignedBigInteger('entered_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('convener');
            $table->timestamps(6);

            $table->foreign('convener')->references('id')->on('users');
            $table->foreign('room_option')->references('id')->on('rooms');
            $table->foreign('payment_reference')->references('id')->on('payments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('room_schedule', function (Blueprint $table) {
            $table->dropForeign(['convener', 'room_option']);
        });

        Schema::dropIfExists('room_schedule');
    }
}