<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBenefitManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefits_management', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('benefit_id');
            $table->unsignedBigInteger('subscription_plan_id');
        });

        Schema::enableForeignKeyConstraints();

        Schema::table('benefits_management', function (Blueprint $table) {
            $table->foreign('benefit_id')->references('id')->on('benefits');
            $table->foreign('subscription_plan_id')->references('id')->on('subscription_plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('benefits_management', function (Blueprint $table) {
            $table->dropForeign(['benefit_id', 'subscription_plan_id']);
        });

        Schema::dropIfExists('benefits_management');
    }
}
