<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('subscription_plan_id');
            $table->date('begin');
            $table->date('end');
            $table->string('payment_reference')->nullable();
            $table->tinyInteger('payment_confirmed')->default(0);
            $table->unsignedBigInteger('entered_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->boolean('paused')->default(false);
            $table->integer('pause_length')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('subscription_plan_id')->references('id')->on('subscription_plans');
            $table->foreign('payment_reference')->references('id')->on('payments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
