@extends ('layouts.main')

@section('title', 'Confirmation')

@section ('content')
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                    <form action="{{ route('completeProfile') }}" method="post" name="profileCompletion">
                        @csrf
                        <ul id="progressbar">
                            <li class="active" id="dob"><h4>Birth Date</h4><hr></li>
                            <li class="" id="profession"><h4>Profession</h4><hr></li>
                            <li class="" id="personal_address"><h4>Personal Address</h4></li>
                        </ul>
                        <fieldset class="active" data-target="dob">
                            <h5 class="uppercase">When is Your Birthday?</h5>
                
                            @if ($errors->any())
                            <span class="help-block alert alert-danger" role="alert">
                                Please correct the following to proceed
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li> {{ $error }}</li>    
                                    @endforeach
                                </ul>
                            </span>
                            @endif
                            
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="feature feature-3">
                                        <div class="text-center">
                                            <i class="icon ti-calendar"></i>
                                            <h4>Enter Your Birth Day</h4>
                                        </div>
                                        <div class="form-group">
                                            <div class="ih-date-container">
                                                <label for="day">Day</label>
                                                <select name="day" id="" class="ih-select ih-date-picker ih-date-day">
                                                </select>
                                            </div>
                                            <div class="ih-date-container">
                                                <label for="month">Month</label>
                                                <select name="month" id="" class="ih-select ih-date-picker ih-date-month">
                                                </select>
                                            </div>
                                            <div class="ih-date-container" style="display: none;">
                                                <label for="year">Year</label>
                                                <select name="year" id="" class="ih-select ih-date-picker ih-date-year">
                                                </select>
                                            </div>
                                            <input type="hidden" name="birth_date">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="button" class="btn btn-next">
                                    <span>Next</span>
                                </button>
                            </div>
                        </fieldset> 
                        <fieldset class="" data-target="profession">
                            <h5 class="uppercase">Whats Your Profession?</h5>

                            <div class="form-group">
                                <select name="profession" id="">
                                    <option value="">--Choose Your Profession--</option>
                                    <option value="Web Developer">Web Developer</option>
                                    <option value="Digital Marketer">Digital Marketer</option>
                                    <option value="DevOps Engineer">DevOps Engineer</option>
                                    <option value="Blogger">Blogger</option>
                                    <option value="Social Entrepreneur">Social Entrepreneur</option>
                                    <option value="Graphic Designer">Graphic Designer</option>
                                    <option value="Real Estate Professional">Real Estate Professional</option>
                                    <option value="Lawyer">Lawyer</option>
                                    <option value="Educator">Educator</option>
                                    <option value="IT Consultant">IT Consultant</option>
                                    <option value="Software Tester">Software Tester</option>
                                    <option value="Photographer">Photographer</option>
                                    <option value="Wedding Planner">Wedding Planner</option>
                                </select>
                                <small>Enter Your Profession Here If Not Listed Above</small>
                                <input type="text" name="profession_other" placeholder="Profession">
                                <input type="text" name="company" placeholder="Company Name">
                                <input type="text" name="position" placeholder="Position">
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-next">
                                    <span>Next</span>
                                </button>
                            </div>
                        </fieldset>
                        <fieldset class="" data-target="personal_address">
                            <h5 class="uppercase">Your Personal Address</h5>

                            <div class="form-group">
                                <input type="text" name="addr_line1" placeholder="Street Address 1">
                                <input type="text" name="addr_line2" placeholder="Street Address 2">
                                <select name="town" id="">
                                    <option value="">--Select Your Area--</option>
                                    <option value='Abarenji'>Abarenji</option>
                                    <option value='Abule Egba'>Abule Egba</option>
                                    <option value='Abule Ijesha'>Abule Ijesha</option>
                                    <option value='Abule Okuta'>Abule Okuta</option>
                                    <option value='Adeniran Ogunsanya'>Adeniran Ogunsanya</option>
                                    <option value='Agbelekale'>Agbelekale</option>
                                    <option value='Agege'>Agege</option>
                                    <option value='Ajao Estate'>Ajao Estate</option>
                                    <option value='Ajegunle Boundry'>Ajegunle Boundry</option>
                                    <option value='Akesan'>Akesan</option>
                                    <option value='Alagbado'>Alagbado</option>
                                    <option value='Alakuko'>Alakuko</option>
                                    <option value='Alapere Ketu'>Alapere Ketu</option>
                                    <option value='Alimosho'>Alimosho</option>
                                    <option value='Allen'>Allen</option>
                                    <option value='Anthony'>Anthony</option>
                                    <option value='Badore'>Badore</option>
                                    <option value='Bakare Faro'>Bakare Faro</option>
                                    <option value='Balogun'>Balogun</option>
                                    <option value='Broad St.'>Broad St.</option>
                                    <option value='Bungalow Estate'>Bungalow Estate</option>
                                    <option value='Dolphin Estate'>Dolphin Estate</option>
                                    <option value='Dopemu'>Dopemu</option>
                                    <option value='Ebute Meta'>Ebute Meta</option>
                                    <option value='Egan'>Egan</option>
                                    <option value='Egbe'>Egbe</option>
                                    <option value='Egbeda'>Egbeda</option>
                                    <option value='Epe Tedo'>Epe Tedo</option>
                                    <option value='Fadeyi'>Fadeyi</option>
                                    <option value='Festac'>Festac</option>
                                    <option value='Idumota'>Idumota</option>
                                    <option value='Idumu'>Idumu</option>
                                    <option value='Ifako Agege'>Ifako Agege</option>
                                    <option value='Igando'>Igando</option>
                                    <option value='Ijedodo'>Ijedodo</option>
                                    <option value='Ijeshatedo'>Ijeshatedo</option>
                                    <option value='Iju Isaga'>Iju Isaga</option>
                                    <option value='Ikeja Oba Akran'>Ikeja Oba Akran</option>
                                    <option value='Ikosi'>Ikosi</option>
                                    <option value='Ikotun'>Ikotun</option>
                                    <option value='Ikoyi'>Ikoyi</option>
                                    <option value='Ipaja'>Ipaja</option>
                                    <option value='Isale Eko'>Isale Eko</option>
                                    <option value='Isheri Ofin'>Isheri Ofin</option>
                                    <option value='Isheri Oke'>Isheri Oke</option>
                                    <option value='Isheri Osun'>Isheri Osun</option>
                                    <option value='Isolo'>Isolo</option>
                                    <option value='Itire'>Itire</option>
                                    <option value='Ketu'>Ketu</option>
                                    <option value='Kirikiri Industrial'>Kirikiri Industrial</option>
                                    <option value='Lafiaji'>Lafiaji</option>
                                    <option value='Lawanson'>Lawanson</option>
                                    <option value='Magodo'>Magodo</option>
                                    <option value='Makoko'>Makoko</option>
                                    <option value='Marina'>Marina</option>
                                    <option value='Maryland'>Maryland</option>
                                    <option value='Meiran'>Meiran</option>
                                    <option value='Mende'>Mende</option>
                                    <option value='Murtala Muhammed Airport'>Murtala Muhammed Airport</option>
                                    <option value='Mushin'>Mushin</option>
                                    <option value='Obalende'>Obalende</option>
                                    <option value='Ogba Aguda'>Ogba Aguda</option>
                                    <option value='Ogudu'>Ogudu</option>
                                    <option value='Ojodu'>Ojodu</option>
                                    <option value='Ojokoro'>Ojokoro</option>
                                    <option value='Oko Oba Agege'>Oko Oba Agege</option>
                                    <option value='Olodi Apapa'>Olodi Apapa</option>
                                    <option value='Olowogbowo'>Olowogbowo</option>
                                    <option value='Olute / Navy Town'>Olute / Navy Town</option>
                                    <option value='Onikan'>Onikan</option>
                                    <option value='Onike'>Onike</option>
                                    <option value='Onipanu'>Onipanu</option>
                                    <option value='Opebi'>Opebi</option>
                                    <option value='Oregun'>Oregun</option>
                                    <option value='Oremeji Ifako'>Oremeji Ifako</option>
                                    <option value='Orile Igamu'>Orile Igamu</option>
                                    <option value='Oshodi'>Oshodi</option>
                                    <option value='Oworonshoki'>Oworonshoki</option>
                                    <option value='Oya Estate'>Oya Estate</option>
                                    <option value='Satelite Town'>Satelite Town</option>
                                    <option value='Shogunle'>Shogunle</option>
                                    <option value='Shomolu'>Shomolu</option>
                                    <option value='Surulere'>Surulere</option>
                                    <option value='Victoria Island'>Victoria Island</option>
                                    <option value='Yaba'>Yaba</option>
                                </select>

                                <select name="state" id="" disabled="disabled">
                                    <option value="Lagos">Lagos</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-submit">
                                    <span class="ih-submit-button-text">
                                        Submit
                                    </span>
                                    <span style="display:none;" class="confirm-success glyphicon glyphicon-ok"></span>
                                    <span style="display:none;" class="confirm-failure glyphicon glyphicon-remove"></span>
                                </button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <!--end of row-->
        </div>
        <div class="foundry_modal" data-time-delay="0">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="feature text-center">
                        <i class="ti-check-box icon icon-lg mb24 mb-xs-0"></i>
                        @if ($context == 'subscription')
                        <h1 class="large">Thanks for subscribing!</h1>
                        @else
                        <h1 class="large">Thanks for scheduling the use of our room!</h1>
                        @endif
                        <p class="mb40 mb-xs-24">
                            You will receive a confirmation email shortly.                                
                        </p>
                        <ul class="list-inline social-list">
                            <li>
                                <a href="https://twitter.com/hub_impression">
                                    <i class="icon icon-sm ti-twitter-alt"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/theimpressionhub">
                                    <i class="icon icon-sm ti-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/d_impressionhub/?hl=en">
                                    <i class="icon icon-sm ti-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end of container-->
    </section>
    @section ('jsScripts')
        @parent
    @endsection
@endsection