@php 
    $money = new \NumberFormatter('en_NG', \NumberFormatter::CURRENCY); 
@endphp

@extends ('layouts.main')

@section('title', 'Account')

@section ('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4 class="text-center">Hello {{ auth()->user()->fname }} 👋 </h4>
            </div>
        </div>

        @if($subscriptions->count() > 0)
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h5 class="text-center">Your Subscription History</h5>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table class="table-striped table-hover ih-info-table" id="ih-subscription">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Subscription Option</th>
                            <th>Starts</th>
                            <th>Ends</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Cost</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($subscriptions as $subscription)
                        <tr>
                            @php
                                $option      = ucwords(str_replace('_', ' ', $subscription->subscription_plan->plan_name));
                                $currentTime = Carbon\Carbon::now();
                                $start       = Carbon\Carbon::createFromFormat('Y-m-d H', $subscription->begin . ' 00');
                                $end         = Carbon\Carbon::createFromFormat('Y-m-d H', $subscription->end   . ' 21');

                                $startTime   = Carbon\Carbon::createFromTimeString($subscription->subscription_plan->starts_at, 'Africa/Lagos');
                                $endTime     = Carbon\Carbon::createFromTimeString($subscription->subscription_plan->ends_at,   'Africa/Lagos');
    
                                $status      = $currentTime->between($start, $end) ? 'Ongoing' : (($currentTime->isBefore($start)) ? 'Not Started': 'Ended');
                            @endphp
    
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $option }}</td>
                            <td>{{ $start->format('jS F') }}</td>
                            <td>{{ $end->format('jS F') ?? 'N/A' }}</td>
                            <td>{{ $startTime->format('h:i:s A') }}</td>
                            <td>{{ $endTime->format('h:i:s A') ?? 'N/A' }}</td>
                            <td>{{ $money->format( $subscription->subscription_plan->plan_cost ) }}</td>
                            <td>{{ $status }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>                   
                <hr>
            </div>
        </div>
        @else
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h5 class="text-center">No Subscription History</h5>
            </div>
        </div>
        @endif

        @if($roomBookings->count())
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h5 class="text-center">Your Room Booking History</h5>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">               
                <table class="table-striped table-hover ih-info-table" id="ih-room">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Room Type</th>
                            <th>Date</th>
                            <th>Starts</th>
                            <th>Ends</th>
                            <th>Cost</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($roomBookings as $roomBooking)
                        <tr>
                            @php
                                $option      = ucwords(str_replace('_', ' ', $roomBooking->roomOption->room_option_name));
                                $currentTime = Carbon\Carbon::now();

                                $startTime   = Carbon\Carbon::createFromTimeString($roomBooking->begin, 'Africa/Lagos');
                                $endTime     = Carbon\Carbon::createFromTimeString($roomBooking->end,   'Africa/Lagos');

                                $status      = $currentTime->between($startTime, $endTime) ? 'Ongoing' : (($currentTime->isBefore($startTime)) ? 'Not Started': 'Ended');
                            @endphp
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $option }}</td>
                            <td>{{ $startTime->format('jS F') }}</td>
                            <td>{{ $startTime->format('h:i:s A') }}</td>
                            <td>{{ $endTime->format('h:i:s A') }}</td>
                            <td>{{ $money->format($roomBooking->payment->amount/100) }}</td>
                            <td>{{ $status }}</td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>        
            </div>
        </div>
        @else 
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h5 class="text-center">No Room Booking History</h5>
        </div>
        @endif
    </div>
</section>
@endsection

@section('jsScripts')
    @parent
@endsection