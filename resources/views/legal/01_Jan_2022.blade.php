<section data-tou-version="01_Jan_2022">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3 class="uppercase">ImpressionHub <br> TERMS AND CONDITIONS OF USE (TOU)</h3>


                <h4>1. Acceptance of Terms</h4>
                <p>The services ImpressionHub provides to you, the undersigned (including but not limited to use of
                    office space, access to Internet, access to kitchenette, use of training facility, use of conference
                    room), are subject to the following Terms Of Use (TOU). ImpressionHub reserves the right to update
                    the TOU at any time without notice to you. An active ImpressionHub workspace subscription does not
                    create a tenancy but a prepaid usage license to use the provided amenities on a monthly or casual
                    basis.</p>
                <br>

                <h4>2. Description of Services</h4>
                <p>ImpressionHub may provide you with access to office space, Internet access, office equipment,
                    conference space, knowledge resources, and other services. The Services at all times are subject to
                    the TOU.</p>
                <br>

                <h4>3. No Unlawful or Prohibited Use</h4>
                <p>You will not use the Services for any purpose that is unlawful or prohibited by these terms,
                    conditions and notices. You may not use the Services in any manner that could damage, disable,
                    overburden, or impair any internet connection, or interfere with any other partyʼs use and enjoyment
                    of any Services. You may not attempt to gain unauthorized access to any Services, or accounts,
                    computer systems or networks connected to any ImpressionHub server or device, or to any of the
                    services, through hacking, password mining or any other means. You may not obtain or attempt to
                    obtain any materials or information through any means not intentionally made available through the
                    Services, nor should you post or download files that you know or should know are illegal or that you
                    have no rights to. You hereby represent and warrant that you have all requisite legal power and
                    authority to enter into and abide by the terms and conditions of this TOU and no further
                    authorization or approval is necessary. You further represent and warrant that your participation or
                    use of the Services will not conflict with or result in any breach of any license, contract,
                    agreement or other instrument or obligation to which you are a party.</p>
                <br>

                <h4>4. Use of services.</h4>
                <p>You agree that when participating in or using the Services, you will not:
                <ol type="a" style="margin-left: 2em;font-size: 0.95em;">
                    <li>Use the Services in connection with contests, pyramid schemes, chain letters, junk email,
                        spamming, or any duplicative or unsolicited message (commercial or otherwise);</li>
                    <li>Defame, abuse, harass, stalk, threaten or otherwise violate the legal rights (such as rights of
                        privacy and publicity) of others;</li>
                    <li>Publish, post, upload, distribute or disseminate any inappropriate, profane, defamatory,
                        obscene, indecent or unlawful topic, name, material or information on or through Impression Hub
                        networks, notice boards, events or Impression Hub spaces;</li>
                    <li>Upload, or otherwise make available, files that contain images, photographs, software or other
                        material protected by intellectual property laws, including, by way of example, and not as
                        limitation, copyright or trademark laws (or by rights of privacy or publicity) unless you own or
                        control the rights thereto or have received all necessary consent to do the same;</li>
                    <li>Use any material or information, including images or photographs, which are made available
                        through the Services in any manner that infringes any copyright, trademark, patent, trade
                        secret, or other proprietary right of any party;</li>
                    <li>Upload files that contain viruses, Trojan Horses, worms, time bombs, cancelbots, corrupted
                        files, or any other similar software or programs that may damage the operation of anotherʼs
                        computer or property of another;</li>
                    <li>Download any file(s) that you know, or reasonably should know, cannot be legally reproduced,
                        displayed, performed, and/or distributed in such manner;</li>
                    <li>Restrict or inhibit any other user from using and enjoying the Services;</li>
                    <li>Harvest or otherwise collect information about others, including email addresses, without the
                        authorization or consent of the disclosing party;</li>
                    <li>Violate any applicable laws or regulations;</li>
                    <li>Create a false identity for the purpose of misleading others.</li>
                    <li>Agree that ImpressHub Coworking Space, allocated workspaces are under 24-hour CCTV surveillance.
                        ImpressionHub will only provide CCTV footage based on the request of local law enforcer.</li>
                </ol>
                </p>
                <br>

                <h4>5. Confidentiality</h4>
                <p>You acknowledge and agree that during your participation in and use of the Services you may be
                    exposed to Confidential Information. “Confidential Information” shall mean all information, in whole
                    or in part, that is disclosed by the ImpressionHub or any participant or user of the Services or any
                    employee, affiliate, or agent thereof, that is nonpublic, confidential or proprietary in nature.</p>
                <br>

                <h4>6. Invoicing and Payment</h4>
                <p>The invoices are issued in advance, based on chosen membership option. Payment is on invoice date on
                    the beginning of the period. Payment for walk-in usage is made on the starting day of use. This will
                    take effect from January 2022.</p>
                <br>

                <h4>7. Renewals and Terminations</h4>
                <p>Once the payment for a period of time is done, no refunds are possible. In case of renewal of the
                    services, these terms and conditions are automatically renewed with consent of each party.
                    ImpressionHub reserves the right to terminate any Service at any time, immediately and without
                    notice, if you fail to comply with the TOU. Members may terminate this Agreement by giving a written
                    notice over email of termination as established in this Agreement.</p>
                <br>

                <h4>8. Participation In or Use of Services</h4>
                <p>You acknowledge that you are participating in or using the Services at your own free will and
                    decision. You acknowledge that the ImpressionHub does not have any liability with respect to your
                    access, participation in, use of the Services, or any loss of information, loss from theft or damage
                    of personal property on its premises, resulting from such participation or use. You are liable for
                    the full cost of any damages caused by you to any property in the space, owned either by the
                    ImpressionHub or its members.</p>
                <br>

                <h4>9. Space Eligibility</h4>
                <p>Only members who have registered for the use of a space are eligible for the use of the said space.
                    No other person is eligible to use the space until the tenure of usage has elapsed.</p>
                <br>

                <h4>10. Severability</h4>
                <p>In the event that any provision or portion of this TOU is determined to be invalid, illegal or
                    unenforceable for any reason, in whole or in part, the remaining provisions of this TOU shall be
                    unaffected thereby and shall remain in full force and effect to the fullest extent permitted by
                    applicable law.</p>
                <br>

                <h4>11. Assurance of Power</h4>
                <p>Due to the condition of the power resources of the country which we reside in, ImpressionHub is
                    unable to assure members of uninterrupted power during the hours of operation. ImpressionHub however
                    intends to do all within its capacity to ensure power outages are reduced to the barest minimum.</p>
                <br>

                <h4>12. Hours of Operation</h4>
                <p>The Impression Hub is operational between the hours of 9:00am and 9:00pm on working days (Mondays to
                    Fridays) and from 10:00am to 5:00pm on Saturdays. Members are encouraged to plan to leave on time
                    daily to avoid incidents where power to the facility is taken while still engrossed in work.</p>
                <br>

                <h4>13. Hub Credits</h4>
                <p>You may use credits for training room or conference room or other Services in certain of our Premises
                    during the applicable period of use for the credits. Use of our workspaces and conference rooms in
                    excess of any credits would be subject to the standard fees for such Services. Such Fees are subject
                    to change from time to time.</p>
                <br>

                <h4>14. Damage To Property</h4>
                <p>The member shall not alter, install, remove nor damage any furniture, fixtures, decorative materials,
                    office equipment, IT cabling and telecommunications tools from within the Coworking Space, allocated
                    desks, dedicated workspaces and common areas, in which the Members have had access. ImpressionHub
                    shall have the right to claim compensation from the Member for repairs, replacement, loss or
                    damages.</p>
                <br>

                <h4>15. Use of Address</h4>
                <p>Subject to availability, you may choose to receive mail and packages at our Coworking Space within
                    the duration of your service. Within the duration of your active service we will accept mail and
                    deliveries on your behalf during our regular working hours on Business Days. We have no obligation
                    to store them longer than seven (7) days of our receipt or if we receive mail or packages after you
                    terminate your subscription or after the services expiration. This feature is to allow you accept
                    business correspondence from time to time. It is not meant for an address for the receipt of
                    merchandise or personal goods. As such, we are not under any obligation to receive bulk or oversized
                    mail or packages on your behalf.</p>
                <br>

                <h4>16. Use of Conference Room or Training Room</h4>
                <p>The member shall use the conference room only on the basis that the intended room for use has been
                    scheduled for prior to the target time required by the member using ImpressionHub’s meeting
                    scheduler. A room booking is concluded only when booking has been paid for.</p>
                <br>

                <h4>17. Use of Relaxation Corner</h4>
                <p>
                    A member shall use the ImpressionHub’s relaxation corner in the following ways
                <ol type="a" style="margin-left: 2em;font-size: 0.95em;">
                    <li>To receive guests</li>
                    <li>To rest and relax in a considerate manner</li>
                    <li>To read or review a book or documents</li>
                    <li>To have casual conversations with other members</li>
                </ol>
                All members are to be considerate of others during their use of the relaxation corner in relation to the
                space taken and the time used in the corner.
                The relaxation corner should not be used for lectures, meetings, to receive guests indefinitely or to
                store personal belongings.
                </p>
                <br>

                <h4>18. Decorum</h4>
                <p>Members should be mindful and precise while engaging with other co-workers, not to use passages for
                    calls and meetings. This may disturb you and other co-workers and can hamper their
                    productivity.</p>
                <br>

                <h4>19. Kitchenette </h4>
                <p>ImpressionHub provides a furnished kitchenette for the convenience of members. We provide a limited
                    quantity of coffee and tea for free to members. Members are expected to apply moderation in the
                    consumption of these beverages.</p>
                <p>Members are also expected to wash their utensils like mugs, cutlery and plates after using them.</p>
                <br>

                <h4>20. Leave of Absence </h4>
                <p>A member cannot have her or his active subscription paused or extended due to a leave of absence.
                </p>
                <br>

                <h4>21. Keycards </h4>
                <p>As a member of ImpressionHub, you cannot transfer your keycard or other access device or credentials
                    to anyone else, and you are not allowed to make any copies of any keys, keycards, or other means of
                    entry to our Premises (each, an “Access Device”). </p>
                <p>You are responsible for maintaining the confidentiality of your password and security of your Access
                    Device. You must promptly notify us if you suspect your password or Access Device has been
                    compromised. The Access Devices remain our property, and you must return them immediately upon
                    termination or expiration of your Subscription or Hot Desk. You will be charged a replacement fee
                    for any lost or damaged Access Devices.</p>
                <br>

                <h4>22. Sub-lease </h4>
                <p>You shall not re-allocate or sublease your service allocation to another party once you have been
                    provisioned with the service.</p>
                <br>

                <h4>23. Indemnification </h4>
                <p>You will indemnify and hold harmless ImpressionHub from and against any and all claims, liabilities,
                    damages and expenses (“Claims”) including reasonable attorneys’ fees, resulting from any breach of
                    these TOU by you or your employees or guests, or your or their invitees or any of your or their
                    actions or omissions, and ImpressionHub will have sole control over the defense of any such Claims.
                    You are responsible for the actions of and all damages caused by all persons that you or your guests
                    invite to enter our premises.</p>
                <br>
            </div>
        </div>
    </div>
</section>