@extends('layouts.main')

@section('title', 'Home')

@section('content')
    <section class="fullscreen image-bg parallax background-multiply">
        <div class="background-image-holder bg-image-adjust-right">
            <img alt="image" class="background-image" src="{{ asset('img/Building.jpg') }}" />
        </div>
        <div class="container v-align-transform">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="thin mb0">A Coworking Space in the heart of Akoka, powering startups and freelancers.</h1>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
        <div class="align-bottom text-center">
            <a class="btn btn-white mb32" href="/contact">Schedule A Visit</a>
            <ul class="list-inline social-list mb24">
                <li>
                    <a href="https://twitter.com/hub_impression">
                        <i class="ti-twitter-alt"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.facebook.com/theimpressionhub">
                        <i class="ti-facebook"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/d_impressionhub/?hl=en">
                        <i class="ti-instagram"></i>
                    </a>
                </li>
            </ul>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h6 class="uppercase">About Impression Hub</h6>
                    <hr class="mb160 mb-xs-24">
                </div>
            </div>
            <!--end of row-->
            <div class="row">
                <div class="col-md-10">
                    <h1 class="thin">Providing Coworking Services for Startups & Freelancers since 2017</h1>
                </div>
            </div>
            <!--end of row-->
            <div class="row mb160 mb-xs-0">
                <div class="col-md-6 col-sm-8">
                    <p class="lead">
                        Impression Hub has been providing quality Coworking experiences for Startups since August 2017, enabling them focus on what trully matters, Their product/service delivery.
                    </p>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="pt240 pb240 image-bg parallax">
        <div class="background-image-holder bg-image-adjust-right">
            <img alt="image" class="background-image" src="{{ asset('img/hub.jpg') }}"/>
        </div>
    </section>
    <section class="bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h6 class="uppercase">Offerings</h6>
                    <hr class="mb160 mb-xs-24">
                </div>
            </div>
            <!--end of row-->
            <div class="row">
                <div class="col-md-10">
                    <h1 class="thin">Coworking, Meetings, Training & Events</h1>
                </div>
            </div>
            <!--end of row-->
            <div class="row mb160 mb-xs-0">
                <div class="col-md-6 col-sm-8">
                    <p class="lead">
                        Impression Hub has put together a bundle of services aimed at eliminating the infrastructure costs related with getting a startup up and running.
                    </p>
                </div>
            </div>
            <!--end of row-->
            <div class="row">
                <div class="col-md-4 col-sm-6 mb-xs-24">
                    <i class="ti-desktop icon mb32"></i>
                    <h6 class="uppercase">Coworking</h6>
                    <ul>
                        <li>Free Conference Room Use</li>
                        <li>Fast Internet Access</li>
                        <li>Free Breakfast</li>
                        <li>Free Coffee</li>
                        <li>Parking</li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-6 mb-xs-24">
                    <i class="ti-calendar icon mb32"></i>
                    <h6 class="uppercase">Meetings</h6>
                    <ul>
                        <li>Conference Room With 6 Person Capacity</li>
                        <li>Fast Internet Access</li>
                        <li>Conducive Ambience</li>
                        <li>Free Coffee</li>
                        <li>Projector</li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-6 mb-xs-24">
                    <i class="ti-marker-alt icon mb32"></i>
                    <h6 class="uppercase">Training</h6>
                    <ul>
                        <li>Training Room With 20 Person Capacity</li>
                        <li>Fast Internet Access</li>
                        <li>Conducive Ambience</li>
                        <li>Projector</li>
                    </ul>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="image-bg bg-dark parallax overlay background-multiply pt160 pb160 pt-xs-80 pb-xs-80">
        <div class="background-image-holder bg-image-adjust-right">
            <img alt="image" class="background-image" src="{{ asset('img/standard.jpg') }}" />
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-8">
                    <i class="ti-quote-left icon icon-sm mb16"></i>
                    <h3 class="mb32">We endeavor to provide our clients with the tools and support needed to focus on their business.</h3>
                    <p>
                        Our environment has been designed to reduce distractions to the bearest minimum to ensure the productivity of our Coworkers.
                    </p>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h6 class="uppercase">Updates & Insights</h6>
                    <hr class="mb160 mb-xs-24">
                </div>
            </div>
            <!--end of row-->
            <div class="row">
                <div class="col-md-10">
                    <a class="h1 thin color-primary inline-block mb24" href="#">@ImpressionHub</a>
                </div>
            </div>
            <!--end of row-->
            <div class="row mb160 mb-xs-0">
                <div class="col-md-6 col-sm-8">
                    <p class="lead">
                        Engagement and speedy remediation of issues are what make us stand-out. Follow us on Twitter for announcements.
                    </p>
                </div>
            </div>
            <!--end of row-->
            <div class="row">
                <div class="twitter-feed thirds">
                    <div class="tweets-feed" data-feed-name="hub_impression">
                    </div>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="fullscreen image-bg parallax background-multiply">
        <div class="background-image-holder">
            <img alt="image" class="background-image" src="{{ asset('img/events.jpg') }}" />
        </div>
        <div class="container v-align-transform">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h1 class="large mb0">See you soon.</h1>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection

@section('jsScripts')
    @parent
@endsection