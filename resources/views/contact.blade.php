@extends ('layouts.main')

@section('title', 'Contact Us')

@section ('content')
    <div class="main-container">
        <section class="page-title page-title-4 bg-secondary">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="uppercase mb0">Contact Us</h3>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <section class="image-square left">
            <div class="col-md-6 image">
                <div class="background-image-holder">
                    <img alt="image" class="background-image" src="img/coworkers.jpg" />
                </div>
            </div>
            <div class="col-md-6 col-md-offset-1 content">
                <h4 class="uppercase">Get In Touch</h4>
                <p>
                    If you're unsure about anything or you need more info about our Coworking Services, Virtual Office Services, Meetings or Event Bookings, just shoot us an email and we'll get right back to you.
                </p>
                <hr>
                <p>
                    <strong>Address:</strong> 3, St. Finbarr's College Road
                    <br /> Akoka, Yaba
                    <br /> Lagos
                </p>
                <p>
                    <strong>E:</strong> <a href="mailto:info@impressionhub.ng">info@impressionhub.ng</a>
                    <br />
                    <strong>P:</strong> <a href="tel:+234 905 274 6836">0905 274 6836</a>
                    <br />
                </p>
            </div>
        </section>
        <section class="image-square right">
            <div class="col-md-6 p0 image">
                <div class="map-holder background-image-holder">
                    <iframe width="100%" height="320px" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJi1lG9f2MOxARRCzgnosDX_o&key=AIzaSyAj3swEgvzqY1hiEE-9r4rLRFSaQ_zdgv0"></iframe>
                </div>
            </div>
            <div class="col-md-6 content">
                <form method="post" action="/contact" class="form-email" data-success="Thanks for your submission, we will be in touch shortly." data-error="Please fill all fields correctly.">
                    @csrf
                    <h5 class="uppercase text-center">Send A Message</h5>
                    <input type="text" class="validate-required" name="name" placeholder="Your Name" />
                    <input type="text" class="validate-required validate-email" name="email" placeholder="Email Address" />
                    <textarea class="validate-required" name="message" rows="4" placeholder="Message"></textarea>
                    <button type="submit">Send Message</button>
                </form>
            </div>
        </section>
    </div>
    @section('jsScripts')
        @parent
    @endsection
@endsection