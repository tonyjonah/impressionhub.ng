@extends ('layouts.main')

@section('title', 'Showcase')

@section ('content')
    <div class="main-container">
        <section class="page-title page-title-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="uppercase mb0">Project Title</h3>
                    </div>
                    <div class="col-md-6 text-right">
                        <ol class="breadcrumb breadcrumb-2">
                            <li>
                                <a href="index.html">Home</a>
                            </li>
                            <li>
                                <a href="#">Portfolio</a>
                            </li>
                            <li class="active">Project Title</li>
                        </ol>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <section class="pt0 pb40">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 col-xs-6 mb-xs-24">
                        <span>Client:</span>
                        <h6 class="uppercase mb0">Medium Rare Inc.</h6>
                    </div>
                    <div class="col-sm-3 col-xs-6 mb-xs-24">
                        <span>Our Role:</span>
                        <h6 class="uppercase mb0">Design, Consultation</h6>
                    </div>
                    <div class="col-sm-3 col-xs-6 mb-xs-24">
                        <span>Turnaround:</span>
                        <h6 class="uppercase mb0">May - July</h6>
                    </div>
                    <div class="col-sm-3 col-xs-6 mb-xs-24">
                        <span>Category:</span>
                        <h6 class="uppercase mb0">Rebranding</h6>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <section class="pt0 pb0">
            <img alt="Feature Image" class="col-xs-12 p0" src="img/project-case-study-1.jpg" />
        </section>
        <section>
            <div class="container">
                <div class="row mb64 mb-xs-24">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <h2 class="uppercase mb16">The Brief</h2>
                        <p class="lead mb48">A short description to add some contetxt.</p>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
                        </p>
                    </div>
                </div>
                <!--end of row-->
                <div class="row mb80 mb-xs-40">
                    <div class="col-sm-6 mb-xs-16">
                        <img alt="Project Image" src="img/project-case-study-2.jpg" />
                    </div>
                    <div class="col-sm-6">
                        <img alt="Project Image" src="img/project-case-study-3.jpg" />
                    </div>
                </div>
                <!--end of row-->
                <div class="row mb80 mb-xs-40">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <h6 class="uppercase">An Additional Description</h6>
                        <p class="lead">
                            Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam.
                        </p>
                    </div>
                </div>
                <!--end of row-->
                <div class="row mb80 mb-xs-24">
                    <div class="col-sm-12 text-center">
                        <img alt="Project Image" src="img/project-case-study-4.jpg" />
                    </div>
                </div>
                <!--end of row-->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <h2 class="uppercase mb16">The Result</h2>
                        <p class="lead mb48">An 80% increase in sales for Medium Rare.</p>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
                        </p>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <footer class="footer-1 bg-dark">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <img alt="Logo" class="logo" src="img/logo-light.png" />
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h6 class="title">Recent Posts</h6>
                            <hr>
                            <ul class="link-list recent-posts">
                                <li>
                                    <a href="#">Hugging pugs is super trendy</a>
                                    <span class="date">February
                                        <span class="number">14, 2015</span>
                                    </span>
                                </li>
                                <li>
                                    <a href="#">Spinning vinyl is oh so cool</a>
                                    <span class="date">February
                                        <span class="number">9, 2015</span>
                                    </span>
                                </li>
                                <li>
                                    <a href="#">Superior theme design by pros</a>
                                    <span class="date">January
                                        <span class="number">27, 2015</span>
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <!--end of widget-->
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h6 class="title">Latest Updates</h6>
                            <hr>
                            <div class="twitter-feed">
                                <div class="tweets-feed" data-feed-name="hub_impression">
                                </div>
                            </div>
                        </div>
                        <!--end of widget-->
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h6 class="title">Instagram</h6>
                            <hr>
                            <div class="instafeed" data-user-name="d_impressionhub">
                                <ul></ul>
                            </div>
                        </div>
                        <!--end of widget-->
                    </div>
                </div>
                <!--end of row-->
                <div class="row">
                    <div class="col-sm-6">
                        <span class="sub">&copy; Copyright 2017 - Impression Hub</span>
                    </div>
                    <div class="col-sm-6 text-right">
                        <ul class="list-inline social-list">
                            <li>
                                <a href="#">
                                    <i class="ti-twitter-alt"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="ti-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="ti-dribbble"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="ti-vimeo-alt"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--end of container-->
            <a class="btn btn-sm fade-half back-to-top inner-link" href="#top">Top</a>
        </footer>
    </div>
    @section ('jsScripts')
        @parent
    @endsection
@endsection