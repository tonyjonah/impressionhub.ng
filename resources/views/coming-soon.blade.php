@extends ('layouts.main')

@section('title', 'Coming Soon')

@section ('content')
    <div class="main-container">
        <section class="cover fullscreen image-bg overlay">
            <div class="background-image-holder">
                <img alt="image" class="background-image" src="img/premium3.jpg" />
            </div>
            <div class="container v-align-transform">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">
                        <div class="feature bordered text-center">
                            <h3 class="uppercase">Feature Will Be Available Soon</h3>
                            <p>
                                We'll be launching this feature shortly. Please check back soon!
                            </p>
                        </div>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
    </div>
    @section ('jsScripts')
        @parent
    @endsection
@endsection