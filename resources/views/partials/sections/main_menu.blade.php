<ul class="menu">
    <li class="">
        <a href="/">
            Home
        </a>
    </li>
    <li class="has-dropdown">
        <a href="/services">
            Service Offerings
        </a>
        <ul>
            <li>
                <a href="/services#daily_use_costs">Daily Subscription Plans</a>
            </li>
            <li>
                <a href="/services#weekly_use_costs">Weekly Subscription Plans</a>
            </li>
            <li>
                <a href="/services#monthly_use_costs">Monthly Subscription Plans</a>
            </li>
            <li>
                <a href="/services#conference_info">Conference Room Booking</a>
            </li>
            <li>
                <a href="/services#training_info">Training Room Booking</a>
            </li>
        </ul>
    </li>
    <li class="has-dropdown">
        <a href="#" target="_blank" rel="noopener noreferrer">
            Bookings
        </a>
        <ul class="">
            <li>
                <a href="/coming">
                    Event Listing
                </a>
            </li>
            <li>
                <a href="/coming">
                    Meetings
                </a>
            </li>
            <li>
                <a href="/coming">
                    Trainings
                </a>
            </li>
        </ul>
    </li>
    <li class="has-dropdown">
        <a href="#">
            Coworkers
        </a>
        <ul>
            <li class="">
                <a href="/coming">
                    Members Index
                </a>
            </li>
            <li class="">
                <a href="/coming">
                    Showcase
                </a>
            </li>
            <li class="">
                <a href="/coming">
                    Gallery
                </a>
            </li>
            @guest
            <li class="">
                <a href="/login">
                    Login
                </a>
            </li>
            @endguest
            <li class="">
                <a href="/coming">
                    FAQ
                </a>
            </li>
        </ul>
    </li>
    <li class="">
        <a href="/blog">
            Blog
        </a>
    </li>
    <li class="">
        <a href="/contact">
            Contact
        </a>
    </li>
    @guest
        
    @else
    <li class="has-dropdown">
        <a href="#" target="_blank" rel="noopener noreference">
            <img src="{{ asset('storage/profile_photos/' . auth()->user()->photo ) }}" alt="{{auth()->user()->fname}}" style="width:32px;height:32px;border-radius:50%">
        </a>
        <ul>
            <li class=""><a href="{{ route('viewProfile') }}">Profile</a></li>
           <li class=""><a href="{{ route('serviceListing') }}">Account</a></li>
            @if (auth()->user()->email_verified_at == null)
            <li class="">
                <a href="{{ route('verification.notice') }}">Verify Your Email</a>
            </li>
            @endif
            <li class="">
                <a class="" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
        </ul>
    </li>
    @endguest
</ul>