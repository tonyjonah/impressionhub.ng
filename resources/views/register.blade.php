@extends ('layouts.main')

@section ('content')
    <div class="main-container">
        <section class="cover fullscreen image-bg overlay">
            <div class="background-image-holder">
                <img alt="image" class="background-image" src="img/home2.jpg" />
            </div>
            <div class="container v-align-transform">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="feature bordered text-center">
                            <h4 class="uppercase">Register Here</h4>
                            <form class="text-left">
                                <input type="text" name="fname" placeholder="First Name" />
                                <input type="text" name="lname" placeholder="Last Name" />
                                <input type="text" name="email" placeholder="Email Address" />
                                <input type="text" name="phone" placeholder="Phone Number">
                                <input type="password" placeholder="Password" />
                                <input type="password" placeholder="Confirm Password" />
                                <input type="submit" value="Register" />
                            </form>
                            <p class="mb0">By signing up, you agree to our
                                <a href="#">Terms Of Use</a>
                            </p>
                        </div>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
    </div>
    @section('jsScripts')
        @parent
    @endsection
@endsection