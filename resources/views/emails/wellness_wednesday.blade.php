<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]-->
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Wellness Wednesday</title>
    <style>
    @import url('https://fonts.googleapis.com/css2?family=Fira+Sans:wght@300&family=League+Gothic&family=Moon+Dance&family=Noto+Sans:wght@200;300&family=Roboto:wght@700&family=Sevillana&family=Sofia+Sans:wght@300&family=Work+Sans:ital,wght@0,100;0,200;0,300;1,100&display=swap');
        * {
            margin: 0;
            padding: 0;
            border: 0;
        }

        body {

            background-color: whitesmoke;
            color: floralwhite;
            font-size: 19px;
            max-width: 800px;
            margin: 0 auto;
            padding: 3%;
        }

        #logo {
            width: 50%;
            margin: auto;
        }

        img {
            max-width: 100%;
        }

        header {
            width: 98%;
        }

        #wrapper {
            background-color: #293036;
            border-radius: 5px;

        }


        #social {
            margin: 3% 2% 4% 3%;
            list-style-type: none;
        }

        #social>li {
            display: inline;
        }

        #social>li>a>img {
            max-width: 25px;
        }

        

        .col-one,
        .footies,
        .btn {
            font-family: 'Work Sans', sans-serif;
        }

        .headies {
            font-family: 'Noto Sans', sans-serif;
            text-align: center;
            padding-top: 2%;
        }

        .peacie {
            color: #1c2e14;
            font-family: 'Moon Dance', cursive;
        }

        .flows {
            font-family: 'Work Sans', sans-serif;
            text-align: center;
            text-transform: uppercase;
            font-size: large;
        }

        .paragraphs {
            font-family: 'Fira Sans', sans-serif;
            text-align: justify;
            font-size: smaller;
            padding: 1% 10% 5% 10%;
        }

        p {
            padding-bottom: 1em;
        }

        h1,
        h2,
        p {
            margin: 3%;
        }

        .btn-container {
            display: flex;
            justify-content: center;
        }

        .btn {
            margin: 0 2% 4% 0;
            background-color: rgba(33, 70, 199, 0.6);
            text-decoration: none;
            color: whitesmoke;
            font-weight: 800;
            padding: 8px 12px;
            border-radius: 8px;
            letter-spacing: 2px;
        }

        .spanner {
            color: #8cbd18;
        }

        hr {
            height: 1px;
            background-color: black;
            clear: both;
            width: 96%;
            margin: auto;
        }

        #contact {
            text-align: center;
            padding-bottom: 3%;
            line-height: 16px;
            font-size: 12px;
        }
    </style>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="">

</head>

<body>
    <div>
        <header>
            <div id="logo">
                <img src="https://www.impressionhub.ng/img/logo-dark.png" alt="logo">
            </div>
        </header>
        <!-- background-color: #d8dada; body color-->
    </div>
    <div id="wrapper">
        <header>
            <h1 class="headies">🌴 WELLNESS WEDNESDAY 🌞</h1>
        </header>
        <div id="banner">
            <img src="https://www.impressionhub.ng/img/med1.jpg" alt="">
        </div>
        <div class="col-one">
            <h2 class="flows">Stay in the <span class="spanner">Flow!</span> 😇</h2>

            <div class="paragraphs">
                <p>
                    As the week comes to an end, it's important to stay focused and remain in the flow. When we're in 
                    the flow, we're fully engaged in what we're doing and are able to accomplish more in less time. 
                    This can lead to increased productivity, creativity and overall satisfaction in our work.
                </p>
    
                <p>
                    One way to stay in the flow is to set clear and specific goals for the day or week. Having a 
                    clear understanding of what needs to be accomplished can help you stay focused and motivated. 
                    Additionally, it's important to eliminate distractions and create a conducive work environment. 
                    This can be achieved by turning off notifications on your phone, closing unnecessary tabs on your 
                    computer and creating a comfortable and organized workspace.
    
                <p>
                    It's also important to take regular breaks to rest and recharge. This can help to prevent burnout 
                    and increase your ability to stay in the flow. So, as the week comes to an end, remember to stay 
                    focused, set specific goals, eliminate distractions, create a conducive work environment and take 
                    regular breaks. With these tips in mind, you'll be able to make the most of your time and achieve great results.
                </p>
            </div>

            <div class="btn-container">
                <a href="https://www.impressionhub.ng/" class="btn">Visit Us</a>
            </div>

            <hr />

            <footer id="contact">
                <p>
                    Impression Hub <br>
                    3, St Finbarr's College Road<br>
                    Pako Bus Stop, Akoka, Lagos <br>
                    info@impressionhub.ng
                </p>
                <div>
                    <ul id="social">
                        <li>
                            <a href="https://twitter.com/hub_impression" target="_blank">
                                <img src="https://www.impressionhub.ng/img/tw-color.png" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/d_impressionhub/?hl=en" target="_blank">
                                <img src="https://www.impressionhub.ng/img/in-color.png" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/theimpressionhub" target="_blank">
                                <img src="https://www.impressionhub.ng/img/fb-color.png" alt="">
                            </a>
                        </li>
                    </ul>
                </div>
            </footer>

        </div>

    </div>

    <script src="" async defer></script>
</body>

</html>