<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]-->
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Friday Email</title>
    <style>

        @import url('https://fonts.googleapis.com/css2?family=Fira+Sans:wght@300&family=Lato:wght@300&family=League+Gothic&family=Montserrat:wght@200&family=Moon+Dance&family=Noto+Sans:wght@200;300&family=Open+Sans:wght@300&family=Roboto:wght@700&family=Sevillana&family=Sofia+Sans:wght@300&family=Work+Sans:ital,wght@0,100;0,200;0,300;1,100&display=swap');
        
        * {
            margin: 0;
            padding: 0;
            border: 0;
        }

        body {

            background-color: black;
            font-size: 19px;
            max-width: 800px;
            margin: 0 auto;
            padding: 3%;
        }

        #logo {
            width: 50%;
            margin: auto;
        }

        img {
            max-width: 100%;
        }

        header {
            width: 98%;
        }

        #wrapper {
            background-color: #f0f6fb;
            border-radius: 5px;
        }


        #social {
            margin: 3% 2% 4% 3%;
            list-style-type: none;
        }

        #social>li {
            display: inline;
        }

        #social>li>a>img {
            max-width: 25px;
        }

        

        .col-one,
        .footies,
        .btn {
            font-family: 'Work Sans', sans-serif;
        }

        .headies {
            font-family: 'Noto Sans', sans-serif;
            text-align: center;
            padding-top: 2%;
        }

        .go-emoji {
            color: red;
        }

        .peacie {
            color: #1c2e14;
            font-family: 'Moon Dance', cursive;
        }

        .grins {
            font-family: 'Work Sans', sans-serif;
            text-align: center;
            text-transform: uppercase;
            font-size: large;
        }

        .grinny {
            color: purple;
        }

        .paragraphs {
            font-family: 'Open Sans', sans-serif;
            text-align: justify;
            font-size: smaller;
            padding: 0.5% 10% 2% 10%;
        }

        .e-images {
            width: 50%;
            margin: auto;
            padding-bottom: 2%;
        }
        .holics {
            max-width: 500px;
            margin: auto;
            text-align: justify;
        }
        .events-header {
            text-align: center;
            font-size: large;
        }

        .e-graphs {
            text-align: center;
            font-size: smaller;
        }


        .dis-parent {
            width: 40%;
            margin: auto;
        }

        .disclaimer {
            font-size: x-small;
            text-align: center;
        }

        .dis-pad {
            margin-top: -3%;
        }

        .linkie {
            text-decoration: none;
            color: rgb(230, 87, 99);
        }

        p {
            padding-bottom: 1em;
        }

        h1,
        h2,
        p {
            margin: 3%;
        }

        .btn-container {
            display: block;
            width: 10em;
            margin-left: auto;
            margin-right: auto;
            margin-bottom: 1em;
        }

        .btn {
            margin: 0 2% 4% 0;
            background-color: rgba(33, 70, 199, 0.6);
            text-decoration: none;
            color: whitesmoke;
            font-weight: 800;
            padding: 8px 12px;
            border-radius: 8px;
            letter-spacing: 2px;
        }



        hr {
            height: 1px;
            background-color: black;
            clear: both;
            width: 96%;
            margin: auto;
        }

        #contact {
            text-align: center;
            padding-bottom: 3%;
            line-height: 16px;
            font-size: 12px;
        }
    </style>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="">

</head>

<body>
    <div>
        <header>
            <div id="logo">
                <img src="https://impressionhub.ng/img/logo-light.png" alt="logo">
            </div>
        </header>
        <!-- background-color: #d8dada; body color-->
    </div>
    <div id="wrapper">
        <header>
            <h1 class="headies">Turn up Friday! 🎉</h1>
        </header>
        <div id="banner">
            <img src="https://impressionhub.ng/img/31.03.230.jpeg" alt="">
        </div>
        <div class="col-one">
            <h2 class="grins">Co-working Space Etiquette<span class="go-emoji">🤼</span></h2>

            <div class="paragraphs">
                <p>Co-working spaces have become increasingly popular over the years, providing professionals with a collaborative and 
                    flexible workspace. However, with different individuals sharing the same space, co-working space etiquette is critical 
                    to ensure everyone has a comfortable and productive environment.
                </p>

                <p>
                    
                    It is essential to maintain a quiet and professional environment in co-working spaces. Loud phone conversations and music can be 
                    disruptive and disturb others' work. If you need to make a phone call, take it outside or to a designated phone booth.

                </p>
               
                <p>
                    Secondly, respect others' privacy and workspace. Avoid using others' equipment or disturbing their workspace without permission. 
                    Keep your workspace clean and tidy, and avoid taking up more space than necessary.
                </p>
                <p>
                    Thirdly, co-working spaces are collaborative environments, and networking is encouraged. However, be respectful of others' time and 
                    avoid interrupting their work. If you want to start a conversation, be sure to introduce yourself and ask if it's a good time to chat.
                </p>
                <p>
                    Lastly, be mindful of the community guidelines and rules. Each co-working space has its own set of guidelines, and it is essential to 
                    follow them to maintain a professional and productive environment.
                </p>
                <p>
                    In summary, co-working space etiquette is about being respectful and considerate of others. By following these simple guidelines, co-working 
                    spaces can be a great environment for collaboration and productivity.
                </p>
                <p>
                    It's the weekend again, we have curated a list of events that you can attend this weekend.
                </p>
            </div>
            <div class="events">
                <div>
                    <h2 class="events-header">The Rooftop XP</h2>
                    <div class="e-images"><img src="https://impressionhub.ng/img/31.03.231.jpg" alt="Rooftop xp picture"></div>
                    <p class="e-graphs">Learn more about this event by <span><a href="https://turnuplagos.com/01-apr-2023-the-rooftop-xp/" target="_blank" class="linkie">clicking here</a></span></p>
                </div>
                <div>
                    <h2 class="events-header">Unikorn's Game Night</h2>
                    <div class="e-images"><img src="https://impressionhub.ng/img/31.03.232.jpg" alt="Game night picture"></div>
                    <p class="e-graphs">Learn more about this event by <span><a href="https://turnuplagos.com/01-apr-2023-unikorns-game-night/" target="_blank" class="linkie">clicking here</a></span></p>
                </div>
                <div>
                    <h2 class="events-header">Storytelling</h2>
                    <div class="e-images"><img src="https://impressionhub.ng/img/31.03.233.jpg" alt="Story telling picture"></div>
                    <p class="e-graphs">Learn more about this event by <span><a href="https://www.eventbrite.com/e/storytelling-tickets-598273079887?aff=ebdssbcitybrowse" target="_blank" class="linkie">clicking here</a></span></p>
                </div>
                <div>
                    <h2 class="events-header">Amma Abena Live</h2>
                    <div class="e-images"><img src="https://impressionhub.ng/img/31.03.234.jpg" alt="Abena Live Picture"></div>
                    <p class="e-graphs">Learn more about this event by <span><a href="https://turnuplagos.com/01-apr-2023-amma-abena-live/" target="_blank" class="linkie">clicking here</a></span></p>
                </div>
            </div>
            <p class="paragraphs">
                Impressionhub is a unique co-working space that blends work and play in a fun and lively environment. 
                We have a vibrant community of entrepreneurs, freelancers, and creatives who share a passion for innovation.
            </p>

            <div class="btn-container">
                <a href="https://impressionhub.ng/" class="btn">Pay us a Visit</a>
            </div>

            <hr />

            <footer id="contact">
                <p>
                    Impression Hub <br>
                    3, St Finbarr's College Road<br>
                    Pako Bus Stop, Akoka, Lagos <br>
                    info@impressionhub.ng
                </p>
                <div>
                    <ul id="social">
                        <li>
                            <a href="https://twitter.com/hub_impression" target="_blank">
                                <img src="https://impressionhub.ng/img/tw-color.png" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/d_impressionhub/?hl=en" target="_blank">
                                <img src="https://impressionhub.ng/img/in-color.png" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/theimpressionhub" target="_blank">
                                <img src="https://impressionhub.ng/img/fb-color.png" alt="">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="dis-parent">
                    <h1 class="disclaimer">Disclaimer</h1>
                    <p class="disclaimer dis-pad">We are not affiliated with any of these event platforms or planners. 
                        Please conduct thorough research before attending any events and use your own discretion.
                    </p>
                </div>
            </footer>

        </div>

    </div>

    <script src="" async defer></script>
</body>

</html>