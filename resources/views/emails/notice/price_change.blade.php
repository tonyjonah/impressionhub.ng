@component('mail::message')

@component('mail::panel')
<h4>🚨 Notice of Upcoming Price Changes</h4>
@endcomponent

@php
    $fmt = numfmt_create( 'en_NG', \NumberFormatter::CURRENCY );
@endphp

<p>
    ImpressionHub appreciates all her customers and their continued patronage. To say that we are grateful for the opportunity to serve you is an understatement. We are working hard to put in place utilities to ensure you are proud of your workspace and also to make sure you have a wonderful working experience at our hub.
</p>

<p>
    It is not easy to deliver quality work experiences in our country, but this is a mandate we have given ourselves, to ensure all our customers are given the best possible treatment.
</p>

<p>
    Some challenges have presented themselves in recent months. A few of these include the rising cost of diesel, the increasing cost of power, and the general increase of the cost of utilities in the country. As we prepare this, the current cost of diesel is <strong style="font-weight: 700">{{ $fmt->format(650.00) }} per litre ( an increase from {{ $fmt->format(410.00) }} per litre in less than two weeks )</strong> and is rising rapidly.
</p>

<p>
    In order to improve our service offerings and the quality of service delivered, we will be reviewing our prices to make them commensurate with the current realities being faced in the country. The price changes will take effect on the <strong>24th of March, 2022</strong>.
</p>

<p>
    We continue to appreciate your understanding and we do promise to do our best to continue to improve our services to give you the best possible work experience at our coworking space.
</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
