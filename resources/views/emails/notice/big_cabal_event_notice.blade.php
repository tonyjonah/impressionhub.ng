@component('mail::message')

@component('mail::panel')
<h3>🚨 TechCabal Event Notice</h3>
@endcomponent

<p>
TechCabal have extended an invite to all ImpressionHub Coworkers for their upcoming event tagged <b>The Future of Commerce</b>.
</p>

<p>
Use this <a href="https://techcabal.com/2022/08/15/techcabal-and-moniepoint-partner-to-discuss-how-tech-is-changing-the-informal-economy/">link</a> to register for the event.
</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent