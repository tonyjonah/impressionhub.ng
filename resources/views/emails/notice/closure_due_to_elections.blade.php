@component('mail::message')

@component('mail::panel')
<h3>🚨 Notice of Closure Due to Upcoming Elections</h3>
@endcomponent

<p>Please note that Impression Hub will not be open on <b>Monday 20th</b>, due to the upcoming elections.</p>

Thanks for your understanding,<br>
{{ config('app.name') }}
@endcomponent