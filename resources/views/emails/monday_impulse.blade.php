<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]-->
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Email</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Work+Sans:wght@200;300&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=League+Gothic&family=Sevillana&family=Work+Sans:ital,wght@0,100;0,200;0,300;1,100&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=League+Gothic&family=Roboto:wght@700&family=Sevillana&family=Work+Sans:ital,wght@0,100;0,200;0,300;1,100&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=Fira+Sans:wght@300&family=League+Gothic&family=Roboto:wght@700&family=Sevillana&family=Work+Sans:ital,wght@0,100;0,200;0,300;1,100&display=swap');

        * {
            margin: 0;
            padding: 0;
            border: 0;
        }

        body {

            background-color: black;
            font-size: 19px;
            max-width: 800px;
            margin: 0 auto;
            padding: 3%;
        }

        #logo {
            width: 50%;
            margin: auto;
        }

        img {
            max-width: 100%;
        }

        header {
            width: 98%;
        }

        #wrapper {
            background-color: #f0f6fb;
            border-radius: 5px;

        }

        #social {
            margin: 3% 2% 4% 3%;
            list-style-type: none;
        }

        #social>li {
            display: inline;
        }

        #social>li>a>img {
            max-width: 25px;
        }

        

        .col-one,
        .footies,
        .btn {
            font-family: 'Work Sans', sans-serif;
        }

        .headies {
            font-family: 'Roboto', sans-serif;
            text-align: center;
            padding-top: 2%;
        }
        
        .bicy {
            font-family: 'Work Sans', sans-serif;
            text-align: center;
            
            font-size: large;
        }

        .paragraphs {
            font-family: 'Fira Sans', sans-serif;
            text-align: justify;
            padding: 1% 10% 5% 10%;
        }

        p {
            padding-bottom: 1em;
        }

        h1,
        h2,
        p {
            margin: 3%;
        }

        .sponsored-post {
            color: rgba(0,0,0,0.5);
            text-align: center;
            margin-top: -5%;
        }

        .hacker {
            text-align: center;
            margin-top: -2%;
        }

        .hacker-photo {
            width: 50%;
            margin: auto;
            margin-top: 2%;
        }

        .sponsor-link {
            color: rgb(199, 101, 44);
            text-decoration: none;
        }
        

        .btn-container {
            display: flex;
            justify-content: center;
        }

        .btn {
            margin: 0 2% 4% 0;
            background-color: rgba(33, 70, 199, 0.6);
            text-decoration: none;
            color: whitesmoke;
            font-weight: 800;
            padding: 8px 12px;
            border-radius: 8px;
            letter-spacing: 2px;
        }

        hr {
            height: 1px;
            background-color: black;
            clear: both;
            width: 96%;
            margin: auto;
        }

        #contact {
            text-align: center;
            padding-bottom: 3%;
            line-height: 16px;
            font-size: 12px;
        }
    </style>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="">

</head>

<body>
    <div>
        <header>
            <div id="logo">
                <img src="https://impressionhub.ng/img/logo-email-light.png" alt="logo">
            </div>
        </header>
        <!-- background-color: #d8dada; body color-->
    </div>
    <div id="wrapper">
        <header>
            <h1 class="headies">IMPULSE MONDAY 💪</h1>
        </header>
        <div id="banner">
            <img src=https://impressionhub.ng/img/27.03.230.jpg" alt="">
        </div>
        <div class="col-one">
            <h2 class="bicy">Keep it Moving 🚀</h2>

            <div class="paragraphs">
                <p> 
                    Do you you know that continuous improvement is essential for success in all aspects of life? Whether it's personal growth, career development, or 
                    mastering a new skill, the world rewards excellence. To achieve greatness, we must strive to be better every day and embrace a growth mindset.
                </p>
    
                <p>
                    The world is constantly evolving, and those who can adapt and improve themselves are the ones who succeed. Excellence is recognized and rewarded 
                    in all industries, from sports to business to the arts. Those who stand out from the crowd are those who have put in the time and effort to hone 
                    their skills and knowledge.
                <p>
                    However, the only barrier standing between us and our growth is our attitude towards learning. When we embrace a growth mindset and approach 
                    challenges with a willingness to learn and improve, we open ourselves up to endless possibilities. We become more resilient, adaptable, and 
                    better equipped to handle the challenges that life throws at us.
                </p>
                <p>
                    Moreover, when we keep getting better, we inspire others to do the same. Our excellence can motivate and encourage those around us to strive 
                    for greatness in their own lives. In this way, our personal growth has a ripple effect that can benefit not only ourselves but also those around us.
                </p>
                <p>
                    In conclusion, the world rewards excellence, and to achieve greatness, we must keep getting better. The only barrier standing in our way is our attitude 
                    towards learning. With a growth mindset and a willingness to continuously improve ourselves, we can unlock our full potential and achieve our goals.
                </p>
            </div>
            <div>
                <p class="sponsored-post">Sponsored post</p>
                <h3 class="hacker">Hackaholics 4.0</h3>
                <div class="hacker-photo"> <img src="https://impressionhub.ng/img/wema2.jpeg" alt="Wema bank hackerton image"></div>
                <p class="paragraphs">
                    Hello tech enthusiasts and future tech innovators, Hackaholics 4.0 is here to give you the opportunity to meet mentors and top techpreneurs and 
                    also a chance to win up to $10,000 to help you develop that great tech idea.
                </p>
                <p class="paragraphs">What are you waiting for? Hurry now to register by <span ><a class="sponsor-link" target="#" href="http://bit.ly/Hackaholics4">clicking here</a></span></p>
            </div>

            <div class="btn-container">
                <a href="https://www.impressionhub.ng/" class="btn">Pay us a visit</a>
            </div>

            <hr />

            <footer id="contact">
                <p>
                    Impression Hub <br>
                    3, St Finbarr's College Road<br>
                    Pako Bus Stop, Akoka, Lagos <br>
                    info@impressionhub.ng
                </p>
                <div>
                    <ul id="social">
                        <li>
                            <a href="https://twitter.com/hub_impression" target="_blank">
                                <img src="https://impressionhub.ng/img/tw-color.png" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/d_impressionhub/?hl=en" target="_blank">
                                <img src="https://impressionhub.ng/img/in-color.png" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/theimpressionhub" target="_blank">
                                <img src="https://impressionhub.ng/img/fb-color.png" alt="">
                            </a>
                        </li>
                    </ul>
                </div>
            </footer>

        </div>

    </div>

    <script src="" async defer></script>
</body>

</html>