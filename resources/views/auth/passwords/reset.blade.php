@extends('layouts.main')

@section('title', 'Reset Password')

@section ('content')
    <div class="main-container">
        <section class="cover fullscreen image-bg overlay">
            <div class="background-image-holder">
                <img alt="image" class="background-image" src="img/home12.jpg" />
            </div>
            <div class="container v-align-transform">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">
                        <div class="feature bordered text-center">
                            <h4 class="uppercase">Reset Password</h4>
                            <form class="text-left" method="POST" action="{{ route('password.update') }}">
                                @csrf
                                
                                <input type="hidden" name="token" value="{{ $token }}">

                                <input class="mb0" name="email" type="text" placeholder="Email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <input class="mb0" name="password" type="password" placeholder="Password" required autocomplete="new-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <input class="mb0" name="password_confirmation" type="password" placeholder="Confirm Password" required autocomplete="new-password">
                                <input type="submit" value="Reset Password">
                            </form>
                        </div>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
    </div>
    @section('jsScripts')
        @parent
    @endsection
@endsection        