<div class="ih-modal-confirmation" style="display: none;">
    <h4 class="text-center">Your Subscription's Summary</h4>                                    
    @if ($option->tenure == 1)
    <ul>
        <li class="subscription_start_confirmation"></li>
        <li>Space Option <b class="uppercase">{{$option->type}}</b></li>
        <li style="font-size: 1.1em">Subscription Cost is <span>{{$money->format($option->plan_cost)}}</span></li>
    </ul>
    @else
    <ul>
        @if ($option->id == 2)
        <li>Subscription Duration is <b>{{$option->tenure . " Days, with 3 Days Access Every Week, Monthly" }}</b></li>
        @else
        <li>Subscription Duration is <b>{{$option->tenure . " Days"  }}</b></li>
        @endif
        <li class="subscription_start_confirmation"></li>
        <li class="subscription_end_date"></li>
        <li>Space Option <b class="uppercase">{{$option->type}}</b></li>
        <li style="font-size: 1.1em">Subscription Cost is <b>{{$money->format($option->plan_cost)}}</b></li>
    </ul>
    @endif
    
    <div class="form-group">
        <button class="btn btn-submit ih-modal-btn" type="submit" data-target="subscription">
            <span class="ih-submit-button-text">
                Pay
            </span>
            <span style="display:none;" class="confirm-success glyphicon glyphicon-ok"></span>
            <span style="display:none;" class="confirm-failure glyphicon glyphicon-remove"></span>
        </button>
    </div>
    <div class="ih-modal-upload-notice" style="display:none;">
        <p class="text-center"><small>Upload in progress...</small></p>
    </div>
</div>