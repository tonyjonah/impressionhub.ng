@php 
    $money = new \NumberFormatter('en_NG', \NumberFormatter::CURRENCY); 
@endphp

@extends ('layouts.main')

@section('title', 'Subscribe')

@section ('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <form action="{{ route('spaceSubscription', $option->id) }}" method="post" name="subscription" class="ih-ajax" enctype="multipart/form-data">
                    @csrf
                    <ul id="progressbar">
                        @if (!is_null(auth()->user()))
                        <li class="" id="pay"><h4>Complete Your Subscription</h4><hr></li>
                        @else
                        <li class="active" id="existing_account"><h4>Existing Account</h4><hr></li>
                        <li class="" id="basic_info"><h4>Basic Information</h4><hr></li>
                        <li class="" id="pay"><h4>Complete Your Subscription</h4><hr></li>
                        @endif
                    </ul>

                    @if (!is_null(auth()->user()))
                    <fieldset class="active" data-target="pay">
                        <h5 class="uppercase">Complete Your Subscription's Schedule Information</h5>
                        @if ($errors->any())
                        <span class="help-block alert alert-danger" role="alert">                      
                            Please correct the following to proceed                        
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li> {{ $error }}</li>    
                                @endforeach
                            </ul>
                        </span>
                        @endif
                        
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="feature feature-3">
                                    <div class="text-center">
                                        <i class="icon ti-calendar"></i>
                                        <h4>Your Starting Date</h4>
                                    </div>
                                    <div class="form-group">
                                        <div class="ih-date-container">
                                            <label for="day">Day</label>
                                            <select name="day" id="" class="ih-select ih-date-picker ih-date-day">
                                            </select>
                                        </div>

                                        <div class="ih-date-container">
                                            <label for="month">Month</label>
                                            <select name="month" id="" class="ih-select ih-date-picker ih-date-month">
                                            </select>
                                        </div>
                        
                                        <div class="ih-date-container">
                                            <label for="year">Year</label>
                                            <select name="year" id="" class="ih-select ih-date-picker ih-date-year">
                                            </select>
                                        </div>

                                        <input type="hidden" name="tenure" value="{{$option->tenure}}">
                                    </div>                        
                                </div>
                            </div>
                        
                            <div class="col-xs-12">
                                <div class="feature feature-3">
                                    <div class="text-center">
                                        <i class="icon ti-time"></i>
                                        <h4>Duration</h4>
                                    </div>
                        
                                    <div class="text-center">
                                        <p class="text-center lead ih-subscription-schedule" id="hide_schedule">
                                            Please select a date.
                                        </p>
                        
                                        @switch($option->tenure)
                                            @case(1)
                                                <p class="text-center lead ih-subscription-schedule" id="show_schedule" style="display:none;">
                                                    Your'e Scheduled to use the Hub on <span id="subscription_start"></span>
                                                </p>
                                                <small id="notice" class="text-center" style="display:none;"></small>
                                                @break
                                            @case(5)
                                                <p class="text-center lead ih-subscription-schedule" id="show_schedule" style="display:none;">
                                                    <span id="">Your One Week of use starts from</span> <span id="subscription_start"></span> and ends
                                                    <span id="subscription_end"></span>
                                                </p>
                                                <small id="notice" class="text-center" style="display:none;"></small>
                                                @break
                                            @case(30)
                                                <p class="text-center lead ih-subscription-schedule" id="show_schedule" style="display:none;">
                                                    Your One Month's use starts from <span id="subscription_start"></span> and ends
                                                    <span id="subscription_end"></span>
                                                </p>
                                                <small id="notice" class="text-center" style="display:none;"></small>
                                                @break
                                            @default
                                        @endswitch
                                    </div>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="plan" value="{{$option->id}}">
                        <input type="hidden" name="currency" value="NGN">
                        <input type="hidden" name="reference" value="{{ App\Http\Controllers\PaymentController::genTranxRef() }}">
                        
                        @if ($option->tenure == 1)
                            <input type="hidden" name="schedule_start">
                            <input type="hidden" name="amount" value="{{$option->plan_cost * 100}}">
                        @else
                            <input type="hidden" name="schedule_start">
                            <input type="hidden" name="schedule_end">
                            <input type="hidden" name="amount" value="{{$option->plan_cost * 100}}">
                        @endif

                        <div class="form-group">                        
                            <div class="modal-container">
                                <div class="modal_contents">
                                    <a id="schedule_confirmation" class="btn btn-modal disabled" style="margin-left:auto;margin-right:auto;display:block;" href="#" aria-disabled="true">
                                        <span>Subscribe</span>
                                    </a>
                                </div>

                                <div class="foundry_modal">
                                    <div class="ih-modal-text-content ih-terms">
                                        <div class="ih-terms-content">

                                        </div>
                                        <div class="ih-terms-acceptance" style="display: inline;">
                                            <span><strong>Accept</strong></span>
                                            <input type="checkbox" name="accept" class="accept-terms" value="2022_Jan_01">
                                        </div>
                                    </div>
                                    @include('subscriptions.includes.subscription_confirmation')
                                </div>
                            </div>
                        </div>
                        
                    </fieldset>    
                    @else
                    <fieldset class="active" data-target="existing_account">
                        <h5 class="uppercase">Do You Have An Existing Account?</h5>
            
                        @if ($errors->any())
                        <span class="help-block alert alert-danger" role="alert">
                            Please correct the following to proceed
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li> {{ $error }}</li>    
                                @endforeach
                            </ul>
                        </span>
                        @endif
            
                        <div class="form-group">
                            <div class="radio-option checked">
                                <div class="inner"></div>
                                <input type="radio" name="account_exists" value="no">
                            </div>
                            <span>No</span>
                        
                
                            <div class="radio-option" id="loginConfirm">
                                <div class="inner"></div>
                                <input type="radio" name="account_exists" value="yes">
                            </div>
                            <span>Yes</span>
                        </div>

                        <div class="form-group">
                            <button type="button" class="btn btn-next">
                                <span>Next</span>
                            </button>
                        </div>
                    </fieldset>

                    <fieldset class="ih-confirm" data-target="basic_info">
                        <h5 class="uppercase">Please Provide Us With Some Info About Yourself</h5>

                        <div class="form-group">
                            <div class="ih-avatar-upload">
                                <div class="ih-avatar-edit">
                                    <input type='file' id="imageUpload" name="photo" accept=".png, .jpg, .jpeg" required>
                                    <label for="imageUpload" title="2MB Max Size"></label>
                                </div>
                                <div class="ih-avatar-preview">
                                    <div id="imagePreview" style="background-image: url({{ asset('/img/profile_pic.svg') }});">
                                    </div>
                                </div>
                                <div class="text-center uppercase">
                                    <span style="color:#ccc; font-size:smaller">Upload Image : 2MB Max Size</span>
                                  </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="text" name="fname" placeholder="First Name">
                            <input type="text" name="lname" placeholder="Last Name">
                            <input type="text" name="email" placeholder="Email">
                            <input type="password" name="password" placeholder="Password">
                            <input type="password" name="password_confirmation" placeholder="Confirm Password">
                            <input type="text" name="phone" placeholder="Phone">
                            <select name="gender">
                                <option>Indicate Your Gender</option>
                                <option value="0">Female</option>
                                <option value="1">Male</option>
                            </select>
                        </div>

                        <div class="form-group">
                            {{-- <button type="button" class="btn btn-prev">
                                <span>Previous</span>
                            </button> --}}
                            <button type="button" class="btn btn-next fields-check">
                                <span>Next</span>
                            </button>
                            <small class="ih-field-check-notify" style="display:block;"></small>
                        </div>

                    </fieldset>

                    <fieldset class="" data-target="pay">
                        <h5 class="uppercase">Complete Your Subscription's Schedule Information</h5>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="feature feature-3">
                                    <div class="text-center">
                                        <i class="icon ti-calendar"></i>
                                        <h4>Your Starting Date</h4>
                                    </div>
                                    <div class="form-group">
                                        <div class="ih-date-container">
                                            <label for="day">Day</label>
                                            <select name="day" id="" class="ih-select ih-date-picker ih-date-day">
                                            </select>
                                        </div>
                                        <div class="ih-date-container">
                                            <label for="month">Month</label>
                                            <select name="month" id="" class="ih-select ih-date-picker ih-date-month">
                                            </select>
                                        </div>
                                        <div class="ih-date-container">
                                            <label for="year">Year</label>
                                            <select name="year" id="" class="ih-select ih-date-picker ih-date-year">
                                            </select>
                                        </div>
                                        <input type="hidden" name="tenure" value="{{$option->tenure}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="feature feature-3">
                                    <div class="text-center">
                                        <i class="icon ti-time"></i>
                                        <h4>Duration</h4>
                                    </div>
                                    <div class="text-center">
                                        <p class="text-center lead ih-subscription-schedule" id="hide_schedule">
                                            Please select a date.
                                        </p>
                                        @switch($option->tenure)
                                            @case(1)
                                                <p class="text-center lead ih-subscription-schedule" id="show_schedule" style="display:none;">
                                                    Your'e Scheduled to use the Hub on <span id="subscription_start"></span>
                                                </p>
                                                <small id="notice" class="text-center" style="display:none;"></small>
                                                @break
                                            @case(5)
                                                <p class="text-center lead ih-subscription-schedule" id="show_schedule" style="display:none;">
                                                    <span id="">Your One Week of use starts from</span> <span id="subscription_start"></span> and ends
                                                    <span id="subscription_end"></span>
                                                </p>
                                                <small id="notice" class="text-center" style="display:none;"></small>
                                                @break
                                            @case(30)
                                                <p class="text-center lead ih-subscription-schedule" id="show_schedule" style="display:none;">
                                                    Your One Month's use starts from <span id="subscription_start"></span> and ends
                                                    <span id="subscription_end"></span>
                                                </p>
                                                <small id="notice" class="text-center" style="display:none;"></small>
                                                @break
                                            @default
                                                
                                        @endswitch
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="plan" value="{{$option->id}}">
                        <input type="hidden" name="currency" value="NGN">
                        <input type="hidden" name="reference" value="{{ App\Http\Controllers\PaymentController::genTranxRef() }}">

                        @if ($option->tenure == 1)
                            <input type="hidden" name="schedule_start">
                            <input type="hidden" name="amount" value="{{$option->plan_cost * 100}}">
                        @else
                            <input type="hidden" name="schedule_start">
                            <input type="hidden" name="schedule_end">
                            <input type="hidden" name="amount" value="{{$option->plan_cost * 100}}">
                        @endif
                        
                        <div class="form-group">
                            <div class="modal-container">
                                <div class="modal_contents">
                                    <a id="schedule_confirmation" class="btn btn-modal disabled" style="margin-left:auto;margin-right:auto;display:block;" href="#" aria-disabled="true">
                                        <span>Subscribe</span>
                                    </a>
                                </div>
                                <div class="foundry_modal">
                                    <div class="ih-modal-text-content ih-terms">
                                        <div class="ih-terms-content">

                                        </div>
                                        <div class="ih-terms-acceptance" style="display: inline;">
                                            <span><strong>Accept</strong></span>
                                            <input type="checkbox" name="accept" class="accept-terms" value="2022_Jan_01">
                                        </div>
                                    </div>
                                    @include('subscriptions.includes.subscription_confirmation')
                                </div>
                            </div>
                        </div>
                    </fieldset>  
                    @endif       
                </form>
            </div>
        </div>
    </div>

</section>
@endsection

@section('jsScripts')
    {{-- Login Modal --}}
    @if(!auth()->user())
        <div class="modal-container">
            <a class="btn btn-modal" id="loginModalBtn" href="#" style="display:none;">Show Modal</a>
            <div class="foundry_modal">
                <div class="feature bordered text-center">
                    <h4 class="uppercase">Login Here</h4>
                    <form class="text-left" action="/subscription/auth/login" method="POST">
                        @csrf
                        <input class="mb0" name="email" type="text" placeholder="Email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <input class="mb0" name="password" type="password" placeholder="Password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <input type="submit" value="Login" />
                        <span class="mb0">Remember Me</span>
                        <div class="checkbox-option pull-right">
                            <div class="inner"></div>
                            <input type="checkbox" name="remember" value="yes">
                        </div>
                    </form>
                    <p class="mb0">Forgot your password?
                        <a href="{{ route('password.request') }}">Click Here To Reset</a>
                    </p>
                </div>
            </div>
        </div>
    @endif
    {{-- End of Login Modal --}}
    @parent
@endsection