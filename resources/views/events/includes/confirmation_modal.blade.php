<div class="ih-modal-confirmation" style="display: none;">
    <h4 class="text-center">Your Event's Schedule Summary</h4>                                    
    @if (str_ends_with($option->room_option_name, 'daily'))
    <ul>
        @if (auth()->user())
        <li>Meeting Booked by {{auth()->user()->fname . " " . auth()->user()->lname}}</li>
        @else
        <li>Meeting Booked by <b><span class="meeting_organizer"></span></b></li>    
        @endif

        <li>Room Option <b>{{ucwords(str_replace('_', ' ', $option->room_option_name)) }}</b></li>
        <li style="font-size: 1.1em">Cost is <span>{{$money->format($option->room_option_cost)}}</span></li>
    </ul>
    @else
    <ul>
        @if (auth()->user())
        <li>Meeting Booked by <b><span class="meeting_organizer">{{auth()->user()->fname . " " . auth()->user()->lname}}</span></b></li>
        @else
        <li>Meeting Booked by <b><span class="meeting_organizer"></span></b></li>    
        @endif
        
        <li>Starts at <b><span class="meeting_start_time"></span></b></li>
        <li>Ends at <b><span class="meeting_end_time"></span></b></li>
        <li>Room Option <b>{{ucwords(str_replace('_', ' ', $option->room_option_name))}}</b></li>
        <li>Duration is <b><span class="meeting_duration"></span> Hour(s)</b></li>
        <li>Hourly use rate is <span>{{$money->format($option->room_option_cost)}}</span></li>
        <li style="font-size: 1.1em">Cost is <span class="meeting_cost"></span></li>
    </ul>
    @endif
    
    <div class="form-group">
        {{-- data-target should be the same as the name of the form --}}
        <button class="btn btn-submit ih-modal-btn" type="submit" data-target="schedule">
            <span class="ih-submit-button-text">
                Pay
            </span>
            <span style="display:none;" class="confirm-success glyphicon glyphicon-ok"></span>
            <span style="display:none;" class="confirm-failure glyphicon glyphicon-remove"></span>
        </button>
    </div>
    <div class="ih-modal-upload-notice" style="display:none;">
        <p class="text-center"><small>Upload in progress...</small></p>
    </div>
</div>