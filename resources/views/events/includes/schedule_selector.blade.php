<div class="form-group" id="ih-subscription-schedule-time" style="display: none;">
    <ul class="schedule_picker">
        <li data-time-begin-marker="09:00:00" data-time-end-marker="09:30:00"> </li>
        <li data-time-begin-marker="09:30:00" data-time-end-marker="10:00:00"> </li>
        <li data-time-begin-marker="10:00:00" data-time-end-marker="10:30:00"> </li>
        <li data-time-begin-marker="10:30:00" data-time-end-marker="11:00:00"> </li>
        <li data-time-begin-marker="11:00:00" data-time-end-marker="11:30:00"> </li>
        <li data-time-begin-marker="11:30:00" data-time-end-marker="12:00:00"> </li>
        <li data-time-begin-marker="12:00:00" data-time-end-marker="12:30:00"> </li>
        <li data-time-begin-marker="12:30:00" data-time-end-marker="13:00:00"> </li>
        <li data-time-begin-marker="13:00:00" data-time-end-marker="13:30:00"> </li>
        <li data-time-begin-marker="13:30:00" data-time-end-marker="14:00:00"> </li>
        <li data-time-begin-marker="14:00:00" data-time-end-marker="14:30:00"> </li>
        <li data-time-begin-marker="14:30:00" data-time-end-marker="15:00:00"> </li>
        <li data-time-begin-marker="15:00:00" data-time-end-marker="15:30:00"> </li>
        <li data-time-begin-marker="15:30:00" data-time-end-marker="16:00:00"> </li>
        <li data-time-begin-marker="16:00:00" data-time-end-marker="16:30:00"> </li>
        <li data-time-begin-marker="16:30:00" data-time-end-marker="17:00:00"> </li>
        <li data-time-begin-marker="17:00:00" data-time-end-marker="17:30:00"> </li>
        <li data-time-begin-marker="17:30:00" data-time-end-marker="18:00:00"> </li>
        <li data-time-begin-marker="18:00:00" data-time-end-marker="18:30:00"> </li>
        <li data-time-begin-marker="18:30:00" data-time-end-marker="19:00:00"> </li>
        <li data-time-begin-marker="19:00:00" data-time-end-marker="19:30:00"> </li>
        <li data-time-begin-marker="19:30:00" data-time-end-marker="20:00:00"> </li>
        <li data-time-begin-marker="20:00:00" data-time-end-marker="20:30:00"> </li>
        <li data-time-begin-marker="20:30:00" data-time-end-marker="21:00:00"> </li>
    </ul>
    @if (! str_ends_with($option->room_option_name,'daily'))
    <label for="scheduleStart">Scheduled Start Time</label>
    <select name="scheduleStart" id="scheduleStart">
        <option value="09:00:00">9:00:00</option>
        <option value="09:30:00">9:30:00</option>
        <option value="10:00:00">10:00:00</option>
        <option value="10:30:00">10:30:00</option>
        <option value="11:00:00">11:00:00</option>
        <option value="11:30:00">11:30:00</option>
        <option value="12:00:00">12:00:00</option>
        <option value="12:30:00">12:30:00</option>
        <option value="13:00:00">13:00:00</option>
        <option value="13:30:00">13:30:00</option>
        <option value="14:00:00">14:00:00</option>
        <option value="14:30:00">14:30:00</option>
        <option value="15:00:00">15:00:00</option>
        <option value="15:30:00">15:30:00</option>
        <option value="16:00:00">16:00:00</option>
        <option value="16:30:00">16:30:00</option>
        <option value="17:00:00">17:00:00</option>
        <option value="17:30:00">17:30:00</option>
        <option value="18:00:00">18:00:00</option>
        <option value="18:30:00">18:30:00</option>
        <option value="19:00:00">19:00:00</option>
        <option value="19:30:00">19:30:00</option>
        <option value="20:00:00">20:00:00</option>
        <option value="20:30:00">20:30:00</option>
    </select>
    <label for="scheduleEnd">Scheduled End Time</label>
    <select name="scheduleEnd" id="scheduleEnd">
        <option value="09:30:00">9:30:00</option>
        <option value="10:00:00">10:00:00</option>
        <option value="10:30:00">10:30:00</option>
        <option value="11:00:00">11:00:00</option>
        <option value="11:30:00">11:30:00</option>
        <option value="12:00:00">12:00:00</option>
        <option value="12:30:00">12:30:00</option>
        <option value="13:00:00">13:00:00</option>
        <option value="13:30:00">13:30:00</option>
        <option value="14:00:00">14:00:00</option>
        <option value="14:30:00">14:30:00</option>
        <option value="15:00:00">15:00:00</option>
        <option value="15:30:00">15:30:00</option>
        <option value="16:00:00">16:00:00</option>
        <option value="16:30:00">16:30:00</option>
        <option value="17:00:00">17:00:00</option>
        <option value="17:30:00">17:30:00</option>
        <option value="18:00:00">18:00:00</option>
        <option value="18:30:00">18:30:00</option>
        <option value="19:00:00">19:00:00</option>
        <option value="19:30:00">19:30:00</option>
        <option value="20:00:00">20:00:00</option>
        <option value="20:30:00">20:30:00</option>
        <option value="21:00:00">21:00:00</option>
    </select>
    @else
    <input type="hidden" name="scheduleStart" value="09:00:00">
    <input type="hidden" name="scheduleEnd" value="21:00:00">
    @endif
</div>