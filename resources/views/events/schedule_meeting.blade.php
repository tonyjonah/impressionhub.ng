@php 
    $money = new \NumberFormatter('en_NG', \NumberFormatter::CURRENCY);
@endphp

@extends ('layouts.main')

@section('title', 'Schedule Event')

@section ('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <form action="{{ route('storeBooking', $option->id) }}" method="post" name="schedule" class="ih-ajax" enctype="multipart/form-data">
                    @csrf
                    <ul id="progressbar">
                        @if(auth()->user())
                        <li class="active" id="pay"><h4>Schedule Details</h4><hr></li>
                        @else
                        <li class="active" id="existing_account"><h4>Existing Account</h4><hr></li>
                        <li class="" id="basic_info"><h4>Basic Information</h4><hr></li>
                        <li class="" id="pay"><h4>Schedule Details</h4><hr></li>
                        @endif
                    </ul>

                    @if (auth()->user())
                    <fieldset class="active" data-target="pay">
                        <h5 class="uppercase">Enter Your Event Schedule Details</h5>

                        @if ($errors->any())
                        <span class="help-block alert alert-danger" role="alert">
                            Please correct the following to proceed
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li> {{ $error }}</li>    
                                @endforeach
                            </ul>
                        </span>
                        @endif

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="feature feature-3">
                                    <div class="text-center">
                                        <i class="icon ti-calendar"></i>
                                        <h4>Your Chosen Date</h4>
                                    </div>
                                    <div class="form-group">
                                        <div class="ih-date-container">
                                            <label for="intended_day">Day</label>
                                            <select name="day" id="intended_day" class="ih-select ih-date-picker ih-date-day">
                                            </select>
                                        </div>
                                        <div class="ih-date-container">
                                            <label for="intended_month">Month</label>
                                            <select name="month" id="intended_month" class="ih-select ih-date-picker ih-date-month">
                                            </select>
                                        </div>
                                        <div class="ih-date-container">
                                            <label for="intended_year">Year</label>
                                            <select name="year" id="intended_year" class="ih-select ih-date-picker ih-date-year">
                                            </select>
                                        </div>
                                        <input type="hidden" name="date" value="{{old('date')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="feature feature-3">
                                    <div class="text-center">
                                        <i class="icon ti-time"></i>
                                        <h4>Timing</h4>
                                    </div>
                                    @if (! str_ends_with($option->room_option_name, 'daily'))
                                    <div class="text-center">
                                        <p class="text-center lead" id="hide_schedule">
                                            Select The Start & End Time.
                                        </p>
                                    </div>
                                    @endif
                                    @include('events.includes.schedule_selector')
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="fname" value="{{ auth()->user()->fname }}">
                        <input type="hidden" name="lname" value="{{ auth()->user()->lname}}">                        
                        <input type="hidden" name="room_option" value="{{ old('room_option') ?? $option->id}}">
                        <input type="hidden" name="unitCost" value="{{ old('unitCost') ?? $option->room_option_cost}}">
                        <input type="hidden" name="currency" value="NGN">
                        <input type="hidden" name="reference" value="{{ App\Http\Controllers\PaymentController::genTranxRef() }}">

                        @if (str_ends_with($option->room_option_name, 'daily'))
                            <input type="hidden" name="scheduleType" value="daily">
                            <input type="hidden" name="amount" value="{{ old('amount') ?? $option->room_option_cost * 100}}">
                        @else
                            <input type="hidden" name="scheduleType" value="hourly">
                            <input type="hidden" name="amount" value="">
                        @endif

                        <div class="form-group">
                            <div class="modal-container">
                                <div class="modal_contents">
                                    <a id="meeting_schedule_confirmation" class="btn btn-modal disabled" style="margin-left:auto;margin-right:auto;display:block;" href="#" aria-disabled="true">
                                        <span>Schedule Event</span>
                                    </a>
                                </div>
                                <div class="foundry_modal">
                                    <div class="ih-modal-text-content ih-terms">
                                        <div class="ih-terms-content">

                                        </div>
                                        <div class="ih-terms-acceptance" style="display: inline;">
                                            <span><strong>Accept</strong></span>
                                            <input type="checkbox" name="accept" class="accept-terms" value="2022_Jan_01">
                                        </div>
                                    </div>
                                    @include('events.includes.confirmation_modal')
                                </div>
                            </div>
                        </div>
                    </fieldset> 

                    @else

                    <fieldset class="active" data-target="existing_account">
                        <h5 class="uppercase">Do You Have An Existing Account?</h5>
            
                        @if ($errors->any())
                        <span class="help-block alert alert-danger" role="alert">
                            Please correct the following to proceed
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li> {{ $error }}</li>    
                                @endforeach
                            </ul>
                        </span>
                        @endif
            
                        <div class="form-group">
                            <div class="radio-option checked">
                                <div class="inner"></div>
                                <input type="radio" name="account_exists" value="no">
                            </div>
                            <span>No</span>
                        
                
                            <div class="radio-option" id="loginConfirm">
                                <div class="inner"></div>
                                <input type="radio" name="account_exists" value="yes">
                            </div>
                            <span>Yes</span>
                        </div>

                        <div class="form-group">
                            <button type="button" class="btn btn-next">
                                <span>Next</span>
                            </button>
                        </div>
                    </fieldset>

                    <fieldset class="ih-confirm" data-target="basic_info">
                        <h5 class="uppercase">Please Provide Us With Some Info About Yourself</h5>

                        <div class="form-group">
                            <div class="ih-avatar-upload">
                                <div class="ih-avatar-edit">
                                    <input type='file' id="imageUpload" name="photo" accept=".png, .jpg, .jpeg" required>
                                    <label for="imageUpload" title="2MB Max Size"></label>
                                </div>
                                <div class="ih-avatar-preview">
                                    <div id="imagePreview" style="background-image: url({{ asset('/img/profile_pic.svg') }});">
                                    </div>
                                </div>
                                <div class="text-center uppercase">
                                    <span style="color:#ccc; font-size:smaller">Upload Image : 2MB Max Size</span>
                                  </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="text" name="fname" placeholder="First Name" value="{{ old('fname')}}">
                            <input type="text" name="lname" placeholder="Last Name" value="{{ old('lname')}}">
                            <input type="text" name="email" placeholder="Email" value="{{ old('email')}}">
                            <input type="password" name="password" placeholder="Password">
                            <input type="password" name="password_confirmation" placeholder="Confirm Password">
                            <input type="text" name="phone" placeholder="Phone" value="{{ old('phone')}}">
                            <select name="gender">
                                <option>Indicate Your Gender</option>
                                <option value="0">Female</option>
                                <option value="1">Male</option>
                            </select>
                        </div>

                        <div class="form-group">
                            {{-- <button type="button" class="btn btn-prev">
                                <span>Previous</span>
                            </button> --}}
                            <button type="button" class="btn btn-next fields-check">
                                <span>Next</span>
                            </button>
                            <small class="ih-field-check-notify" style="display:block;"></small>
                        </div>

                    </fieldset>

                    <fieldset class="" data-target="pay">
                        <h5 class="uppercase">Enter Your Event Schedule Details</h5>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="feature feature-3">
                                    <div class="text-center">
                                        <i class="icon ti-calendar"></i>
                                        <h4>Your Chosen Date</h4>
                                    </div>
                                    <div class="form-group">
                                        <div class="ih-date-container">
                                            <label for="intended_day">Day</label>
                                            <select name="day" id="intended_day" class="ih-select ih-date-picker ih-date-day">
                                            </select>
                                        </div>
                                        <div class="ih-date-container">
                                            <label for="intended_month">Month</label>
                                            <select name="month" id="intended_month" class="ih-select ih-date-picker ih-date-month">
                                            </select>
                                        </div>
                                        <div class="ih-date-container">
                                            <label for="intended_year">Year</label>
                                            <select name="year" id="intended_year" class="ih-select ih-date-picker ih-date-year">
                                            </select>
                                        </div>
                                        <input type="hidden" name="date" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="feature feature-3">
                                    <div class="text-center">
                                        <i class="icon ti-time"></i>
                                        <h4>Timing</h4>
                                    </div>
                                    @if (! str_ends_with($option->room_option_name, 'daily'))
                                    <div class="text-center">
                                        <p class="text-center lead" id="hide_schedule">
                                            Select The Start & End Time.
                                        </p>
                                    </div>
                                    @endif
                                    @include('events.includes.schedule_selector')
                                </div>
                            </div>
                        </div>                        
                        <input type="hidden" name="tou" value="2022_Jan_01">
                        <input type="hidden" name="room_option" value="{{$option->id}}">
                        <input type="hidden" name="unitCost" value="{{$option->room_option_cost}}">
                        <input type="hidden" name="currency" value="NGN">
                        <input type="hidden" name="reference" value="{{ App\Http\Controllers\PaymentController::genTranxRef() }}">

                        @if (str_ends_with($option->room_option_name, 'daily'))
                            <input type="hidden" name="scheduleType" value="daily">
                            <input type="hidden" name="amount" value="{{$option->room_option_cost * 100}}">
                        @else
                            <input type="hidden" name="scheduleType" value="hourly">
                            <input type="hidden" name="amount" value="">
                        @endif

                        <div class="form-group">
                            <div class="modal-container">
                                <div class="modal_contents">
                                    <a id="meeting_schedule_confirmation" class="btn btn-modal disabled" style="margin-left:auto;margin-right:auto;display:block;" href="#" aria-disabled="true">
                                        <span>Schedule Event</span>
                                    </a>
                                </div>
                                <div class="foundry_modal">
                                    <div class="ih-modal-text-content ih-terms">
                                        <div class="ih-terms-content">

                                        </div>
                                        <div class="ih-terms-acceptance" style="display: inline;">
                                            <span><strong>Accept</strong></span>
                                            <input type="checkbox" name="accept" class="accept-terms" value="2022_Jan_01">
                                        </div>
                                    </div>
                                    @include('events.includes.confirmation_modal')
                                </div>
                            </div>
                        </div>
                    </fieldset> 
                        
                    @endif     
                </form>
            </div>
        </div>
    </div>

</section>
@endsection

@section('jsScripts')
    {{-- Login Modal --}}
    @if(!auth()->user())
        <div class="modal-container">
            <a class="btn btn-modal" id="loginModalBtn" href="#" style="display:none;">Show Modal</a>
            <div class="foundry_modal">
                <div class="feature bordered text-center">
                    <h4 class="uppercase">Login Here</h4>
                    <form class="text-left" action="/room/auth/login" method="POST">
                        @csrf
                        <input class="mb0" name="email" type="text" placeholder="Email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <input class="mb0" name="password" type="password" placeholder="Password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <input type="submit" value="Login" />
                        <span class="mb0">Remember Me</span>
                        <div class="checkbox-option pull-right">
                            <div class="inner"></div>
                            <input type="checkbox" name="remember" value="yes">
                        </div>
                    </form>
                    <p class="mb0">Forgot your password?
                        <a href="{{ route('password.request') }}">Click Here To Reset</a>
                    </p>
                </div>
            </div>
        </div>
    @endif
    {{-- End of Login Modal --}}
    @parent
@endsection