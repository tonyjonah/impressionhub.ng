<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>@yield('title', 'Home Page') | Impression Hub</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        @section('headerStyles')
            <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
            <link href="{{ asset('css/themify-icons.css') }}" rel="stylesheet" type="text/css" media="all" />
            <link href="{{ asset('css/flexslider.css') }}" rel="stylesheet" type="text/css" media="all" />
            <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" media="all" />
            <link href="{{ asset('css/lightbox.min.css') }}" rel="stylesheet" type="text/css" media="all" />
            <link href="{{ asset('css/ytplayer.css') }}" rel="stylesheet" type="text/css" media="all" />
            <link href="{{ asset('css/theme-purple.css') }}" rel="stylesheet" type="text/css" media="all" />
            <script src="https://kit.fontawesome.com/f3d6ca34be.js" crossorigin="anonymous"></script>
        @show
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" media="all" />
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400%7CRaleway:100,400,300,500,600,700%7COpen+Sans:400,500,600' rel='stylesheet' type='text/css'>
    </head>
    <body class="scroll-assist btn-rounded">
        <div class="nav-container">
            <a id="top"></a>
            <nav class="{{ Request::path() === '/' ? 'absolute transparent' : ''  }}">
                <div class="nav-bar">
                    <div class="module left">
                        <a href="/">
                            <img class="logo logo-light" alt="ImpressionHub Logo" src="{{ asset('img/logo-light.png') }}" />
                            <img class="logo logo-dark" alt="ImpressionHub Logo" src="{{ asset('img/logo-dark.png') }}" />
                        </a>
                    </div>
                    <div class="module widget-handle mobile-toggle right visible-sm visible-xs">
                        <i class="ti-menu"></i>
                    </div>
                    <div class="module-group right">
                        <div class="module left">
                            @include('partials.sections.main_menu')
                        </div>
                    </div>
                    <!--end of module group-->
                </div>
            </nav>
        </div>
        <div class="main-container">
            @yield('content')
            <footer class="footer-1 bg-dark">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <img alt="Logo" class="logo" src="{{asset('img/logo-light.png') }}" />
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="widget">
                                <h6 class="title">Recent Posts</h6>
                                <hr>
                                <ul class="link-list recent-posts">
                                </ul>
                            </div>
                            <!--end of widget-->
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="widget">
                                <h6 class="title">Latest Updates</h6>
                                <hr>
                                <div class="twitter-feed">
                                    <div class="tweets-feed" data-feed-name="hub_impression">
                                    </div>
                                </div>
                            </div>
                            <!--end of widget-->
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="widget">
                                <h6 class="title">Instagram</h6>
                                <hr>
                                <div class="instafeed" data-user-name="d_impressionhub">
                                    <ul></ul>
                                </div>
                            </div>
                            <!--end of widget-->
                        </div>
                    </div>
                    <!--end of row-->
                    <div class="row">
                        <div class="col-sm-6">
                            <span class="sub">&copy; Copyright 2017 - Impression Hub</span>
                        </div>
                        <div class="col-sm-6 text-right">
                            <ul class="list-inline social-list">
                                <li>
                                    <a href="https://twitter.com/hub_impression">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/theimpressionhub">
                                        <i class="ti-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/d_impressionhub/?hl=en">
                                        <i class="ti-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--end of container-->
                <a class="btn btn-sm fade-half back-to-top inner-link" href="#top">Top</a>
            </footer>
        </div>

        {{-- JavaScript sources & libraries --}}
        @section('jsScripts')
            {{-- JavaScript Notification Modal --}}
            <div class="modal-container">
                <a href="#" style="display: none;" class="btn btn-modal" id="notificationModalBtn">Show Notification</a>
                <div class="foundry_modal">
                    <h3>Something's Wrong</h3>
                    <p class="notificationMessage"></p>
                </div>
            </div>
            {{-- End of JavaScript Notification modal --}}
    
            {{-- Section for notifications in app --}}
            @if (session('notice'))
                <div class="foundry_modal text-center" data-time-delay="0">
                    <h3>There's An Issue</h3>
                        {!! session('notice') !!}
                </div>
            @endif
            {{-- End of App notifications section --}}

            {{-- Dynamic Modals --}}
            <div class="modal-container">
                <a href="#" style="display: none" class="btn btn-modal" id="dynamicModalBtn">#</a>
                <div class="foundry_modal">
                    <div class="modal-content load-modal"></div>
                </div>
            </div>
            {{-- End of Dynamic Modals --}}

            {{-- Announcement Modals --}}
            {{-- @php
                $nameOfNotice = 'price_change_alert';   
            @endphp
            <div class="foundry_modal text-justify" data-cookie="dismissed_{{ $nameOfNotice }}" data-time-delay="4000">
                @include('partials.notifications.' . $nameOfNotice)
            </div> --}}
            {{-- End of Announcement Modals --}}

        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/flickr.js') }}"></script>
        <script src="{{ asset('js/flexslider.min.js') }}"></script>
        <script src="{{ asset('js/lightbox.min.js') }}"></script>
        <script src="{{ asset('js/masonry.min.js') }}"></script>
        <script src="{{ asset('js/twitterfetcher.min.js') }}"></script>
        <script src="{{ asset('js/spectragram.min.js') }}"></script>
        <script src="{{ asset('js/ytplayer.min.js') }}"></script>
        <script src="{{ asset('js/countdown.min.js') }}"></script>
        <script src="{{ asset('js/smooth-scroll.min.js') }}"></script>
        <script src="{{ asset('js/parallax.js') }}"></script>
        <script src="{{ asset('js/scripts.js') }}"></script>
        @show
    </body>
</html>