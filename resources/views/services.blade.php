@php 
    $money = new \NumberFormatter('en_NG', \NumberFormatter::CURRENCY); 
@endphp

@extends ('layouts.main')

@section('title', 'Services')

@section ('content')
<section class="page-title page-title-4 image-bg overlay parallax">
    <div class="background-image-holder bg-image-adjust-top">
        <img alt="Background Image" class="background-image" src="{{ asset('img/open_plan.jpg') }}" />
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="uppercase mb0">Services</h3>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="feature feature-1 boxed">
                    <div class="text-center">
                        <i class="fas fa-users fa-5x"></i>
                        <h4 class="uppercase">Community Based</h4>
                    </div>
                    <p>
                        The actual value of using a Coworking space, asides from the cost savings, is that sense of being part of a friendly, growth oriented community. We foster this by encouraging social interaction, organizing social activities and free training sessions for our Coworkers.
                    </p>
                </div>
                <!--end of feature-->
            </div>
            <div class="col-sm-6">
                <div class="feature feature-1 boxed">
                    <div class="text-center">
                        <i class="fas fa-laptop-code fa-5x"></i>
                        <h4 class="uppercase">Productivity Focused</h4>
                    </div>
                    <p>
                        From the onset our intent was to provide an environment where focused work is possible. This is something we take seriously as we know every productive minute counts. Our community of Coworkers appreciate this and everyone plays their part in ensuring the space is a distraction free zone.
                    </p>
                </div>
                <!--end of feature-->
            </div>
            <div class="col-sm-6">
                <div class="feature feature-1 boxed">
                    <div class="text-center">
                        <i class="fas fa-coffee fa-5x"></i>
                        <h4 class="uppercase">Free Beverages</h4>
                    </div>
                    <p>
                        Being productivity focused ourselves, we believe in leaving no brain behind. This statement encouraged us to ensure that we always have caffenated beverages available for the productivity heavy weights and goal getters seeking to stay sharp through the day.
                    </p>
                </div>
                <!--end of feature-->
            </div>
            <div class="col-sm-6">
                <div class="feature feature-1 boxed">
                    <div class="text-center">
                        <i class="fas fa-money-bill-wave fa-5x"></i>
                        <h4 class="uppercase">Flexible Pricing & Schedules</h4>
                    </div>
                    <p>
                        We understand that while having a fixed schedule might be a great help to many, alot of people require flexibility due to their none-routine lifestyle. With that in mind, we have created a number of scheduling options for our space options to enable our customers opt in for what suits them.
                    </p>
                </div>
                <!--end of feature-->
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <h3 class="uppercase text-center">Space Options</h3>
            </div>
        </div>
    </div>
</section>

<section class="image-square left" id="standard_spaces_info">
    <div class="col-md-6 image">
        <div class="background-image-holder fadeIn" style="background:url('/img/standard2.jpg')");>
            <img src="{{asset('img/standard2.jpg') }}" alt="Two men using our standard spaces">
        </div>
    </div>
    <div class="col-md-6 content">
        <h4 class="uppercase">Standard Space Option</h4>
        <p class="mb0">
            The Standard Space is a cost effective, shared space option ideal for teams and for those who need to get down to business quickly with no fluff. The Standard Spaces come with comfortable seating, two (2) hours access to our conference room, free coffee / tea, fast internet access and access to community events and resources.
        </p>
    </div>
</section>

<section class="image-square right" id="premium_spaces_info">
    <div class="col-md-6 image">
        <div class="background-image-holder fadeIn" style="background:url('/img/premium.jpg');">
            <img src="{{ asset('img/premium.jpg') }}" alt="Individual using the premium space">
        </div>
    </div>
    <div class="col-md-6 content">
        <h4 class="uppercase">Premium Space Option</h4>
        <p class="mb0">
            Our Premium spaces were made for those who work independently and require relative privacy. It's ideal for Architects, Senior Developers, Devops Engineers, Writers and other professionals who work alone. The Premium Spaces come with comfortable seating, three (3) hours access to our conference room, free coffee / tea, free breakfast once a week, fast internet access, access to community events and resources.
        </p>
    </div>
</section>

<section id="daily_use_costs">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <h3 class="uppercase text-center">Daily Cost Options For Coworking</h3>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="pricing-table pt-2 emphasis text-center mb8">
                    <h5 class="uppercase">{{$plans['standard_daily']->type}}</h5>
                    <span class="price">{{$money->format($plans['standard_daily']->plan_cost)}}</span>
                    @isset($plans['standard_daily']->starts_at)
                    <div class="uppercase text-center mb8" style="display:block;font-size:0.9em;">Weekdays</div>
                    <div class="uppercase text-center" style="display:block;font-size:0.9em;">{{ date('h:i:s A', strtotime($plans['standard_daily']->starts_at)) . ' - ' . date('h:i:s A', strtotime($plans['standard_daily']->ends_at)) }}</div>
                    @endisset
                    @isset($plans['standard_daily']->weekend_starts_at)
                    <div class="uppercase text-center mb8" style="display:block;font-size:0.9em;">Saturdays</div>
                    <div class="uppercase text-center" style="display:block;font-size:0.9em;">{{ date('h:i:s A', strtotime($plans['standard_daily']->weekend_starts_at)) . ' - ' . date('h:i:s A', strtotime($plans['standard_daily']->weekend_ends_at)) }}</div>
                    @endisset
                    <a class="btn btn-white btn-lg" href="{{ route('subscribe', $plans['standard_daily']) }}">Get Started</a>
                    <ul>
                        <li>
                            <strong>Unlimited</strong> Internet Access
                        </li>
                        <li>
                            <strong>FREE</strong> Beverages
                        </li>
                    </ul>
                </div>
                <!--end of pricing table-->
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="pricing-table pt-2 boxed text-center mb8">
                    <h5 class="uppercase">{{$plans['premium_daily']->type}}</h5>
                    <span class="price">{{$money->format($plans['premium_daily']->plan_cost)}}</span>
                    @isset($plans['premium_daily']->starts_at)
                    <div class="uppercase text-center mb8" style="display:block;font-size:0.9em;">Weekdays</div>
                    <div class="uppercase text-center" style="display:block;font-size:0.9em;">{{ date('h:i:s A', strtotime($plans['premium_daily']->starts_at)) . ' - ' . date('h:i:s A', strtotime($plans['premium_daily']->ends_at)) }}</div>
                    @endisset
                    @isset($plans['premium_daily']->weekend_starts_at)
                    <div class="uppercase text-center mb8" style="display:block;font-size:0.9em;">Saturdays</div>
                    <div class="uppercase text-center" style="display:block;font-size:0.9em;">{{ date('h:i:s A', strtotime($plans['premium_daily']->weekend_starts_at)) . ' - ' . date('h:i:s A', strtotime($plans['premium_daily']->weekend_ends_at)) }}</div>
                    @endisset
                    <a class="btn btn-filled btn-lg" href="{{ route('subscribe', $plans['premium_daily']) }}">Get Started</a>
                    <ul>
                        <li>
                            <strong>Unlimited</strong> Internet Access
                        </li>
                        <li>
                            <strong>FREE</strong> Beverages
                        </li>
                    </ul>
                </div>
                <!--end of pricing table-->
            </div>
        </div>
    </div>
</section>

<section id="weekly_use_costs">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <h3 class="uppercase text-center">Weekly Cost Options For Coworking</h3>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="pricing-table pt-2 emphasis text-center mb8">
                    <h5 class="uppercase">{{$plans['standard_extended_weekly']->type}}</h5>
                    <span class="price">{{$money->format($plans['standard_extended_weekly']->plan_cost)}}</span>
                    @isset($plans['standard_extended_weekly']->starts_at)
                    <div class="uppercase text-center mb8" style="display:block;font-size:0.9em;">Weekdays</div>
                    <div class="uppercase text-center" style="display:block;font-size:0.9em;">{{ date('h:i:s A', strtotime($plans['standard_extended_weekly']->starts_at)) . ' - ' . date('h:i:s A', strtotime($plans['standard_extended_weekly']->ends_at)) }}</div>
                    @endisset
                    @isset($plans['standard_extended_weekly']->weekend_starts_at)
                    <div class="uppercase text-center mb8" style="display:block;font-size:0.9em;">Saturdays</div>
                    <div class="uppercase text-center" style="display:block;font-size:0.9em;">{{ date('h:i:s A', strtotime($plans['standard_extended_weekly']->weekend_starts_at)) . ' - ' . date('h:i:s A', strtotime($plans['standard_extended_weekly']->weekend_ends_at)) }}</div>
                    @endisset
                    <a class="btn btn-white btn-lg" href="{{ route('subscribe', $plans['standard_extended_weekly']) }}">Get Started</a>
                    <ul>
                        <li>
                            <strong>Unlimited</strong> Internet Access
                        </li>
                        <li>
                            <strong>FREE</strong> Beverages
                        </li>
                    </ul>
                </div>
                <!--end of pricing table-->
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="pricing-table pt-2 boxed text-center mb8">
                    <h5 class="uppercase">{{$plans['premium_extended_weekly']->type}}</h5>
                    <span class="price">{{$money->format($plans['premium_extended_weekly']->plan_cost)}}</span>
                    @isset($plans['premium_extended_weekly']->starts_at)
                    <div class="uppercase text-center mb8" style="display:block;font-size:0.9em;">Weekdays</div>
                    <div class="uppercase text-center" style="display:block;font-size:0.9em;">{{ date('h:i:s A', strtotime($plans['premium_extended_weekly']->starts_at)) . ' - ' . date('h:i:s A', strtotime($plans['premium_extended_weekly']->ends_at)) }}</div>
                    @endisset
                    @isset($plans['premium_extended_daily']->weekend_starts_at)
                    <div class="uppercase text-center mb8" style="display:block;font-size:0.9em;">Saturdays</div>
                    <div class="uppercase text-center" style="display:block;font-size:0.9em;">{{ date('h:i:s A', strtotime($plans['premium_extended_weekly']->weekend_starts_at)) . ' - ' . date('h:i:s A', strtotime($plans['premium_extended_weekly']->weekend_ends_at)) }}</div>
                    @endisset
                    <a class="btn btn-filled btn-lg" href="{{ route('subscribe', $plans['premium_extended_weekly']) }}">Get Started</a>
                    <ul>
                        <li>
                            <strong>Unlimited</strong> Internet Access
                        </li>
                        <li>
                            <strong>FREE</strong> Beverages
                        </li>
                    </ul>
                </div>
                <!--end of pricing table-->
            </div>
        </div>
    </div>
</section>

<section id="monthly_use_costs">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <h3 class="uppercase text-center">Monthly Cost Options For Coworking</h3>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="pricing-table pt-2 boxed text-center mb8">
                    <h5 class="uppercase">{{$plans['standard_flexi_monthly']->type}}</h5>
                    <span class="price">{{$money->format($plans['standard_flexi_monthly']->plan_cost)}}</span>
                    <p class="lead"><strong>Three Days Every Week</strong> Per Month</p>
                    @isset($plans['standard_flexi_monthly']->starts_at)
                    <div class="uppercase text-center mb8" style="display:block;font-size:0.9em;">Mondays - Fridays</div>
                    <div class="uppercase text-center" style="display:block;font-size:0.9em;">{{ date('h:i:s A', strtotime($plans['standard_flexi_monthly']->starts_at)) . ' - ' . date('h:i:s A', strtotime($plans['standard_flexi_monthly']->ends_at)) }}</div>
                    @endisset
                    @isset($plans['standard_flexi_monthly']->weekend_starts_at)
                    <div class="uppercase text-center mb8" style="display:block;font-size:0.9em;">Saturdays</div>
                    <div class="uppercase text-center" style="display:block;font-size:0.9em;">{{date('h:i:s A', strtotime($plans['standard_flexi_monthly']->weekend_starts_at)) . ' - ' . date('h:i:s A', strtotime($plans['standard_flexi_monthly']->weekend_ends_at)) }}</div>
                    @endisset
                    <a class="btn btn-filled btn-lg" href="{{ route('subscribe', $plans['standard_flexi_monthly']) }}">Get Started</a>
                    <ul>
                        <li>
                            <strong>Unlimited</strong> Internet Access
                        </li>
                        <li>
                            <strong>FREE</strong> Beverages
                        </li>
                        <li>
                            <strong>Conference Room</strong> Access
                        </li>
                    </ul>
                </div>
                <!--end of pricing table-->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="pricing-table pt-2 emphasis text-center mb8">
                    <h5 class="uppercase">{{ $plans['standard_extended_monthly']->type }}</h5>
                        <span class="price">{{$money->format($plans['standard_extended_monthly']->plan_cost)}}</span>
                        <p class="lead">Per Month</p>
                        @isset($plans['standard_extended_monthly']->starts_at)
                        <div class="uppercase text-center mb8" style="display:block;font-size:0.9em;">Mondays - Fridays</div>
                        <strong class="uppercase text-center" style="display:block;font-size:0.9em;">{{ date('h:i:s A', strtotime($plans['standard_extended_monthly']->starts_at )) . ' - ' . date('h:i:s A', strtotime($plans['standard_extended_monthly']->ends_at)) }}</strong>                            
                        @endisset
                        @isset($plans['standard_extended_monthly']->weekend_starts_at)
                        <div class="uppercase text-center mb8" style="display:block;font-size:0.9em;">Saturdays</div>
                        <strong class="uppercase text-center" style="display:block;font-size:0.9em;">{{ date('h:i:s A', strtotime($plans['standard_extended_monthly']->weekend_starts_at )) . ' - ' . date('h:i:s A', strtotime($plans['standard_extended_monthly']->weekend_ends_at)) }}</strong>
                        @endisset
                    <a class="btn btn-white btn-lg" href="{{ route('subscribe', $plans['standard_extended_monthly']) }}">Get Started</a>
                    <ul>
                        <li>
                            <strong>Unlimited</strong> Internet Access
                        </li>
                        <li>
                            <strong>FREE</strong> Beverages
                        </li>
                        <li>
                            <strong>Conference Room</strong> Access
                        </li>
                    </ul>
                </div>
                <!--end of pricing table-->
            </div>
            {{-- <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="pricing-table pt-2 boxed text-center mb8">
                    <h5 class="uppercase">{{$plans['premium_normal']->type}}</h5>
                        <span class="price">{{$money->format($plans['premium_normal']->plan_cost)}}</span>
                        <p class="lead">Per Month</p>
                        @isset($plans['premium_normal']->starts_at)
                        <div class="uppercase text-center mb8" style="display:block;font-size:0.9em;">Mondays - Fridays</div>
                        <strong class="uppercase text-center" style="display:block;font-size:0.9em;">{{ date('h:i:s A', strtotime($plans['premium_normal']->starts_at )) . ' - ' . date('h:i:s A', strtotime($plans['premium_normal']->ends_at)) }}</strong>
                        @endisset
                        @isset($plans['premium_normal']->weekend_starts_at)
                        <div class="uppercase text-center mb8" style="display:block;font-size:0.9em;">Saturdays</div>
                        <strong class="uppercase text-center" style="display:block;font-size:0.9em;">{{ date('h:i:s A', strtotime($plans['premium_normal']->weekend_starts_at)) . ' - ' . date('h:i:s A', strtotime($plans['premium_normal']->weekend_ends_at )) }}</strong>
                        @endisset
                    <a class="btn btn-filled btn-lg" href="{{ route('subscribe', $plans['premium_normal']) }}">Get Started</a>
                    <ul>
                        <li>
                            <strong>Unlimited</strong> Internet Access
                        </li>
                        <li>
                            <strong>FREE</strong> Beverages
                        </li>
                        <li>
                            <strong>Conference Room</strong> Access
                        </li>
                    </ul>
                </div> 
                <!--end of pricing table-->
            </div> --}}
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="pricing-table pt-2 boxed text-center mb8">
                <h5 class="uppercase">{{ $plans['premium_extended_monthly']->type }}</h5>
                    <span class="price">{{$money->format($plans['premium_extended_monthly']->plan_cost ) }}</span>
                    <p class="lead">Per Month</p>
                    @isset($plans['premium_extended_monthly']->starts_at)
                    <div class="uppercase text-center mb8" style="display:block;font-size:0.9em;">Mondays - Fridays</div>
                    <strong class="uppercase text-center" style="display:block;font-size:0.9em;">{{ date('h:i:s A', strtotime($plans['premium_extended_monthly']->starts_at)) . ' - ' . date('h:i:s A', strtotime($plans['premium_extended_monthly']->ends_at)) }}</strong>
                    @endisset
                    @isset($plans['premium_extended_monthly']->weekend_starts_at)
                    <div class="uppercase text-center mb8" style="display:block;font-size:0.9em;">Saturdays</div>
                    <strong class="uppercase text-center" style="display:block;font-size:0.9em;">{{ date('h:i:s A', strtotime($plans['premium_extended_monthly']->weekend_starts_at)) . ' - ' . date('h:i:s A', strtotime($plans['premium_extended_monthly']->weekend_ends_at)) }}</strong>
                    @endisset
                    <a class="btn btn-filled btn-lg" href="{{ route('subscribe', $plans['premium_extended_monthly']) }}">Get Started</a>
                    <ul>
                        <li>
                            <strong>Unlimited</strong> Internet Access
                        </li>
                        <li>
                            <strong>FREE</strong> Beverages
                        </li>
                        <li>
                            <strong>Conference Room</strong> Access
                        </li>
                    </ul>
                </div>
                <!--end of pricing table-->
            </div>
        </div>
    </div>
</section>

<section class="cover fullscreen parallax" id="conference_info">
    <ul class="slides">
        <li class="overlay image-bg flex-active-slide">
            <div class="background-image-holder bg-image-adjust-left fadeIn">
                <img src="{{ asset('img/conference room.jpg') }}" alt="Conference Room" class="background-image">
            </div>
            <div class="container v-align-transform">
                <div class="row">
                    <div class="col-md-6 col-sm-8">
                        <h1 class="mb40 mb-xs-16 large">A Fully Equipped Conference Room</h1>
                        <h6 class="uppercase mb16">A Valuable Companion For Making Great Impressions</h6>
                        <p class="lead">
                            Make your pitch or presentations with confidence in our fully equipped conference room. The ambience provides a cool and calming disposition so that all the focus is placed on what is being discussed. <br>Our conference room can accomodate six (6) people confortably and in special cases can be arranged to take ten (10) people. 
                        </p>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</section>
<section id="conference_hourly_options">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <h3 class="uppercase text-center">Hourly Cost Options For Conference Room</h3>
            </div>
            @if (auth()->user())
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
                <div class="pricing-table pt-2 boxed text-center mb8">
                    <h5 class="uppercase">For Members</h5>
                    <span class="price">{{$money->format($room_options['meeting_room_members_hourly']->room_option_cost)}}</span>
                    <p class="lead">Per Hour</p>
                    <a class="btn btn-filled btn-lg" href="{{ route('createBooking', $room_options['meeting_room_members_hourly']) }}">Schedule Meeting</a>
                    <ul>
                        <li>
                            <strong>Unlimited</strong> Internet Access
                        </li>
                        <li>
                            <strong>FREE</strong> Beverages
                        </li>
                        <li>
                            <strong> Confirmed Booking</strong> 24hrs Ahead of Schedule
                        </li>
                    </ul>
                </div>
                <!--end of pricing table-->
            </div>
            @else
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
                <div class="pricing-table pt-2 emphasis text-center mb8">
                    <h5 class="uppercase">For None Hub Members</h5>
                    <span class="price">{{$money->format($room_options['meeting_room_standard_hourly']->room_option_cost)}}</span>
                    <p class="lead">Per Hour</p>
                    <a class="btn btn-white btn-lg" href="{{ route('createBooking', $room_options['meeting_room_standard_hourly']) }}">Schedule Meeting</a>
                    <ul>
                        <li>
                            <strong>Unlimited</strong> Internet Access
                        </li>
                        <li>
                            <strong>FREE</strong> Beverages
                        </li>
                        <li>
                            <strong>Confirmed Booking</strong> 24hrs Ahead of Schedule
                        </li>
                    </ul>
                </div>
                <!--end of pricing table-->
            </div>
            @endif
        </div>
    </div>
</section>
<section id="conference_daily_options">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <h3 class="uppercase text-center">Daily Cost Options For Conference Room</h3>
            </div>
            @if (auth()->user())
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
                <div class="pricing-table pt-2 boxed text-center mb8">
                    <h5 class="uppercase">For Members</h5>
                    <span class="price">{{$money->format($room_options['meeting_room_members_daily']->room_option_cost)}}</span>
                    <p class="lead">Per Day</p>
                    <a class="btn btn-filled btn-lg" href="{{ route('createBooking', $room_options['meeting_room_members_daily']) }}">Schedule Meeting</a>
                    <ul>
                        <li>
                            <strong>Unlimited</strong> Internet Access
                        </li>
                        <li>
                            <strong>FREE</strong> Beverages
                        </li>
                        <li>
                            <strong> Confirmed Booking</strong> 24hrs Ahead of Schedule
                        </li>
                    </ul>
                </div>
                <!--end of pricing table-->
            </div>
            @else
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
                <div class="pricing-table pt-2 emphasis text-center mb8">
                    <h5 class="uppercase">For None Hub Members</h5>
                    <span class="price">{{$money->format($room_options['meeting_room_standard_daily']->room_option_cost)}}</span>
                    <p class="lead">Per Day</p>
                    <a class="btn btn-white btn-lg" href="{{ route('createBooking', $room_options['meeting_room_standard_daily']) }}">Schedule Meeting</a>
                    <ul>
                        <li>
                            <strong>Unlimited</strong> Internet Access
                        </li>
                        <li>
                            <strong>FREE</strong> Beverages
                        </li>
                        <li>
                            <strong>Confirmed Booking</strong> 24hrs Ahead of Schedule
                        </li>
                    </ul>
                </div>
                <!--end of pricing table-->
            </div>
            @endif
        </div>
    </div>
</section>
</section>
<section class="cover fullscreen parallax" id="training_info">
    <ul class="slides">
        <li class="overlay image-bg flex-active-slide">
            <div class="background-image-holder bg-image-adjust-left fadeIn">
                <img src="{{ asset('img/training room.jpg') }}" alt="Training Room" class="background-image">
            </div>
            <div class="container v-align-transform">
                <div class="row">
                    <div class="col-md-6 col-sm-8">
                        <h1 class="mb40 mb-xs-16 large">Training Room</h1>
                        <h6 class="uppercase mb16">For Large Scale Presentations</h6>
                        <p class="lead">
                            Pass your message clearly and professionally with our well equipped training room. Complete with Fast Internet Access for participants as well as a white board with a projector for all to follow along.<br/> The training room has a capacity to take Twenty (20) students.
                        </p>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</section>
<section id="training_hourly_options">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-12">
                <h3 class="uppercase text-center">Hourly Use Options For Training Room</h3>
            </div>
            @if (auth()->user())
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
                <div class="pricing-table pt-2 boxed text-center mb8">
                    <h5 class="uppercase">For Members</h5>
                    <span class="price">{{$money->format($room_options['training_room_members_hourly']->room_option_cost)}}</span>
                    <p class="lead">Per Hour</p>
                    <a class="btn btn-filled btn-lg" href="{{ route('createBooking', $room_options['training_room_members_hourly']) }}">Get Started</a>
                    <ul>
                        <li>
                            <strong>Unlimited</strong> Internet Access
                        </li>
                        <li>
                            <strong>FREE</strong> Beverages
                        </li>
                        <li>
                            <strong>Confirmed Booking</strong> At least 24hrs Ahead of Schedule
                        </li>
                    </ul>
                </div>
                <!--end of pricing table-->
            </div>
            @else
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
                <div class="pricing-table pt-2 emphasis text-center mb8">
                    <h5 class="uppercase">For None Hub Members</h5>
                    <span class="price">{{$money->format($room_options['training_room_standard_hourly']->room_option_cost)}}</span>
                    <p class="lead">Per Hour</p>
                    <a class="btn btn-white btn-lg" href="{{ route('createBooking', $room_options['training_room_standard_hourly']) }}">Get Started</a>
                    <ul>
                        <li>
                            <strong>Unlimited</strong> Internet Access
                        </li>
                        <li>
                            <strong>FREE</strong> Beverages
                        </li>
                        <li>
                            <strong>Confirmed Booking</strong> At least 24hrs Ahead of Schedule
                        </li>
                    </ul>
                </div>
                <!--end of pricing table-->
            </div>
            @endif
        </div>
    </div>
</section>
<section id="training_daily_options">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <h3 class="uppercase text-center">Daily Use Options For Training Room</h3>
            </div>
            @if (auth()->user())
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
                <div class="pricing-table pt-2 boxed text-center mb8">
                    <h5 class="uppercase">For Members</h5>
                    <span class="price">{{$money->format($room_options['training_room_members_daily']->room_option_cost)}}</span>
                    <p class="lead">Per Day</p>
                    <a class="btn btn-filled btn-lg" href="{{ route('createBooking', $room_options['training_room_members_daily']) }}">Get Started</a>
                    <ul>
                        <li>
                            <strong>Unlimited</strong> Internet Access
                        </li>
                        <li>
                            <strong>FREE</strong> Beverages
                        </li>
                        <li>
                            <strong>Confirmed Booking</strong> At least 24hrs Ahead of Schedule
                        </li>
                    </ul>
                </div>
                <!--end of pricing table-->
            </div>    
            @else
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
                <div class="pricing-table pt-2 emphasis text-center mb8">
                    <h5 class="uppercase">For None Hub Members</h5>
                    <span class="price">{{$money->format($room_options['training_room_standard_daily']->room_option_cost)}}</span>
                    <p class="lead">Per Day</p>
                    <a class="btn btn-white btn-lg" href="{{ route('createBooking', $room_options['training_room_standard_daily']) }}">Get Started</a>
                    <ul>
                        <li>
                            <strong>Unlimited</strong> Internet Access
                        </li>
                        <li>
                            <strong>FREE</strong> Beverages
                        </li>
                        <li>
                            <strong>Confirmed Booking</strong> At least 24hrs Ahead of Schedule
                        </li>
                    </ul>
                </div>
                <!--end of pricing table-->
            </div>
            @endif
        </div>
    </div>
</section>
@endsection

@section('jsScripts')
    @parent
@endsection