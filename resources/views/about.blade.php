@extends ('layouts.main')

@section('title', 'About')

@section ('content')
    <div class="main-container">
        <section class="page-title page-title-2 image-bg overlay parallax">
            <div class="background-image-holder">
                <img alt="Background Image" class="background-image" src="img/cover11.jpg" />
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="uppercase mb8">About Us</h2>
                        <p class="lead mb0">A descriptive subtitle for your page.</p>
                    </div>
                    <div class="col-md-6 text-right">
                        <ol class="breadcrumb breadcrumb-2">
                            <li>
                                <a href="#">Home</a>
                            </li>
                            <li>
                                <a href="#">Pages</a>
                            </li>
                            <li class="active">About Us</li>
                        </ol>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 text-center">
                        <h3 class="mb64 mb-xs-24">Diversity and difference are our guiding principles.
                            <br /> Our approach is tailored and unique to each client.</h3>
                        <div class="tabbed-content button-tabs">
                            <ul class="tabs">
                                <li class="active">
                                    <div class="tab-title">
                                        <span>History</span>
                                    </div>
                                    <div class="tab-content">
                                        <p>
                                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.
                                        </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="tab-title">
                                        <span>Approach</span>
                                    </div>
                                    <div class="tab-content">
                                        <p>
                                            Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
                                        </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="tab-title">
                                        <span>Culture</span>
                                    </div>
                                    <div class="tab-content">
                                        <p>
                                            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est.
                                        </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="tab-title">
                                        <span>Method</span>
                                    </div>
                                    <div class="tab-content">
                                        <p>
                                            Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!--end of button tabs-->
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <section class="p0">
            <div class="containr">
                <div class="row">
                    <div class="col-md-4 col-sm-6 p0">
                        <div class="image-tile inner-title hover-reveal text-center mb0">
                            <img alt="Pic" src="img/team-1.jpg" />
                            <div class="title">
                                <h5 class="uppercase mb0">Sally Marsh</h5>
                                <span>Creative Director</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 p0">
                        <div class="image-tile inner-title hover-reveal text-center mb0">
                            <img alt="Pic" src="img/team-2.jpg" />
                            <div class="title">
                                <h5 class="uppercase mb0">Tim Foley</h5>
                                <span>iOS Developer</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 p0">
                        <div class="image-tile inner-title hover-reveal text-center mb0">
                            <img alt="Pic" src="img/team-3.jpg" />
                            <div class="title">
                                <h5 class="uppercase mb0">Jake Robbins</h5>
                                <span>Brand Director</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h4 class="uppercase mb64 mb-xs-40">Previous Collaborators</h4>
                    </div>
                </div>
                <!--end of row-->
                <div class="row">
                    <div class="logo-carousel">
                        <ul class="slides">
                            <li>
                                <a href="#">
                                    <img alt="Logo" src="img/l1.png" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="Logo" src="img/l2.png" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="Logo" src="img/l3.png" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="Logo" src="img/l4.png" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="Logo" src="img/l1.png" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="Logo" src="img/l2.png" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="Logo" src="img/l3.png" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="Logo" src="img/l4.png" />
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!--end of logo slider-->
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <section class="bg-dark pt64 pb64">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2 class="mb8">Start A Foundry Project</h2>
                        <p class="lead mb40 mb-xs-24">
                            Variant Page Builder, Over 100 Page Templates - The choice is clear.
                        </p>
                        <a class="btn btn-filled btn-lg mb0" href="#">Purchase Foundry</a>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <footer class="footer-1 bg-dark">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <img alt="Logo" class="logo" src="img/logo-light.png" />
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h6 class="title">Recent Posts</h6>
                            <hr>
                            <ul class="link-list recent-posts">
                                <li>
                                    <a href="#">Hugging pugs is super trendy</a>
                                    <span class="date">February
                                        <span class="number">14, 2015</span>
                                    </span>
                                </li>
                                <li>
                                    <a href="#">Spinning vinyl is oh so cool</a>
                                    <span class="date">February
                                        <span class="number">9, 2015</span>
                                    </span>
                                </li>
                                <li>
                                    <a href="#">Superior theme design by pros</a>
                                    <span class="date">January
                                        <span class="number">27, 2015</span>
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <!--end of widget-->
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h6 class="title">Latest Updates</h6>
                            <hr>
                            <div class="twitter-feed">
                                <div class="tweets-feed" data-feed-name="hub_impression">
                                </div>
                            </div>
                        </div>
                        <!--end of widget-->
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h6 class="title">Instagram</h6>
                            <hr>
                            <div class="instafeed" data-user-name="d_impressionhub">
                                <ul></ul>
                            </div>
                        </div>
                        <!--end of widget-->
                    </div>
                </div>
                <!--end of row-->
                <div class="row">
                    <div class="col-sm-6">
                        <span class="sub">&copy; Copyright 2017 - Impression Hub</span>
                    </div>
                    <div class="col-sm-6 text-right">
                        <ul class="list-inline social-list">
                            <li>
                                <a href="#">
                                    <i class="ti-twitter-alt"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="ti-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="ti-dribbble"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="ti-vimeo-alt"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--end of container-->
            <a class="btn btn-sm fade-half back-to-top inner-link" href="#top">Top</a>
        </footer>
    </div>
    @section('jsScripts')
        @parent
    @endsection
@endsection