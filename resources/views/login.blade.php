@extends('layouts.main')

@section('title', 'Login')

@section ('content')
    <div class="main-container">
        <section class="cover fullscreen image-bg overlay">
            <div class="background-image-holder">
                <img alt="image" class="background-image" src="img/home12.jpg" />
            </div>
            <div class="container v-align-transform">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">
                        <div class="feature bordered text-center">
                            <h4 class="uppercase">Login Here</h4>
                            <form class="text-left" action="{{ route('login') }}" method="POST">
                                @csrf
                                <input class="mb0" name="email" type="text" placeholder="Email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <input class="mb0" name="password" type="password" placeholder="Password" required autocomplete="current-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <input type="submit" value="Login" />
                                <span class="mb0">Remember Me</span>
                                <div class="checkbox-option pull-right">
                                    <div class="inner"></div>
                                    <input type="checkbox" name="remember" value="yes">
                                </div>
                            </form>
                            <p class="mb0">Forgot your password?
                                <a href="{{ route('password.request') }}">Click Here To Reset</a>
                            </p>
                        </div>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
    </div>
    @section('jsScripts')
        @parent
    @endsection
@endsection        