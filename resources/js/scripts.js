const { event } = require("jquery");
const { forEach, add, isArray } = require("lodash");

var mr_firstSectionHeight,
    mr_nav,
    mr_fixedAt,
    mr_navOuterHeight,
    mr_navScrolled = false,
    mr_navFixed = false,
    mr_outOfSight = false,
    mr_floatingProjectSections,
    mr_scrollTop = 0;

$(document).ready(function() { 
    "use strict";

    // Smooth scroll to inner links
        var innerLinks = $('a.inner-link');

        if(innerLinks.length){
            innerLinks.each(function(){
                var link = $(this);
                var href = link.attr('href');
                if(href.charAt(0) !== "#"){
                    link.removeClass('inner-link');
                }
            });

            var offset = 0;
            if($('body[data-smooth-scroll-offset]').length){
                offset = $('body').attr('data-smooth-scroll-offset');
                offset = offset*1;
            }
            
            smoothScroll.init({
                selector: '.inner-link',
                selectorHeader: null,
                speed: 750,
                easing: 'easeInOutCubic',
                offset: offset
            });
        }

    // Update scroll variable for scrolling functions

    addEventListener('scroll', function() {
        mr_scrollTop = window.pageYOffset;
    }, false);

    // Append .background-image-holder <img>'s as CSS backgrounds

    $('.background-image-holder').each(function() {
        var imgSrc = $(this).children('img').attr('src');
        $(this).css('background', 'url("' + imgSrc + '")');
        $(this).children('img').hide();
        $(this).css('background-position', 'initial');
    });

    // Fade in background images

    setTimeout(function() {
        $('.background-image-holder').each(function() {
            $(this).addClass('fadeIn');
        });
    }, 200);

    // Initialize Tooltips

    $('[data-toggle="tooltip"]').tooltip();

    // Icon bulleted lists

    $('ul[data-bullet]').each(function(){
        var bullet = $(this).attr('data-bullet');
        $(this).find('li').prepend('<i class="'+bullet+'"></i>');
    });

    // Progress Bars

    $('.progress-bar').each(function() {
        $(this).css('width', $(this).attr('data-progress') + '%');
    });

    // Navigation

    if (!$('nav').hasClass('fixed') && !$('nav').hasClass('absolute')) {

        // Make nav container height of nav

        $('.nav-container').css('min-height', $('nav').outerHeight(true));

        $(window).resize(function() {
            $('.nav-container').css('min-height', $('nav').outerHeight(true));
        });

        // Compensate the height of parallax element for inline nav

        if ($(window).width() > 768) {
            $('.parallax:nth-of-type(1) .background-image-holder').css('top', -($('nav').outerHeight(true)));
        }

        // Adjust fullscreen elements

        if ($(window).width() > 768) {
            $('section.fullscreen:nth-of-type(1)').css('height', ($(window).height() - $('nav').outerHeight(true)));
        }

    } else {
        $('body').addClass('nav-is-overlay');
    }

    if ($('nav').hasClass('bg-dark')) {
        $('.nav-container').addClass('bg-dark');
    }


    // Fix nav to top while scrolling

    mr_nav = $('body .nav-container nav:first');
    mr_navOuterHeight = $('body .nav-container nav:first').outerHeight();
        mr_fixedAt = typeof mr_nav.attr('data-fixed-at') !== typeof undefined ? parseInt(mr_nav.attr('data-fixed-at').replace('px', '')) : parseInt($('section:nth-of-type(1)').outerHeight());
    window.addEventListener("scroll", updateNav, false);

    // Menu dropdown positioning

    $('.menu > li > ul').each(function() {
        var menu = $(this).offset();
        var farRight = menu.left + $(this).outerWidth(true);
        if (farRight > $(window).width() && !$(this).hasClass('mega-menu')) {
            $(this).addClass('make-right');
        } else if (farRight > $(window).width() && $(this).hasClass('mega-menu')) {
            var isOnScreen = $(window).width() - menu.left;
            var difference = $(this).outerWidth(true) - isOnScreen;
            $(this).css('margin-left', -(difference));
        }
    });

    // Mobile Menu

    $('.mobile-toggle').click(function() {
        $('.nav-bar').toggleClass('nav-open');
        $(this).toggleClass('active');
    });

    $('.menu li').click(function(e) {
        if (!e) e = window.event;
        e.stopPropagation();
        if ($(this).find('ul').length) {
            $(this).toggleClass('toggle-sub');
        } else {
            $(this).parents('.toggle-sub').removeClass('toggle-sub');
        }
    });

    $('.menu li a').click(function() {
        if ($(this).hasClass('inner-link')){
            $(this).closest('.nav-bar').removeClass('nav-open');
        }
    });

    $('.module.widget-handle').click(function() {
        $(this).toggleClass('toggle-widget-handle');
    });

    $('.search-widget-handle .search-form input').click(function(e){
        if (!e) e = window.event;
        e.stopPropagation();
    });
    
    // Offscreen Nav
    
    if($('.offscreen-toggle').length){
    	$('body').addClass('has-offscreen-nav');
    }
    else{
        $('body').removeClass('has-offscreen-nav');
    }
    
    $('.offscreen-toggle').click(function(){
    	$('.main-container').toggleClass('reveal-nav');
    	$('nav').toggleClass('reveal-nav');
    	$('.offscreen-container').toggleClass('reveal-nav');
    });
    
    $('.main-container').click(function(){
    	if($(this).hasClass('reveal-nav')){
    		$(this).removeClass('reveal-nav');
    		$('.offscreen-container').removeClass('reveal-nav');
    		$('nav').removeClass('reveal-nav');
    	}
    });
    
    $('.offscreen-container a').click(function(){
    	$('.offscreen-container').removeClass('reveal-nav');
    	$('.main-container').removeClass('reveal-nav');
    	$('nav').removeClass('reveal-nav');
    });

    // Populate filters
    
    $('.projects').each(function() {

        var filters = "";

        $(this).find('.project').each(function() {

            var filterTags = $(this).attr('data-filter').split(',');

            filterTags.forEach(function(tagName) {
                if (filters.indexOf(tagName) == -1) {
                    filters += '<li data-filter="' + tagName + '">' + capitaliseFirstLetter(tagName) + '</li>';
                }
            });
            $(this).closest('.projects')
                .find('ul.filters').empty().append('<li data-filter="all" class="active">All</li>').append(filters);
        });
    });

    $('.filters li').click(function() {
        var filter = $(this).attr('data-filter');
        $(this).closest('.filters').find('li').removeClass('active');
        $(this).addClass('active');

        $(this).closest('.projects').find('.project').each(function() {
            var filters = $(this).attr('data-filter');

            if (filters.indexOf(filter) == -1) {
                $(this).addClass('inactive');
            } else {
                $(this).removeClass('inactive');
            }
        });

        if (filter == 'all') {
            $(this).closest('.projects').find('.project').removeClass('inactive');
        }
    });

    // Twitter Feed
       $('.tweets-feed').each(function(index) {
           jQuery(this).attr('id', 'tweets-' + index);
       }).each(function(index) {
           var element = $('#tweets-' + index);
           var TweetConfig = {
               "domId": '',
               "maxTweets": element.attr('data-amount'),
               "enableLinks": true,
               "showUser": true,
               "showTime": true,
               "dateFunction": '',
               "showRetweet": false,
               "customCallback": handleTweets
           };

           if(typeof element.attr('data-widget-id') !== typeof undefined){
                TweetConfig.id = element.attr('data-widget-id');
            }else if(typeof element.attr('data-feed-name') !== typeof undefined && element.attr('data-feed-name') !== "" ){
                TweetConfig.profile = {"screenName": element.attr('data-feed-name').replace('@', '')};
            }else{
                TweetConfig.profile = {"screenName": 'twitter'};
            }

           function handleTweets(tweets) {
               var x = tweets.length;
               var n = 0;
               var element = document.getElementById('tweets-' + index);
               var html = '<ul class="slides">';
               while (n < x) {
                   html += '<li>' + tweets[n] + '</li>';
                   n++;
               }
               html += '</ul>';
               element.innerHTML = html;

               if ($('.tweets-slider').length) {
                    $('.tweets-slider').flexslider({
                        directionNav: false,
                        controlNav: false
                    });
                }       
               return html;
           }
           twitterFetcher.fetch(TweetConfig);
      });

    // Instagram Feed
    
    if($('.instafeed').length){
    	jQuery.fn.spectragram.accessData = {
			accessToken: '1406933036.dc95b96.2ed56eddc62f41cbb22c1573d58625a2',
			clientID: '87e6d2b8a0ef4c7ab8bc45e80ddd0c6a'
		};	

        $('.instafeed').each(function() {
            var feedID = $(this).attr('data-user-name');
            $(this).children('ul').spectragram('getUserFeed', {
                query: feedID,
                max: 12
            });
        });
    }   

   

    // Flickr Feeds

    if($('.flickr-feed').length){
        $('.flickr-feed').each(function(){
            var userID = $(this).attr('data-user-id');
            var albumID = $(this).attr('data-album-id');
            $(this).flickrPhotoStream({ id: userID, setId: albumID, container: '<li class="masonry-item" />' });   
            setTimeout(function(){
                initializeMasonry();
                window.dispatchEvent(new Event('resize'));
            }, 1000); 
        });

    }

    // Image Sliders
    if($('.slider-all-controls, .slider-paging-controls, .slider-arrow-controls, .slider-thumb-controls, .logo-carousel').length){
        $('.slider-all-controls').flexslider({
            start: function(slider){
                if(slider.find('.slides li:first-child').find('.fs-vid-background video').length){
                   slider.find('.slides li:first-child').find('.fs-vid-background video').get(0).play(); 
                }
            },
            after: function(slider){
                if(slider.find('.fs-vid-background video').length){
                    if(slider.find('li:not(.flex-active-slide)').find('.fs-vid-background video').length){
                        slider.find('li:not(.flex-active-slide)').find('.fs-vid-background video').get(0).pause();
                    }
                    if(slider.find('.flex-active-slide').find('.fs-vid-background video').length){
                        slider.find('.flex-active-slide').find('.fs-vid-background video').get(0).play();
                    }
                }
            }
        });
        $('.slider-paging-controls').flexslider({
            animation: "slide",
            directionNav: false
        });
        $('.slider-arrow-controls').flexslider({
            controlNav: false
        });
        $('.slider-thumb-controls .slides li').each(function() {
            var imgSrc = $(this).find('img').attr('src');
            $(this).attr('data-thumb', imgSrc);
        });
        $('.slider-thumb-controls').flexslider({
            animation: "slide",
            controlNav: "thumbnails",
            directionNav: true
        });
        $('.logo-carousel').flexslider({
            minItems: 1,
            maxItems: 4,
            move: 1,
            itemWidth: 200,
            itemMargin: 0,
            animation: "slide",
            slideshow: true,
            slideshowSpeed: 3000,
            directionNav: false,
            controlNav: false
        });
    }
    
    // Lightbox gallery titles
    
    $('.lightbox-grid li a').each(function(){
    	var galleryTitle = $(this).closest('.lightbox-grid').attr('data-gallery-title');
    	$(this).attr('data-lightbox', galleryTitle);
    });

    // Prepare embedded video modals

    $('iframe[data-provider]').each(function(){
        var provider = jQuery(this).attr('data-provider');
        var videoID = jQuery(this).attr('data-video-id');
        var autoplay = jQuery(this).attr('data-autoplay');
        var vidURL = '';

        if(provider == 'vimeo'){
            vidURL = "http://player.vimeo.com/video/"+videoID+"?badge=0&title=0&byline=0&title=0&autoplay="+autoplay;
            $(this).attr('data-src', vidURL);
        }else if (provider == 'youtube'){
            vidURL = "https://www.youtube.com/embed/"+videoID+"?showinfo=0&autoplay="+autoplay;
            $(this).attr('data-src', vidURL);
        }else{
            console.log('Only Vimeo and Youtube videos are supported at this time');
        }
    });
    
    // Multipurpose Modals
    
    jQuery('.foundry_modal[modal-link]').remove();

    if($('.foundry_modal').length && (!jQuery('.modal-screen').length)){
        // Add a div.modal-screen if there isn't already one there.
        var modalScreen = jQuery('<div />').addClass('modal-screen').appendTo('body');

    }

    jQuery('.foundry_modal').click(function(){
        jQuery(this).addClass('modal-acknowledged');
    });

    jQuery(document).on('wheel mousewheel scroll', '.foundry_modal, .modal-screen', function(evt){
        $(this).get(0).scrollTop += (evt.originalEvent.deltaY); 
        return false;
    });
    
    $('.modal-container:not([modal-link])').each(function(index) {
        if(jQuery(this).find('iframe[src]').length){
        	jQuery(this).find('.foundry_modal').addClass('iframe-modal');
        	var iframe = jQuery(this).find('iframe');
        	iframe.attr('data-src',iframe.attr('src'));
            iframe.attr('src', '');

        }
        jQuery(this).find('.btn-modal').attr('modal-link', index);

        // Only clone and append to body if there isn't already one there
        if(!jQuery('.foundry_modal[modal-link="'+index+'"]').length){
            jQuery(this).find('.foundry_modal').clone().appendTo('body').attr('modal-link', index).prepend(jQuery('<i class="ti-close close-modal">'));
        }
    });
    
    $('.btn-modal').unbind('click').click(function(){
    	var linkedModal = jQuery('.foundry_modal[modal-link="' + jQuery(this).attr('modal-link') + '"]'),
            autoplayMsg = "";
        jQuery('.modal-screen').toggleClass('reveal-modal');
        if(linkedModal.find('iframe').length){
            if(linkedModal.find('iframe').attr('data-autoplay') === '1'){
                var autoplayMsg = '&autoplay=1'
            }
        	linkedModal.find('iframe').attr('src', (linkedModal.find('iframe').attr('data-src') + autoplayMsg));
        }
        if(linkedModal.find('video').length){
            linkedModal.find('video').get(0).play();
        }
        linkedModal.toggleClass('reveal-modal');
        return false; 
    });
    
    // Autoshow modals
	
	$('.foundry_modal[data-time-delay]').each(function(){
		var modal = $(this);
		var delay = modal.attr('data-time-delay');
		modal.prepend($('<i class="ti-close close-modal">'));
    	if(typeof modal.attr('data-cookie') != "undefined"){
        	if(!mr_cookies.hasItem(modal.attr('data-cookie'))){
                setTimeout(function(){
        			modal.addClass('reveal-modal');
        			$('.modal-screen').addClass('reveal-modal');
        		},delay);
            }
        }else{
            setTimeout(function(){
                modal.addClass('reveal-modal');
                $('.modal-screen').addClass('reveal-modal');
            },delay);
        }
	});

    // Exit modals
    $('.foundry_modal[data-show-on-exit]').each(function(){
        var modal = $(this);
        var exitSelector = $(modal.attr('data-show-on-exit'));
        // If a valid selector is found, attach leave event to show modal.
        if($(exitSelector).length){
            modal.prepend($('<i class="ti-close close-modal">'));
            $(document).on('mouseleave', exitSelector, function(){
                if(!$('body .reveal-modal').length){
                    if(typeof modal.attr('data-cookie') !== typeof undefined){
                        if(!mr_cookies.hasItem(modal.attr('data-cookie'))){
                            modal.addClass('reveal-modal');
                            $('.modal-screen').addClass('reveal-modal');
                        }
                    }else{
                        modal.addClass('reveal-modal');
                        $('.modal-screen').addClass('reveal-modal');
                    }
                }
            });
        }
    });

    // Autoclose modals

    $('.foundry_modal[data-hide-after]').each(function(){
        var modal = $(this);
        var delay = modal.attr('data-hide-after');
        if(typeof modal.attr('data-cookie') != "undefined"){
            if(!mr_cookies.hasItem(modal.attr('data-cookie'))){
                setTimeout(function(){
                if(!modal.hasClass('modal-acknowledged')){
                    modal.removeClass('reveal-modal');
                    $('.modal-screen').removeClass('reveal-modal');
                }
                },delay); 
            }
        }else{
            setTimeout(function(){
                if(!modal.hasClass('modal-acknowledged')){
                    modal.removeClass('reveal-modal');
                    $('.modal-screen').removeClass('reveal-modal');
                }
            },delay); 
        }
    });
    
    jQuery('.close-modal:not(.modal-strip .close-modal)').unbind('click').click(function(){
    	var modal = jQuery(this).closest('.foundry_modal');
        modal.toggleClass('reveal-modal');
        if(typeof modal.attr('data-cookie') !== "undefined"){
            mr_cookies.setItem(modal.attr('data-cookie'), "true", Infinity);
        }
    	if(modal.find('iframe').length){
            modal.find('iframe').attr('src', '');
        }
        jQuery('.modal-screen').removeClass('reveal-modal');
    });
    
    jQuery('.modal-screen').unbind('click').click(function(){
        if(jQuery('.foundry_modal.reveal-modal').find('iframe').length){
            jQuery('.foundry_modal.reveal-modal').find('iframe').attr('src', '');
        }
    	jQuery('.foundry_modal.reveal-modal').toggleClass('reveal-modal');
    	jQuery(this).toggleClass('reveal-modal');
    });
    
    jQuery(document).keyup(function(e) {
		 if (e.keyCode == 27) { // escape key maps to keycode `27`
            if(jQuery('.foundry_modal').find('iframe').length){
                jQuery('.foundry_modal').find('iframe').attr('src', '');
            }
			jQuery('.foundry_modal').removeClass('reveal-modal');
			jQuery('.modal-screen').removeClass('reveal-modal');
		}
	});
    
    // Modal Strips
    
    jQuery('.modal-strip').each(function(){
    	if(!jQuery(this).find('.close-modal').length){
    		jQuery(this).append(jQuery('<i class="ti-close close-modal">'));
    	}
    	var modal = jQuery(this);

        if(typeof modal.attr('data-cookie') != "undefined"){
           
            if(!mr_cookies.hasItem(modal.attr('data-cookie'))){
            	setTimeout(function(){
            		modal.addClass('reveal-modal');
            	},1000);
            }
        }else{
            setTimeout(function(){
                    modal.addClass('reveal-modal');
            },1000);
        }
    });
    
    jQuery('.modal-strip .close-modal').click(function(){
        var modal = jQuery(this).closest('.modal-strip');
        if(typeof modal.attr('data-cookie') != "undefined"){
            mr_cookies.setItem(modal.attr('data-cookie'), "true", Infinity);
        }
    	jQuery(this).closest('.modal-strip').removeClass('reveal-modal');
    	return false;
    });


    // Video Modals

    jQuery('.close-iframe').click(function() {
        jQuery(this).closest('.modal-video').removeClass('reveal-modal');
        jQuery(this).siblings('iframe').attr('src', '');
        jQuery(this).siblings('video').get(0).pause();
    });

    // Checkboxes

    $('.checkbox-option').on("click",function() {
        $(this).toggleClass('checked');
        var checkbox = $(this).find('input');
        if (checkbox.prop('checked') === false) {
            checkbox.prop('checked', true);
        } else {
            checkbox.prop('checked', false);
        }
    });

    // Radio Buttons

    $('.radio-option').click(function() {

        var checked = $(this).hasClass('checked'); // Get the current status of the radio

        var name = $(this).find('input').attr('name'); // Get the name of the input clicked

        if (!checked) {

            $('input[name="'+name+'"]').parent().removeClass('checked');

            $(this).addClass('checked');

            $(this).find('input').prop('checked', true);

        }

    });


    // Accordions

    $('.accordion li').click(function() {
        if ($(this).closest('.accordion').hasClass('one-open')) {
            $(this).closest('.accordion').find('li').removeClass('active');
            $(this).addClass('active');
        } else {
            $(this).toggleClass('active');
        }
        if(typeof window.mr_parallax !== "undefined"){
            setTimeout(mr_parallax.windowLoad, 500);
        }
    });

    // Tabbed Content

    $('.tabbed-content').each(function() {
        $(this).append('<ul class="content"></ul>');
    });

    $('.tabs li').each(function() {
        var originalTab = $(this),
            activeClass = "";
        if (originalTab.is('.tabs>li:first-child')) {
            activeClass = ' class="active"';
        }
        var tabContent = originalTab.find('.tab-content').detach().wrap('<li' + activeClass + '></li>').parent();
        originalTab.closest('.tabbed-content').find('.content').append(tabContent);
    });

    $('.tabs li').click(function() {
        $(this).closest('.tabs').find('li').removeClass('active');
        $(this).addClass('active');
        var liIndex = $(this).index() + 1;
        $(this).closest('.tabbed-content').find('.content>li').removeClass('active');
        $(this).closest('.tabbed-content').find('.content>li:nth-of-type(' + liIndex + ')').addClass('active');
    });

    // Local Videos

    $('section').closest('body').find('.local-video-container .play-button').click(function() {
        $(this).siblings('.background-image-holder').removeClass('fadeIn');
        $(this).siblings('.background-image-holder').css('z-index', -1);
        $(this).css('opacity', 0);
        $(this).siblings('video').get(0).play();
    });

    // Youtube Videos

    $('section').closest('body').find('.player').each(function() {
        var section = $(this).closest('section');
        section.find('.container').addClass('fadeOut');
        var src = $(this).attr('data-video-id');
        var startat = $(this).attr('data-start-at');
        $(this).attr('data-property', "{videoURL:'http://youtu.be/" + src + "',containment:'self',autoPlay:true, mute:true, startAt:" + startat + ", opacity:1, showControls:false}");
    });

	if($('.player').length){
        $('.player').each(function(){

            var section = $(this).closest('section');
            var player = section.find('.player');
            player.YTPlayer();
            player.on("YTPStart",function(e){
                section.find('.container').removeClass('fadeOut');
                section.find('.masonry-loader').addClass('fadeOut');
            });

        });
    }

    // Interact with Map once the user has clicked (to prevent scrolling the page = zooming the map

    $('.map-holder').click(function() {
        $(this).addClass('interact');
    });
    
    if($('.map-holder').length){
    	$(window).scroll(function() {
			if ($('.map-holder.interact').length) {
				$('.map-holder.interact').removeClass('interact');
			}
		});
    }
    
    // Countdown Timers

    if ($('.countdown').length) {
        $('.countdown').each(function() {
            var date = $(this).attr('data-date');
            $(this).countdown(date, function(event) {
                $(this).text(
                    event.strftime('%D days %H:%M:%S')
                );
            });
        });
    }
    
    //                                                            //
    //                                                            //
    // Contact form code                                          //
    //                                                            //
    //                                                            //

    $('form.form-email, form.form-newsletter').submit(function(e) {

        // return false so form submits through jQuery rather than reloading page.
        if (e.preventDefault) e.preventDefault();
        else e.returnValue = false;

        var thisForm = $(this).closest('form.form-email, form.form-newsletter'),
            submitButton = thisForm.find('button[type="submit"]'),
            error = 0,
            originalError = thisForm.attr('original-error'),
            preparedForm, iFrame, userEmail, userFullName, userFirstName, userLastName, successRedirect, formError, formSuccess;

        // Mailchimp/Campaign Monitor Mail List Form Scripts
        iFrame = $(thisForm).find('iframe.mail-list-form');

        thisForm.find('.form-error, .form-success').remove();
        submitButton.attr('data-text', submitButton.text());
        thisForm.append('<div class="form-error" style="display: none;">' + thisForm.attr('data-error') + '</div>');
        thisForm.append('<div class="form-success" style="display: none;">' + thisForm.attr('data-success') + '</div>');
        formError = thisForm.find('.form-error');
        formSuccess = thisForm.find('.form-success');
        thisForm.addClass('attempted-submit');

        // Do this if there is an iframe, and it contains usable Mail Chimp / Campaign Monitor iframe embed code
        if ((iFrame.length) && (typeof iFrame.attr('srcdoc') !== "undefined") && (iFrame.attr('srcdoc') !== "")) {

            console.log('Mail list form signup detected.');
            if (typeof originalError !== typeof undefined && originalError !== false) {
                formError.html(originalError);
            }
            userEmail = $(thisForm).find('.signup-email-field').val();
            userFullName = $(thisForm).find('.signup-name-field').val();
            if ($(thisForm).find('input.signup-first-name-field').length) {
                userFirstName = $(thisForm).find('input.signup-first-name-field').val();
            } else {
                userFirstName = $(thisForm).find('.signup-name-field').val();
            }
            userLastName = $(thisForm).find('.signup-last-name-field').val();

            // validateFields returns 1 on error;
            if (validateFields(thisForm) !== 1) {
                preparedForm = prepareSignup(iFrame);

                preparedForm.find('#mce-EMAIL, #fieldEmail').val(userEmail);
                preparedForm.find('#mce-LNAME, #fieldLastName').val(userLastName);
                preparedForm.find('#mce-FNAME, #fieldFirstName').val(userFirstName);
                preparedForm.find('#mce-NAME, #fieldName').val(userFullName);
                thisForm.removeClass('attempted-submit');

                // Hide the error if one was shown
                formError.fadeOut(200);
                // Create a new loading spinner in the submit button.
                submitButton.html(jQuery('<div />').addClass('form-loading')).attr('disabled', 'disabled');
                
                try{
                    $.ajax({
                        url: preparedForm.attr('action'),
                        crossDomain: true,
                        data: preparedForm.serialize(),
                        method: "GET",
                        cache: false,
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function(data){
                            // Request was a success, what was the response?
                            if (data.result != "success" && data.Status != 200) {
                                
                                // Error from Mail Chimp or Campaign Monitor

                                // Keep the current error text in a data attribute on the form
                                formError.attr('original-error', formError.text());
                                // Show the error with the returned error text.
                                formError.html(data.msg).fadeIn(1000);
                                formSuccess.fadeOut(1000);

                                submitButton.html(submitButton.attr('data-text')).removeAttr('disabled');
                            } else {
                                
                                // Got Success from Mail Chimp
                                
                                submitButton.html(submitButton.attr('data-text')).removeAttr('disabled');

                                successRedirect = thisForm.attr('success-redirect');
                                // For some browsers, if empty `successRedirect` is undefined; for others,
                                // `successRedirect` is false.  Check for both.
                                if (typeof successRedirect !== typeof undefined && successRedirect !== false && successRedirect !== "") {
                                    window.location = successRedirect;
                                }

                                thisForm.find('input[type="text"]').val("");
                                thisForm.find('textarea').val("");
                                formSuccess.fadeIn(1000);

                                formError.fadeOut(1000);
                                setTimeout(function() {
                                    formSuccess.fadeOut(500);
                                }, 5000);
                            }
                        }
                    });
                }catch(err){
                    // Keep the current error text in a data attribute on the form
                    formError.attr('original-error', formError.text());
                    // Show the error with the returned error text.
                    formError.html(err.message).fadeIn(1000);
                    formSuccess.fadeOut(1000);
                    setTimeout(function() {
                        formError.fadeOut(500);
                    }, 5000);

                    submitButton.html(submitButton.attr('data-text')).removeAttr('disabled');
                }
            
            } else {
                formError.fadeIn(1000);
                setTimeout(function() {
                    formError.fadeOut(500);
                }, 5000);
            }
        } else {
            // If no iframe detected then this is treated as an email form instead.
            console.log('Send email form detected.');
            if (typeof originalError !== typeof undefined && originalError !== false) {
                formError.text(originalError);
            }

            error = validateFields(thisForm);

            if (error === 1) {
                formError.fadeIn(200);
                setTimeout(function() {
                    formError.fadeOut(500);
                }, 3000);
            } else {

                thisForm.removeClass('attempted-submit');

                // Hide the error if one was shown
                formError.fadeOut(200);
                
                // Create a new loading spinner in the submit button.
                submitButton.html(jQuery('<div />').addClass('form-loading')).attr('disabled', 'disabled');

                jQuery.ajax({
                    type: "POST",
                    // url: "mail/mail.php",
                    url: thisForm.attr('action'),
                    data: thisForm.serialize()+"&url="+window.location.href,
                    success: function(response) {
                        // Swiftmailer always sends back a number representing numner of emails sent.
                        // If this is numeric (not Swift Mailer error text) AND greater than 0 then show success message.

                        submitButton.html(submitButton.attr('data-text')).removeAttr('disabled');

                        if ($.isNumeric(response)) {
                            if (parseInt(response) > 0) {
                                // For some browsers, if empty 'successRedirect' is undefined; for others,
                                // 'successRedirect' is false.  Check for both.
                                successRedirect = thisForm.attr('success-redirect');
                                if (typeof successRedirect !== typeof undefined && successRedirect !== false && successRedirect !== "") {
                                    window.location = successRedirect;
                                }


                                thisForm.find('input[type="text"]').val("");
                                thisForm.find('textarea').val("");
                                thisForm.find('.form-success').fadeIn(1000);

                                formError.fadeOut(1000);
                                setTimeout(function() {
                                    formSuccess.fadeOut(500);
                                }, 5000);
                            }
                        }
                        // If error text was returned, put the text in the .form-error div and show it.
                        else {
                            // Keep the current error text in a data attribute on the form
                            formError.attr('original-error', formError.text());
                            // Show the error with the returned error text.
                            formError.text(response).fadeIn(1000);
                            formSuccess.fadeOut(1000);
                        }
                    },
                    error: function(errorObject, errorText, errorHTTP) {
                        // Keep the current error text in a data attribute on the form
                        formError.attr('original-error', formError.text());
                        // Show the error with the returned error text.
                        formError.text(errorHTTP).fadeIn(1000);
                        formSuccess.fadeOut(1000);
                        submitButton.html(submitButton.attr('data-text')).removeAttr('disabled');
                    }
                });
            }
        }
        return false;
    });

    $('.validate-required, .validate-email').on('blur change', function() {
        validateFields($(this).closest('form'));
    });

    $('form').each(function() {
        if ($(this).find('.form-error').length) {
            $(this).attr('original-error', $(this).find('.form-error').text());
        }
    });

    function validateFields(form) {
            var name, error, originalErrorMessage;

            $(form).find('.validate-required[type="checkbox"]').each(function() {
                if (!$('[name="' + $(this).attr('name') + '"]:checked').length) {
                    error = 1;
                    name = $(this).attr('name').replace('[]', '');
                    form.find('.form-error').text('Please tick at least one ' + name + ' box.');
                }
            });

            $(form).find('.validate-required').each(function() {
                if ($(this).val() === '') {
                    $(this).addClass('field-error');
                    error = 1;
                } else {
                    $(this).removeClass('field-error');
                }
            });

            $(form).find('.validate-email').each(function() {
                if (!(/(.+)@(.+){2,}\.(.+){2,}/.test($(this).val()))) {
                    $(this).addClass('field-error');
                    error = 1;
                } else {
                    $(this).removeClass('field-error');
                }
            });

            if (!form.find('.field-error').length) {
                form.find('.form-error').fadeOut(1000);
            }

            return error;
        }

    //
    //    
    // End contact form code
    //
    //


    // Get referrer from URL string 
    if (getURLParameter("ref")) {
        $('form.form-email').append('<input type="text" name="referrer" class="hidden" value="' + getURLParameter("ref") + '"/>');
    }

    function getURLParameter(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
    }

    // Disable parallax on mobile

    if ((/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)) {
        $('section').removeClass('parallax');
    }
    
    // Disqus Comments
    
    if($('.disqus-comments').length){
		/* * * CONFIGURATION VARIABLES * * */
		var disqus_shortname = $('.disqus-comments').attr('data-shortname');

		/* * * DON'T EDIT BELOW THIS LINE * * */
		(function() {
			var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		})();
    }

    // Load Google MAP API JS with callback to initialise when fully loaded
    if(document.querySelector('[data-maps-api-key]') && !document.querySelector('.gMapsAPI')){
        if($('[data-maps-api-key]').length){
            var script = document.createElement('script');
            var apiKey = $('[data-maps-api-key]:first').attr('data-maps-api-key');
            script.type = 'text/javascript';
            script.src = 'https://maps.googleapis.com/maps/api/js?key='+apiKey+'&callback=initializeMaps';
            script.className = 'gMapsAPI';
            document.body.appendChild(script);  
        } 
    }

}); 

$(window).load(function() { 
    "use strict";

    // Initialize Masonry

    setTimeout(initializeMasonry, 1000);
   

    mr_firstSectionHeight = $('.main-container section:nth-of-type(1)').outerHeight(true);


}); 
function updateNav() {

    var scrollY = mr_scrollTop;

    if (scrollY <= 0) {
        if (mr_navFixed) {
            mr_navFixed = false;
            mr_nav.removeClass('fixed');
        }
        if (mr_outOfSight) {
            mr_outOfSight = false;
            mr_nav.removeClass('outOfSight');
        }
        if (mr_navScrolled) {
            mr_navScrolled = false;
            mr_nav.removeClass('scrolled');
        }
        return;
    }

    if (scrollY > mr_navOuterHeight + mr_fixedAt) {
        if (!mr_navScrolled) {
            mr_nav.addClass('scrolled');
            mr_navScrolled = true;
            return;
        }
    } else {
        if (scrollY > mr_navOuterHeight) {
           if (!mr_navFixed) {
                mr_nav.addClass('fixed');
                mr_navFixed = true;
            }

            if (scrollY > mr_navOuterHeight +10) {
                if (!mr_outOfSight) {
                    mr_nav.addClass('outOfSight');
                    mr_outOfSight = true;
                }
            } else {
                if (mr_outOfSight) {
                    mr_outOfSight = false;
                    mr_nav.removeClass('outOfSight');
                }
            }
        } else {
            if (mr_navFixed) {
                mr_navFixed = false;
                mr_nav.removeClass('fixed');
            }
            if (mr_outOfSight) {
                mr_outOfSight = false;
                mr_nav.removeClass('outOfSight');
            }
        }

        if (mr_navScrolled) {
            mr_navScrolled = false;
            mr_nav.removeClass('scrolled');
        }

    }
}


function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function initializeMasonry(){
    $('.masonry').each(function(){
        var container = $(this).get(0);
        var msnry = new Masonry(container, {
            itemSelector: '.masonry-item'
        });

        msnry.on('layoutComplete', function() {

            mr_firstSectionHeight = $('.main-container section:nth-of-type(1)').outerHeight(true);

            // Fix floating project filters to bottom of projects container

            if ($('.filters.floating').length) {
                setupFloatingProjectFilters();
                updateFloatingFilters();
                window.addEventListener("scroll", updateFloatingFilters, false);
            }

            $('.masonry').addClass('fadeIn');
            $('.masonry-loader').addClass('fadeOut');
            if ($('.masonryFlyIn').length) {
                masonryFlyIn();
            }
        });

        msnry.layout();
    });
}

function masonryFlyIn() {
    var $items = $('.masonryFlyIn .masonry-item');
    var time = 0;

    $items.each(function() {
        var item = $(this);
        setTimeout(function() {
            item.addClass('fadeIn');
        }, time);
        time += 170;
    });
}

function setupFloatingProjectFilters() {
    mr_floatingProjectSections = [];
    $('.filters.floating').closest('section').each(function() {
        var section = $(this);

        mr_floatingProjectSections.push({
            section: section.get(0),
            outerHeight: section.outerHeight(),
            elemTop: section.offset().top,
            elemBottom: section.offset().top + section.outerHeight(),
            filters: section.find('.filters.floating'),
            filersHeight: section.find('.filters.floating').outerHeight(true)
        });
    });
}

function updateFloatingFilters() {
    var l = mr_floatingProjectSections.length;
    while (l--) {
        var section = mr_floatingProjectSections[l];

        if ((section.elemTop < mr_scrollTop) && typeof window.mr_variant == "undefined" ) {
            section.filters.css({
                position: 'fixed',
                top: '16px',
                bottom: 'auto'
            });
            if (mr_navScrolled) {
                section.filters.css({
                    transform: 'translate3d(-50%,48px,0)'
                });
            }
            if (mr_scrollTop > (section.elemBottom - 70)) {
                section.filters.css({
                    position: 'absolute',
                    bottom: '16px',
                    top: 'auto'
                });
                section.filters.css({
                    transform: 'translate3d(-50%,0,0)'
                });
            }
        } else {
            section.filters.css({
                position: 'absolute',
                transform: 'translate3d(-50%,0,0)'
            });
        }
    }
}

window.initializeMaps = function(){
    if(typeof google !== "undefined"){
        if(typeof google.maps !== "undefined"){
            $('.map-canvas[data-maps-api-key]').each(function(){
                    var mapInstance   = this,
                        mapJSON       = typeof $(this).attr('data-map-style') !== "undefined" ? $(this).attr('data-map-style'): false,
                        mapStyle      = JSON.parse(mapJSON) || [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}],
                        zoomLevel     = (typeof $(this).attr('data-map-zoom') !== "undefined" && $(this).attr('data-map-zoom') !== "") ? $(this).attr('data-map-zoom') * 1: 17,
                        latlong       = typeof $(this).attr('data-latlong') != "undefined" ? $(this).attr('data-latlong') : false,
                        latitude      = latlong ? 1 *latlong.substr(0, latlong.indexOf(',')) : false,
                        longitude     = latlong ? 1 * latlong.substr(latlong.indexOf(",") + 1) : false,
                        geocoder      = new google.maps.Geocoder(),
                        address       = typeof $(this).attr('data-address') !== "undefined" ? $(this).attr('data-address').split(';'): [""],
                        markerTitle   = "We Are Here",
                        isDraggable = $(document).width() > 766 ? true : false,
                        map, marker, markerImage,
                        mapOptions = {
                            draggable: isDraggable,
                            scrollwheel: false,
                            zoom: zoomLevel,
                            disableDefaultUI: true,
                            styles: mapStyle
                        };

                    if($(this).attr('data-marker-title') != undefined && $(this).attr('data-marker-title') != "" )
                    {
                        markerTitle = $(this).attr('data-marker-title');
                    }

                    if(address != undefined && address[0] != ""){
                            geocoder.geocode( { 'address': address[0].replace('[nomarker]','')}, function(results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {
                                var map = new google.maps.Map(mapInstance, mapOptions); 
                                map.setCenter(results[0].geometry.location);
                                
                                address.forEach(function(address){
                                    var markerGeoCoder;
                                    
                                    markerImage = {url: window.mr_variant == undefined ? 'img/mapmarker.png' : '../img/mapmarker.png', size: new google.maps.Size(50,50), scaledSize: new google.maps.Size(50,50)};
                                    if(/(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)/.test(address) ){
                                        var latlong = address.split(','),
                                        marker = new google.maps.Marker({
                                                        position: { lat: 1*latlong[0], lng: 1*latlong[1] },
                                                        map: map,
                                                        icon: markerImage,
                                                        title: markerTitle,
                                                        optimised: false
                                                    });
                                    }
                                    else if(address.indexOf('[nomarker]') < 0){
                                        markerGeoCoder = new google.maps.Geocoder();
                                        markerGeoCoder.geocode( { 'address': address.replace('[nomarker]','')}, function(results, status) {
                                            if (status == google.maps.GeocoderStatus.OK) {
                                                marker = new google.maps.Marker({
                                                    map: map,
                                                    icon: markerImage,
                                                    title: markerTitle,
                                                    position: results[0].geometry.location,
                                                    optimised: false
                                                });
                                            }
                                        });
                                    }

                                });
                            } else {
                                console.log('There was a problem geocoding the address.');
                            }
                        });
                    }
                    else if(latitude != undefined && latitude != "" && latitude != false && longitude != undefined && longitude != "" && longitude != false ){
                        mapOptions.center   = { lat: latitude, lng: longitude};
                        map = new google.maps.Map(mapInstance, mapOptions); 
                        marker              = new google.maps.Marker({
                                                    position: { lat: latitude, lng: longitude },
                                                    map: map,
                                                    icon: markerImage,
                                                    title: markerTitle
                                                });

                    }

                }); 
        }
    }
}
initializeMaps();

// End of Maps




// Prepare Signup Form - It is used to retrieve form details from an iframe Mail Chimp or Campaign Monitor form.

function prepareSignup(iFrame){
    var form   = jQuery('<form />'),
        div    = jQuery('<div />'),
        action;

    jQuery(div).html(iFrame.attr('srcdoc'));
    action = jQuery(div).find('form').attr('action');



    // Alter action for a Mail Chimp-compatible ajax request url.
    if(/list-manage\.com/.test(action)){
       action = action.replace('/post?', '/post-json?') + "&c=?";
       if(action.substr(0,2) == "//"){
           action = 'http:' + action;
       }
    }

    // Alter action for a Campaign Monitor-compatible ajax request url.
    if(/createsend\.com/.test(action)){
       action = action + '?callback=?';
    }


    // Set action on the form
    form.attr('action', action);

    // Clone form input fields from 
    jQuery(div).find('input, select, textarea').not('input[type="submit"]').each(function(){
        jQuery(this).clone().appendTo(form);

    });

    return form;
        

}



/*\
|*|  COOKIE LIBRARY THANKS TO MDN
|*|
|*|  A complete cookies reader/writer framework with full unicode support.
|*|
|*|  Revision #1 - September 4, 2014
|*|
|*|  https://developer.mozilla.org/en-US/docs/Web/API/document.cookie
|*|  https://developer.mozilla.org/User:fusionchess
|*|
|*|  This framework is released under the GNU Public License, version 3 or later.
|*|  http://www.gnu.org/licenses/gpl-3.0-standalone.html
|*|
|*|  Syntaxes:
|*|
|*|  * mr_cookies.setItem(name, value[, end[, path[, domain[, secure]]]])
|*|  * mr_cookies.getItem(name)
|*|  * mr_cookies.removeItem(name[, path[, domain]])
|*|  * mr_cookies.hasItem(name)
|*|  * mr_cookies.keys()
|*|
\*/

var mr_cookies = {
  getItem: function (sKey) {
    if (!sKey) { return null; }
    return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
  },
  setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
    if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
    var sExpires = "";
    if (vEnd) {
      switch (vEnd.constructor) {
        case Number:
          sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
          break;
        case String:
          sExpires = "; expires=" + vEnd;
          break;
        case Date:
          sExpires = "; expires=" + vEnd.toUTCString();
          break;
      }
    }
    document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
    return true;
  },
  removeItem: function (sKey, sPath, sDomain) {
    if (!this.hasItem(sKey)) { return false; }
    document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
    return true;
  },
  hasItem: function (sKey) {
    if (!sKey) { return false; }
    return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
  },
  keys: function () {
    var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
    for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
    return aKeys;
  }
};

/*\
|*|  END COOKIE LIBRARY
\*/

/**
 * Bootstrap EventListeners for touch events
 * in app
 */
var touchBootStrap = function() {
    document.addEventListener("DOMContentLoaded", startup);
};

/**
 * Tracks the touches on the device's surface
 */
var ongoingTouches = [];

/**
 * 
 */
var eventDetails = {};

/**
 * Register EventListeners for Touch related
 * events
 */
function startup() {
    let scheduleCal = document.querySelectorAll('ul.schedule_picker li');
    if (scheduleCal != null) {
        scheduleCal.forEach(function(li) {
            li.addEventListener('touchstart',  scheduleSelectionStart,  false);
        });
    }
}

/**
 * Handle schedule selections from touchstart
 * @param {Event}} e 
 */
function scheduleSelectionStart(e) {
    e.preventDefault();
    let touches = e.changedTouches;

    document.body.addEventListener('touchend',    scheduleSelectionEnd,    false);
    document.body.addEventListener('touchmove',   scheduleSelectionMove,   false);
    document.body.addEventListener('touchcancel', scheduleSelectionCancel, false);
    
    if(e.target.classList.contains('hover_effect')) {
        e.target.classList.remove('hover_effect');
    } else {
        e.target.classList.add('hover_effect');
        eventDetails.start = e.target.dataset.timeBeginMarker;
        let eventStartSelector = document.getElementById('scheduleStart');
        for(let i = 0; i < eventStartSelector.options.length; i++) {
            if (eventStartSelector.options[i].value == eventDetails.start) eventStartSelector.selectedIndex = i;
        }
    }
    
}

/**
 * Handles schedule selections touchend
 * @param {Event} e 
 */
function scheduleSelectionEnd(e) {
    e.preventDefault();
    let touches = e.changedTouches;

    if (e.target.classList.contains('hover_effect')) {
        eventDetails.end = e.target.dataset.timeEndMarker;
        let eventEndSelector = document.getElementById('scheduleEnd');
        for(let i = 0; i < eventEndSelector.options.length; i++) {
            if (eventEndSelector.options[i].value == eventDetails.end) eventEndSelector.selectedIndex = i;
        }
    }
}

/**
 * Handles schedule selections touchmove
 * @param {Event} e 
 */
function scheduleSelectionMove(e) {
    e.preventDefault();

    if(e.target.classList.contains('hover_effect')) {
        e.target.classList.remove('hover_effect');
    } else {
        e.target.class.add('hover_effect')
    }
}

/**
 * Handles schedule selections touchcancel events
 * @param {Event} e 
 */
function scheduleSelectionCancel(e) {
    e.preventDefault();

}

/**
 * next_fieldset()
 * 
 * Move to next form fieldset
 * 
 * @param {HTMLElement} event
 * @returns void
 */ 
 var next_fieldset = function(event) {
	let target = event.currentTarget || event.target;
	while(target.tagName != "FIELDSET") {
		target = target.parentElement;
	}
	if (target.nextElementSibling != null) 
	{
		let next = target.nextElementSibling;
		target.style.transition = "0.5s";
		target.style.display = "none";
		target.classList.remove('active');
		document.getElementById(target.dataset.target).classList.remove("active");
		next.style.transition = "0.5s";
		next.style.display = "block";
		next.classList.add('active');
		document.getElementById(next.dataset.target).classList.add("active");
        
        // Disable buttons in fieldsets which have class fields-check
        // set on their NEXT button's class to ensure user
        // completes fields in fieldset before 
        // progressing
        if(next.querySelector('button.btn.btn-next.fields-check')) {
            next.querySelector('button.btn.btn-next.fields-check').setAttribute('disabled', 'disabled');
        }
	}
	else return;
}

/**
 * previous_fieldset()
 * 
 * Move to previous form fieldset
 * 
 * @param HTMLElement
 * @returns void
 */ 
var previous_fieldset = function(event) {
	let target = event.currentTarget || event.target;
	while(target.tagName != "FIELDSET") {
		target = target.parentElement;
	}
	if(target.previousElementSibling != null) {
		let previous = target.previousElementSibling;
		target.style.transition = "0.5s";
		target.style.display = "none";
		target.classList.remove('active');
		document.getElementById(target.dataset.target).classList.remove("active");
		previous.style.transition = "0.5s";
		previous.style.display = "block";
		previous.classList.add('active');
		document.getElementById(previous.dataset.target).classList.add("active");
	}else return;
	
}

/**
 * formNav
 * 
 * Adds Navigation to form buttons
 */
 var formNav = function() {
	let btnNext = document.querySelectorAll("button.btn-next");
	let btnPrev = document.querySelectorAll("button.btn-prev");

	for(let i = 0; i < btnNext.length; i++) {
		btnNext[i].addEventListener('click', (e)=>{next_fieldset(e);});
	}

	for(let j = 0; j < btnPrev.length; j++) {
		btnPrev[j].addEventListener('click', (e)=>{previous_fieldset(e);});
	}
}

/**
 * fieldsetComplete
 * 
 * Confirm all fields in fieldset have been 
 * filled, so the next button can be
 * enabled
 */
var fieldsetComplete = function() {
    // let input = document.getElementsByTagName("input");
    // if(input != null) {
    //     for(let i = 0; i < input.length; i++) {
    //         input[i].addEventListener('change', function(e){ checkFieldset(e);})
    //     }
    // }
    let fieldsets = document.getElementsByClassName('ih-confirm');
    let inputs, selects;
    forEach(fieldsets, function(fieldset){
        inputs = fieldset.getElementsByTagName("input")
        selects = fieldset.getElementsByTagName("select");

        forEach(inputs, (input) => {
            input.addEventListener('change', checkOtherSisterFields);
        })

        forEach(selects, (select) => {
            select.addEventListener('change', checkOtherSisterFields);
        });

    })
}

/**
 * Disables or enables the Next button on fieldsets
 * until all fields have been filled
 * 
 */
function checkOtherSisterFields() {
    let target = this;
    let fieldset;

    while(target.tagName != "FIELDSET") {
        target = target.parentElement;
    }

    if(target.style.display != "none") {
        fieldset = target;
        let fields = fieldset.getElementsByTagName('input');
        let selects = fieldset.getElementsByTagName('select');
        for(let j = 0; j < fields.length; j++) {
            if(fields[j].value == "") {
                fieldset.querySelector('button.btn.btn-next.fields-check').setAttribute('disabled','disabled');
                fieldset.querySelector(".ih-field-check-notify").textContent = "Please fill all form fields to progress";
                return;
            }
        }
        for(let k = 0; k < selects.length; k++) {
            if(selects[k].value == "") {
                fieldset.querySelector('button.btn.btn-next.fields-check').setAttribute('disabled','disabled');
                fieldset.querySelector(".ih-field-check-notify").textContent = "Please fill all form fields to progress";
                return;
            }
        }
        fieldset.querySelector(".ih-field-check-notify").style.display = "none";
        fieldset.querySelector('button.btn.btn-next.fields-check').removeAttribute('disabled');
    } else {
        return;
    }
}

/**
 * Array of the list of months
 */
const monthsList = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

/**
 * Adds days to date object
 * @param {*} days 
 * @returns Date
 */
Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

/**
 * Initialize date fields for forms
 * @returns void
 */
var initDate = function() {

	let month = document.querySelectorAll('select.ih-date-picker.ih-date-month');
	let day = document.querySelectorAll('select.ih-date-picker.ih-date-day');
	let year = document.querySelectorAll('select.ih-date-picker.ih-date-year');
	let monthOpt = null;
	let yearOpt = null;
	let initYear = new Date().getFullYear();
    let initMonth = new Date().getMonth();
    let exclusiveUrls = [/subscription\/confirmation/, /room\/confirmation/];

    // For exclusive situations where we need to start
    // from the 1st Month of the year
    exclusiveUrls.forEach(function(url) {
        if (window.location.href.match(url)) {
            initMonth = 0;
            initYear = 1970;
        }
    });

    for(let i = 0; i < month.length; i++) {
		for(let j = initMonth; j < monthsList.length; j++) {
			monthOpt = document.createElement("option");
			monthOpt.value = monthsList[j];
			monthOpt.text = monthsList[j];
			month[i].add(monthOpt);
		}
	}

	for(let m = 0; m < year.length; m++) {
		for(let n = initYear; n <= parseInt(new Date().getFullYear()) + 1; n++) {
			yearOpt = document.createElement("option");
			yearOpt.value = n;
			yearOpt.text = n.toString();
			year[m].add(yearOpt);
		}
	}

    for(let k = 0; k < day.length; k++) {
        updateDayLength(day[k].form.year, day[k].form.day, day[k].form.month);
	}
}


/**
 * adjustDaysOfMonth
 * 
 * Adjusts the days of the month depending
 * on the month selected and if it is a 
 * leap year for February
 * @returns void
 */
var adjustDaysOfMonth = function() {
	let month = document.querySelectorAll('select.ih-date-picker.ih-date-month');
    let year  = document.querySelectorAll('select.ih-date-picker.ih-date-year');

	month.forEach((item)=>{
		item.addEventListener('change', function(e) {
			let dateForm = item.form;
			let yr = dateForm.year;
			let day = dateForm.day;

            updateDayLength(yr, day, item);

            if(window.location.href.match(/room/)) return;
            
            if(!window.location.href.match(/confirmation/)) {
                updateDateString();
            }
		});
	});

    if (window.location.href.match(/subscription\/confirmation/) || window.location.href.match(/room\/confirmation/)) {
        year.forEach((yr)=>{
            yr.addEventListener('change', function(e) {
                let dateForm = yr.form;
                let month = dateForm.month;
                let day = dateForm.day;
    
                updateDayLength(yr, day, month);
    
                if(window.location.href.match(/room/)) return;
                // if(!window.location.href.search(/confirmation/)) {
                //     updateDateString();
                // }
            })
        });
    }
};

/**
 * updateDayLength
 * 
 * Update the length of the day for different month values
 * 
 * @param {HTMLSelectElement} yr 
 * @param {HTMLSelectElement} day 
 * @param {HTMLSelectElement} month 
 * @returns void
 */
var updateDayLength = function(yr, day, month) {
    let year = null;
    let dayOpt = null;
    let numDays = 31;
    
    if(yr.name == "year") {
        let chosenYear = yr.selectedOptions[0].label;
        year = parseInt(chosenYear);
    }

    if(month.value == monthsList[8] || month.value == monthsList[3] || month.value == monthsList[5] || month.value == monthsList[10]) {
        numDays = 30;
    }
    else
    {
        if( (month.value == monthsList[1]) && (( (year % 4 == 0) && (year % 100 != 0) ) || (year % 400 == 0))) {
            numDays = 29;
        } else {
            if(month.value == monthsList[1]) numDays = 28;
        }
    }
    
    switch(numDays) {
        case 28:
        case 29:
        case 30:
        case 31:
            while(day.options.length != 0) day.remove(day.options[day.options.length - 1]);
            
            let initDay;
            (month.value == monthsList[new Date().getMonth()]) ? initDay = new Date().getDate() : initDay = 1;
            dayOpt = document.createElement("option");
            dayOpt.text = '--Select a Day--';
            day.add(dayOpt);
            for(let j = initDay; j <= numDays; j++) {
                dayOpt = document.createElement("option");
                dayOpt.value = j;
                dayOpt.text = j.toString() + " " + new Date(year.toString(), monthsList.indexOf(month.value), j.toString()).toDateString().split(" ")[0];
                day.add(dayOpt);
            }
            day.selectedIndex = 0;
            break;
        default:
            break;			
    }
}

/** 
 * Updates date fields which are hidden
 * 
 * @param {HTMLInputElement} hiddenField
 * @param {*} day
 * @param {*} month
 * @param {*} year
 * @returns void
 */
var updateHiddenDateField = function(hiddenField, day, month, year) {
	let date = String(year) + "-" + ((String(month + 1).length < 2) ? "0":"") + String(month + 1) + "-" + ((String(day).length < 2) ? "0":"") + String(day);
	hiddenField.value = date;
	return;
};

/**
 * For profile completion to update
 * @returns void
 */
var captureHiddenDateFields = function() {
    let formsWithHiddenDates = ['profileCompletion'];
    // let day, month, year;
    
    formsWithHiddenDates.forEach(function (form) {
        if (document.forms[form] != null) {
            document.forms[form].day.addEventListener('change', hD);
            document.forms[form].day.addEventListener('click', hD);

            document.forms[form].month.addEventListener('change', hD);
            document.forms[form].month.addEventListener('click', hD);

            document.forms[form].year.addEventListener('change', hD);
            document.forms[form].year.addEventListener('click', hD);
        }
    });

    return;
};

/**
 * Updates a hidden date field in the form
 * @returns void
 */
function hD() {
    let day   = document.forms.profileCompletion.day.value;
    let month = document.forms.profileCompletion.month;
    let year  = document.forms.profileCompletion.year.value;

    month = monthsList.indexOf(month.value)

    let date = String(year) + "-" + ((String(month + 1).length < 2) ? "0":"") + String(month + 1) + "-" + ((String(day).length < 2) ? "0":"") + String(day);
    let cases = ['hiddenDate', 'birth_date'];

    cases.forEach(function(item){
        if (document.forms.profileCompletion[item] != null) {
            document.forms.profileCompletion[item].value = date;
        }
    });

    return;
}

/**
 * Displays date and duration fields for subscriptions
 * @returns void
 */
var displayDateAndDuration = function() {
    if(document.forms.subscription) {
        let form = document.forms.subscription;
        
        form.day.addEventListener('change', updateDateString);
        form.year.addEventListener('change', updateDateString);
    }
}

/**
 * Updates the presentation of the date for the subscriptions
 * @returns void
 */
function updateDateString() {
    let toBeHidden = document.getElementById('hide_schedule');
    let showSchedule = document.getElementById('show_schedule');
    let form = document.forms.subscription;

    if (form.day.selectedIndex == 0) {
        toBeHidden.style.display = 'block';
        showSchedule.style.display = 'none';
        document.getElementById('notice').style.display = "none";
        document.getElementById('schedule_confirmation').classList.add('disabled');
        return
    }

    let subscriptionBegin = new Date(form.year.value, monthsList.indexOf(form.month.value), form.day.value);
    // This lets us know if the starting day is a Sunday
    // and enables us to change the starting date to 
    // a Monday and send the user a notification
    let adjustmentNotification;
    let tenure = parseInt(form.tenure.value);
    let subscriptionBeginStr;
    let subscriptionEndStr;

    if(subscriptionBegin.getDay() == 0) {
        // If the starting day of the subscription
        // is a Sunday then add 1 day to make it a
        // Monday
        subscriptionBegin = subscriptionBegin.addDays(1);
        adjustmentNotification = 1;
    }

    if(tenure != 1) {
        let subscriptionEnd = new Date();
        if (adjustmentNotification == 1) {
            subscriptionEnd = subscriptionBegin.addDays(tenure - 1);
            document.getElementById('notice').innerHTML = "Currently we do not operate on <b>Sundays</b>, hence your scheduled start date will be adjusted to be on <b>Monday</b>";
            document.getElementById('notice').style.display = "block";
        } else {
            subscriptionEnd = subscriptionBegin.addDays(tenure - 1);
            document.getElementById('notice').style.display = "none";

            // Check for weekly use and ensure that Saturdays & Sundays
            // don't get counted as days used
            if (tenure == 5 && subscriptionBegin.getDay() != 1) {
                switch(subscriptionBegin.getDay()) {
                    case 2:
                        // if a Tuesday is chosen
                        subscriptionEnd = subscriptionBegin.addDays(tenure + 1);
                        document.getElementById('notice').innerHTML = "Weekly subscriptions are available for weekdays only. <b>Saturdays</b> & <b>Sundays</b> are not included.";
                        document.getElementById('notice').style.display = "block";
                        break;
                    case 3:
                        // if a Wednesday is chosen
                        subscriptionEnd = subscriptionBegin.addDays(tenure + 1);
                        document.getElementById('notice').innerHTML = "Weekly subscriptions are available for weekdays only. <b>Saturdays</b> & <b>Sundays</b> are not included.";
                        document.getElementById('notice').style.display = "block";
                        break;
                    case 4:
                        // if a Thursday is chosen
                        subscriptionEnd = subscriptionBegin.addDays(tenure + 1);
                        document.getElementById('notice').innerHTML = "Weekly subscriptions are available for weekdays only. <b>Saturdays</b> & <b>Sundays</b> are not included.";
                        document.getElementById('notice').style.display = "block";
                        break;
                    case 5:
                        // if a Friday is chosen
                        subscriptionEnd = subscriptionBegin.addDays(tenure + 1);
                        document.getElementById('notice').innerHTML = "Weekly subscriptions are available for weekdays only. <b>Saturdays</b> & <b>Sundays</b> are not included.";
                        document.getElementById('notice').style.display = "block";
                        break;
                    case 6:
                        // if a Saturday is chosen
                        subscriptionBegin = subscriptionBegin.addDays(2);
                        subscriptionEnd = subscriptionBegin.addDays(4);
                        document.getElementById('notice').innerHTML = "Weekly subscriptions are available for weekdays only. hence your scheduled start date will be adjusted to be on <b>Monday</b>";
                        document.getElementById('notice').style.display = "block";
                        break;
                }

            }
        } 

        updateHiddenDateField(document.forms.subscription.schedule_start, subscriptionBegin.getDate(), subscriptionBegin.getMonth(), subscriptionBegin.getFullYear());
        updateHiddenDateField(document.forms.subscription.schedule_end, subscriptionEnd.getDate(), subscriptionEnd.getMonth(), subscriptionEnd.getFullYear());

        subscriptionBeginStr = subscriptionBegin.toDateString();
        subscriptionEndStr = subscriptionEnd.toDateString();
        document.getElementById('subscription_start').textContent = subscriptionBeginStr;
        document.getElementById('subscription_end').textContent = subscriptionEndStr;

        let startConfirm = document.getElementsByClassName('subscription_start_confirmation');
        let endConfirm = document.getElementsByClassName('subscription_end_date');

        for(let i = 0; i < startConfirm.length; i++) {
            startConfirm[i].textContent = "Scheduled Start Date " + subscriptionBeginStr;
        }

        for(let j = 0; j < endConfirm.length; j++) {
            endConfirm[j].textContent = "Scheduled End Date " + subscriptionEndStr;
        }

    } else {
        updateHiddenDateField(document.forms.subscription.schedule_start, subscriptionBegin.getDate(), subscriptionBegin.getMonth(), subscriptionBegin.getFullYear());

        subscriptionBeginStr = subscriptionBegin.toDateString();
        document.getElementById('subscription_start').textContent = subscriptionBeginStr;

        let startConfirm = document.getElementsByClassName('subscription_start_confirmation');

        for(let i = 0; i < startConfirm.length; i++) {
            startConfirm[i].textContent = "Scheduled Date " + subscriptionBeginStr;
        }
    }

    toBeHidden.style.display = "none";
    showSchedule.style.display = "block";
    document.getElementById('schedule_confirmation').classList.remove('disabled');

    return;
}

/**
 * Disable additional professions field in confirmation
 * 
 * Ensures the additional professions field is disabled in the confirmation form
 * so a user does not enter multiple professions
 * 
 * @returns void
 */
var confirmationProfession = function() {
    if(document.forms.profileCompletion) {
        let proField = document.forms.profileCompletion.profession;
        proField.addEventListener('change', function(e) {
            if(proField.selectedIndex > 0) {
                document.forms.profileCompletion.profession_other.setAttribute('disabled','disabled');
            } 
            if(proField.selectedIndex == 0) {
                document.forms.profileCompletion.profession_other.removeAttribute('disabled');
            }
        });
    }
};

/**
 * Submits form
 * @returns void
 * @returns bool
 */
var submitForm = function() {
	let submitBtns = document.querySelectorAll('button[type="submit"]');
    submitBtns.forEach(function(submitBtn) {
        if(submitBtn.form.classList.contains('ih-ajax')) {
            submitBtn.addEventListener('click', function(e) {
                $('button span.confirm-failure').hide();
                $('button span.confirm-success').hide();
                e.preventDefault();
                let div = document.createElement('div');
                div.classList.add('form-loading');
                div.setAttribute('disabled','disabled');
                
                submitBtn.appendChild(div);
                submitBtn.querySelector('span.ih-submit-button-text').style.display = "none";
                let Form = submitBtn.form;

                ajaxSubmitData(Form, new FormData(Form), event.target)
                return;
            }, false);
        } else return false;
    });
};

/**
 * Captures events from Modals
 * @returns void
 */
var captureFromModal = function() {
    addCustomEventListener('button[type=submit].btn.btn-submit.ih-modal-btn', 'click', submitFromModal);
}

/**
 * In instanes where data needs to be submitted
 * from a modal not attached to a form, this
 * method is employed to relate the event
 * to the form's submission method
 * @param {Event} event
 * @returns void
 */
var submitFromModal = function(event) {
    let formName = '';
    if (event.target.dataset.target != null) {
        formName = event.target.dataset.target;
    } else {
        formName = event.target.parentElement.dataset.target;
    }
    // let formName = event.target.dataset.target;
    let Form = document.forms[formName];
    $('button span.confirm-failure').hide();
    $('button span.confirm-success').hide();
    
    let div = document.createElement('div');
    div.classList.add('form-loading');
    div.setAttribute('disabled','disabled');
    
    if (event.target.querySelector('span.ih-submit-button-text')) {
        event.target.appendChild(div);
        event.target.querySelector('span.ih-submit-button-text').style.display = "none";
        event.target.setAttribute('disabled', 'disabled');
    } else {
        event.target.parentElement.appendChild(div);
        event.target.parentElement.setAttribute('disabled', 'disabled');
        event.target.style.display = "none";
    }

    if (Form.photo) {
        notifyAboutUpload();
        if (event.target != null) submitFormWithImage(Form, event.target);
        else submitFormWithImage(Form, event.target.parentElement);
    }
    else {
        Form.submit();
    }
}

/**
 * Submits forms with images included
 * @param {HTMLForm} Form 
 * @param {EventTarget} eventTarget
 */
function submitFormWithImage(Form, eventTarget) 
{
    if (Form.photo.files[0].size > 2048) {
        
        let formData = new FormData(Form);
        // Read File
        getImage()

        // Resize file
        .then(resizeImage)

        // Replace current Photo in file
        // with resized photo file
        .then((resized) => {
            let imgFile = fetch(resized)
            .then(response => response.blob())
            .then(blob => {
                const imgFile = new File([blob], 'profile_pic.png', {
                    type: 'image/png'
                });
                
                return imgFile;
            })
            return imgFile;
        })

        .then((imgFile) => {
            formData.delete('photo');
            formData.set('photo', imgFile, 'profile_pic.png')
        })

        setTimeout(ajaxSubmitData, 2000, Form, formData, eventTarget);
    } else { 
        ajaxSubmitData(Form, formData, eventTarget);
    }
}

/**
 * readURL
 * Previews profile image
 * @param {HTMLFormField} input 
 */
 function readURL(input) {
    if (input.files && input.files[0]) {

        // Confirm that the images are PNGs or JPEGs
        switch(input.files[0].type) {
            case "image/jpeg":
            case "image/png" :
                break;
            default :
                notify("Only JPG or PNG image formats are supported at this time.");
                return;
        }

        // if (input.files[0].size > 2048) {
        //     notify("Please upload an image with a maximum size of 2MB.");
        //     return;
        // }

        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+ e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});

/**
 * Retrieve image from filesystem
 * @returns {Promise} resizable
 */
function getImage() {
    return new Promise((resolve) => {
    // Get image as dataUrl
    const image = document.querySelector("#imageUpload").files[0];

    // Holder for image to resize
    let resizable = new Image();
    
    // FileReader to load the image in
    let reader = new FileReader();
    reader.onload = function(e) {
        resizable.src = e.target.result;
        resolve(resizable);
    }
    reader.readAsDataURL(image);
    })
}

/**
 * Resize image using canvas
 * @param {HTMLImageElement} imgToResize 
 * @returns {String} dataUrl
 */
function resizeImage(imgToResize) {
    return new Promise((resolve, reject) => {
        const canvas = document.createElement("canvas");
        const context = canvas.getContext("2d");
    
        const originalWidth = imgToResize.width;
        const originalHeight = imgToResize.height;
    
        // Resize ratio
        let imgRatio = (originalWidth / originalHeight);
    
        const canvasWidth = 500;
        const canvasHeight =  500 / imgRatio;
    
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;
    
        context.drawImage(
            imgToResize,
            0,
            0,
            canvas.width,
            canvas.height
        );
        if (canvas.toDataURL('img/png', 0.6)) {
            resolve(canvas.toDataURL('img/png', 0.6));
        } else {
            reject(notify(`Something happened with the image resize process. Please retry.`));
        }
        
    })
}

/**
 * Notify user about the delay due to
 * the image resizing process
 */
function notifyAboutUpload() {
    setInterval(() => {
        $('.ih-modal-upload-notice').animate({
            display: 'block',
            opacity: "show"
        }, 700, "swing");

        $('.ih-modal-upload-notice').animate({
            display: 'none',
            opacity: "hide"
        }, 700, "swing");
    }, 1400);
}

/**
 * Used to submit asynchronous requests
 * via the jquery ajax method
 * 
 * @param {HTMLFormElement} Form 
 * @param {EventTarget} eventTarget
 * @returns void
 */
var ajaxSubmitData = function(Form, formData = null, eventTarget) {
    if (formData == null) formData = new FormData(Form);

    $.ajax({
        'url' : Form.action,
        'method' : Form.method,
        'data'   : formData,
        'processData' : false,
        'contentType' : false,
        'dataType': 'json',
    })
    
    .done(function(data) {
        if(data['SUCCESS'] == 1) {
            $('span.lds-ripple').hide();
            $('button span.confirm-success').show();

            if (eventTarget.querySelector('span.ih-submit-button-text')) {
                eventTarget.querySelector('span.ih-submit-button-text').style.display = "inline";
            } else {
                eventTarget.parentElement.querySelector('span.ih-submit-button-text').style.display = "inline";
            }
            
            eventTarget.removeAttribute('disabled');
            $('button[type=submit] div.form-loading').remove();

            window.location.href = data['LOCATION'];
        }
        if(data.validation) {
            $('span.lds-ripple').hide();
            $('button span.confirm-failure').show();
        }
    })
    
    .fail(function(errors) {
        $('button[type=submit] div.form-loading').remove();
        $('button span.confirm-failure').show();

        try {
            eventTarget.querySelector('span.ih-submit-button-text').style.display = "none";
            eventTarget.setAttribute('disabled', 'disabled');
        } catch (error) {
            eventTarget.parentElement.setAttribute('disabled', 'disabled');
            eventTarget.style.display = "none";
        }
        // if (eventTarget.querySelector('span.ih-submit-button-text')) {
        //     eventTarget.querySelector('span.ih-submit-button-text').style.display = "none";
        //     eventTarget.setAttribute('disabled', 'disabled');
        // } else {
        //     eventTarget.parentElement.setAttribute('disabled', 'disabled');
        //     eventTarget.style.display = "none";
        // }
        try{
            if (JSON.parse(errors.responseText)) {
                reportAjaxErrors(errors, 400);
            }
        } catch (error) {
            notify(errors.responseText);
        }
        // if (JSON.parse(errors.responseText)) {
        //     reportAjaxErrors(errors, 400);
        // } else {
        //     notify(errors.responseText);
        // }
    })
};


/**
 * Adds event listeners to elements which are not
 * accessible. This is done by bubbling up from
 * the body tag to the target element.
 * 
 * @param {String} selector 
 * @param {WatchEventType} event 
 * @param {EventListener} handler 
 * @returns void
 */
var addCustomEventListener = function (selector, event, handler) {
    let rootElement = document.querySelector('body');
    //since the root element is set to be body for our current dealings
    rootElement.addEventListener(event, function (evt) {
        var targetElement = evt.target;
        while (targetElement != null) {

            if (targetElement.matches(selector + ".ih-submit-button-text") || targetElement.matches(selector)) {
                handler(evt);
                return;
            } 
            targetElement = targetElement.parentElement;
        }
    },true);
}

/**
 * Using Stackable to render tables in a responsive manner
 */
 var resizeTables = function() {
    let tables = document.querySelectorAll('table');
    window.addEventListener('resize', function() {
        adjustTable(tables);
    });

    adjustTable(tables);
};

/**
 * Adjusts Tables in cases where the screen is small or 
 * the window is being resized for mobile viewing
 * @param {HTMLCollection} tables 
 * @returns void
 */
function adjustTable(tables) {
    if (window.innerWidth < 992) {
        tables.forEach(function(table) {
            if (table.classList.contains('stacktable')) {
                $('.stacktable').show();
                $('#' + table.id).hide();
            } else {
                $('#' + table.id).hide();
                $('#' + table.id).stacktable();
            }
        });
        return;
    } else {
        tables.forEach(function(table) {
            if (table.classList.contains('stacktable')) {
                $('.stacktable').hide();
                $('#' + table.id).show();
            } else $('#' + table.id).show();
        });
        return;
    }
}

/**
 * Prepares the form for event scheduling
 * @returns void
 */
var prepRetrieveSchedule = function() {
    let day, month, year, option;
    day     = document.getElementById('intended_day');
    month   = document.getElementById('intended_month');
    year    = document.getElementById('intended_year');
    if (document.getElementById('meeting_schedule_confirmation')) document.getElementById('meeting_schedule_confirmation').classList.add('disabled');

    if(day) {
        option  = document.forms.schedule.room_option.value;
        day.addEventListener('change', (e)=> {retrieveSchedule(e, day, month, year, option)}, false);
        month.addEventListener('change', (e)=> {retrieveSchedule(e, day, month, year, option)}, false);
    } else return;
};

/**
 * Query System for event schedule
 * 
 * @param {Event} e
 * @param {HTMLSelectElement} day
 * @param {HTMLSelectElement} month
 * @param {HTMLSelectElement} year
 * @param {Number} option
 * @returns void
 */
function retrieveSchedule(e, day, month, year, option) {
    let date;
    if(parseInt(day.value) > 0) {
        // Ensure the payment modal button is disabled until the actual schedule
        // begin and end time instances have been provided
        document.getElementById('meeting_schedule_confirmation').classList.add('disabled');
        
        let mnt = monthsList.indexOf(month.value) + 1;
        date = year.value + (( mnt < 10) ? '0' + mnt : mnt) + ((day.value < 10) ? '0' + day.value : day.value);

        $.getJSON('/room/' + option + '/booking/' + date)
        .done(function(response) {

            document.getElementById('ih-subscription-schedule-time').style.display = "block"

            if(response.BEGIN) {
                let scheduleBox = document.querySelector('ul.schedule_picker');
                let markers     = scheduleBox.children;
                
                if(response.BEGIN.length > 0) {         
                    for (let index  = 0; index < response.BEGIN.length; index++) {
                        const begin = response.BEGIN[index].split(" ")[1].split(".")[0];
                        const end   = response.END[index].split(" ")[1].split(".")[0];
                        let within  = 0;

                        for (let i = 0; i < markers.length; i++) {
                            if (begin.match(markers[i].dataset.timeBeginMarker)) {
                                within = 1;
                            }

                            if (within == 1) {
                                markers[i].style.backgroundColor = "#a01010";
                                markers[i].dataset.booked = 1;
                                if (end.match(markers[i].dataset.timeEndMarker)) {
                                    within = 0;
                                }
                            } 
                        }
                    }
                } 
                else {
                    for (let j = 0; j < markers.length; j++) {
                        markers[j].dataset.booked = 0;
                        markers[j].style.backgroundColor = '#3a8e0a';
                    }
                }
                if (document.getElementsByName('scheduleType')[0].value == 'daily') dailyScheduleChecker();
            } 
        })
        .fail(function() {
            notify("There was an issue while checking on the server. Please try again.");
        });

    } else return;
}

/**
 * Display the time span for the schedule indicators
 * @returns void
 */
var scheduleIndicators = function() {
    if(document.forms.schedule) {
        let scheduleBox = document.querySelector('ul.schedule_picker');
        let markers     = scheduleBox.children;
        for(let i = 0; i < markers.length; i++) {
            if ( (i % 2) == 0 ) {
                let startingLabel   = markers[i].dataset.timeBeginMarker;
                let endingLabel     = markers[i].dataset.timeEndMarker;
                markers[i].innerHTML = "<span class='scheduleLabelTop'>" + startingLabel + " - " + endingLabel + "</span>";
            } else {
                let startingLabel   = markers[i].dataset.timeBeginMarker;
                let endingLabel     = markers[i].dataset.timeEndMarker;
                markers[i].innerHTML = "<span class='scheduleLabelBottom'>" + startingLabel + " - " + endingLabel + "</span>";
            }
        }
    }
}

/**
 * Capture the event's schedule from the select element
 * or For daily events, check if any event is set
 * that will block the daily events occurence
 */
var setEventSchedule = function() {
    if (document.forms.schedule && (document.getElementById('scheduleStart') ||
         document.getElementById('scheduleEnd'))) 
    {
        let begin   = document.getElementById('scheduleStart');
        let end     = document.getElementById('scheduleEnd');

        begin.addEventListener('change', setSchedule);
        end.addEventListener('change', setSchedule);
    }
}

/**
 * Sets the value of the select options on the event schedule form
 * @param {Event} e 
 * @returns void
 */
function setSchedule(e) {

    switch (e.target.name) {
        case "scheduleStart":
            if (e.target.selectedIndex > document.forms.schedule.scheduleEnd.selectedIndex) {
                document.forms.schedule.scheduleEnd.selectedIndex = e.target.selectedIndex;
            }
            markScheduleSelection(e.target, document.forms.schedule.scheduleEnd);
            break;
        case "scheduleEnd":
            if (e.target.selectedIndex < document.forms.schedule.scheduleStart.selectedIndex) {
                document.forms.schedule.scheduleStart.selectedIndex = e.target.selectedIndex;
            }
            markScheduleSelection(document.forms.schedule.scheduleStart, e.target);
            break;
        default:
            markScheduleSelection(document.forms.schedule.scheduleStart, document.forms.schedule.scheduleEnd);
            break;
    }
}

/**
 * Checks to see if any hourly event is scheduled and notifies
 * the scheduler if the ability to book an 
 * event is possible or not
 */
function dailyScheduleChecker() {
    let scheduleMarker = document.querySelectorAll('ul.schedule_picker li');
    scheduleMarker.forEach(function(marker) {
        if (marker.dataset.booked == 1) {
            notify("Cannot allocate a slot for the day selected. An event is scheduled to hold on that day in the room. Kindly select another day.");
            return;
        }
    });

    let day         = document.forms.schedule.day.value;
    let year        = document.forms.schedule.year.value;
    const date      = new Date(document.forms.schedule.month.value + ((String(day).length < 2) ? "0":"") + String(day) + ', ' + year);
    let begin       = document.forms.schedule.scheduleStart;
    let end         = document.forms.schedule.scheduleEnd;

    switch (date.getDay()) {
        case 0:
            notify("Dear scheduler, we're sorry to inform you that currently we don't schedule meetings on Sundays. Please choose either a weekday or Saturday (with a shorter window)");
            document.getElementById('meeting_schedule_confirmation').classList.add('disabled');
            document.getElementById('ih-subscription-schedule-time').style.display = 'none';
            return;
        case 6:
            document.forms.schedule.scheduleStart.value = "10:00:00";
            document.forms.schedule.scheduleEnd.value = "17:00:00";
            begin   = document.forms.schedule.scheduleStart;
            end     = document.forms.schedule.scheduleEnd;
            break;
        default:
            document.forms.schedule.scheduleStart.value = "09:00:00";
            document.forms.schedule.scheduleEnd.value = "21:00:00";
            begin   = document.forms.schedule.scheduleStart;
            end     = document.forms.schedule.scheduleEnd;
            break;
    }
    markScheduleSelection(begin, end);
    return;
}

/**
 * Resets all the schedule markers showing
 * scheduled events
 * @param {HTMLListItems} markers 
 */
function resetMarkers(markers) {
    markers.forEach(function(marker) {
        if (marker.dataset.booked != 1) marker.style.backgroundColor = '#3a8e0a';
    })
}

/**
 * Marks schedule selection, updates the form fields
 * for the schedule start and end time instances
 * and also updates the modal with the details
 * @param {HTMLSelectElement} begin 
 * @param {HTMLSelectElement} end
 * @returns void
 */
function markScheduleSelection(begin, end) {
    let scheduleMarker = document.querySelectorAll('ul.schedule_picker li');
    let within = false;
    
    resetMarkers(scheduleMarker);
    // hideHoursUnavailable(begin, end);

    for (let i = 0; i < scheduleMarker.length; i++) {
        const marker = scheduleMarker[i];

        (begin.value == marker.dataset.timeBeginMarker) ? within = true : '';

        if (within == true) {
            if (marker.dataset.booked == 1) {        
                notify("Cannot allocate time for the period selected. Kindly select another period.");
                resetMarkers(scheduleMarker);
                document.getElementById('meeting_schedule_confirmation').classList.contains('disabled') ? ''  : document.getElementById('meeting_schedule_confirmation').classList.add('disabled');;
                return;
            } else {
                marker.style.backgroundColor = '#641b65';
                (end.value == marker.dataset.timeEndMarker) ? within = false : '';
            }
        }
    }
    completeScheduling();
    return;
}

function hideHoursUnavailable(begin, end) {
    let scheduleMarker = document.querySelectorAll('ul.schedule_picker li');
    let within = false;
    let counter = 0;

    for(let i = 0; i < scheduleMarker.length; i++) {
        if ( within == false && scheduleMarker[i].dataset.timeBeginMarker != begin.value ) {
            if (scheduleMarker[i].dataset.timeBeginMarker == begin.value) within = true;
            else scheduleMarker[i].style.display = 'none';
        } else {
            if (scheduleMarker[i].dataset.timeEndMarker == end.value) within = false;
        }
    }

    return;
}

/**
 * Complete the information modal for purchase informaiton
 * @return
 */
function completeScheduling() {
    document.getElementById('meeting_schedule_confirmation').classList.remove('disabled');

    let form        = document.forms.schedule;
    let year        = form.year.value;
    let month       = monthsList.indexOf(form.month.value);
    let day         = form.day.value;
    let unitCost    = form.unitCost.value;
    form.date.value = String(year) + "-" + ((String(month + 1).length < 2) ? "0":"") + String(month + 1) + "-" + ((String(day).length < 2) ? "0":"") + String(day);

    // Confirm if its not a daily meeting (as class meeting_duration will only apply in hourly meeting schedules)
    if (document.querySelector('.meeting_duration') != null) {
        let begin       = document.forms.schedule.scheduleStart;
        let end         = document.forms.schedule.scheduleEnd;
        let timeBegArr  = begin.value.split(':');
        let timeEndArr  = end.value.split(':');
        const beginDate = new Date(form.month.value + ((String(day).length < 2) ? "0":"") + String(day) + ', ' + year);
        const endDate   = new Date(form.month.value + ((String(day).length < 2) ? "0":"") + String(day) + ', ' + year);

        setDisplayTime(beginDate, timeBegArr);
        document.querySelectorAll('.meeting_start_time').forEach(function(elem) {
            elem.textContent = beginDate.toLocaleTimeString('en-NG');
            elem.style.fontWeight = 700;
        });

        setDisplayTime(endDate, timeEndArr);
        document.querySelectorAll('.meeting_end_time').forEach(function(elem) {
            elem.textContent = endDate.toLocaleTimeString('en-NG');
            elem.style.fontWeight = 700;
        });

        let duration    = (endDate.getTime() - beginDate.getTime()) / (1000 * 60 * 60);
        let cost        = new Intl.NumberFormat('en-NG', { style: 'currency', currency: 'NGN'}).format(duration * unitCost);
        form.amount.value     = duration * unitCost * 100;
        document.querySelectorAll('.meeting_duration').forEach(function(elem) {
            elem.textContent = duration;
            elem.style.fontWeight = 700;
        });
        document.querySelectorAll('.meeting_cost').forEach(function(elem) {
            elem.textContent = cost;
            elem.style.fontWeight = 700;
        });
        setOrganizerName(form);
    } else {    
        let cost = new Intl.NumberFormat('en-NG', { style: 'currency', currency: 'NGN' }).format(unitCost);
        document.querySelectorAll('.meeting_cost').forEach(function(elem) {
            elem.textContent = cost;
            elem.style.fontWeight = 700;
        });
        setOrganizerName(form);
    }
    return;
}

/**
 * Sets the display Time for Schedule information
 * @param {Date} date
 * @param {Array} timeArray
 */
function setDisplayTime(date, timeArray)
{
    date.setHours(parseInt(timeArray[0]));
    date.setMinutes(parseInt(timeArray[1]));
    date.setSeconds(parseInt(timeArray[2]));
}

/**
 * Sets the name of the meeting Organizer
 * @param {HTMLFormElement} form 
 */
function setOrganizerName(form) {
    if (document.querySelectorAll(".meeting_organizer") != null) {
        document.querySelectorAll(".meeting_organizer").forEach(function(name) {
            let fname = form.fname.value;
            let lname = form.lname.value;
            name.textContent = fname + " " + lname;
            name.style.fontWeight = 700;
        });
    }
}

/**
 * Validate if string is a JSON parsable string
 * @param {*} strData 
 * @returns boolean
 */
function isJsonObject(strData) {
    try {
        JSON.parse(strData);
    } catch (e) {
        return false;
    }
    return true;
}

/**
 * Deconstructs a JSON error object to extract
 * the error information
 * @param {JQueryXHR} object 
 * @param {String} message 
 * @returns {String} message
 */
function returnJsonError(object, message)
{
    for (const [key, value] of Object.entries(object)) {
        if (key == "errors") {
            message += returnJsonError(value, message)
            continue;
        }
        if (message.includes(value)) continue;
        message += '<div style="display:block;">' + value + '</div><br/>';
    }
    return message
}

/**
 * Bring up the notifiction modal to pass messages
 * to the user on issues or updates
 * @param {String} message
 * @returns void
 */
function notify(message) {
    let notificationBtn = document.getElementById('notificationModalBtn');
    document.querySelectorAll('.notificationMessage').forEach(function(eachOne) {
        eachOne.innerHTML = message;
    });
    notificationBtn.click();
    return;
}

/**
 * Based on different scenarios this function will be 
 * used to load modals dynamically from the
 * server via ajax GET requests
 * @param {String} contentName
 * @returns void
 */
function loadDynamicModal(contentName) {
    const loadBtn = document.getElementById('dynamicModalBtn');
    // based on contentName do a query to the server
    // retrieve the HTML Content and load
    // into the modal
    // then click the button programmatically
}

/**
 * Brings up modal to report on application errors from Server
 * @param {JQueryXHR} errors 
 * @param {Number} time
 */
function reportAjaxErrors(errors, time = 400) {
    if (document.querySelector(".foundry_modal.reveal-modal") != null) document.querySelector(".foundry_modal.reveal-modal").classList.remove('reveal-modal');
        
    if (document.querySelector(".foundry_modal.reveal_modal") == null) {
        setTimeout(() => {
            let message = returnJsonError(errors.responseText, '');
            notify(message + '<div style="display:block;">Please refresh and try again</div><br/>');
        }, time);
    }
}

/**
 * Bring up the LoginModal in subscribe view
 * when user states that they have an 
 * existing account
 * @returns void
 */
var loginModal = function() {
    let checkbox = document.getElementById('loginConfirm');

    if (checkbox != null) {
        checkbox.addEventListener('click', function(e) {
            document.getElementById('loginModalBtn').click();
        });
    }
};

/**
 * Used to bring up Legal Contract
 */
$(document).ready(function() {
    if(window.location.href.match(/room\/\d/) || window.location.href.match(/subscription\/\d/)) {
        
        if(document.forms.schedule || document.forms.subscription) {
            $.get('/legal/terms', function(data) {
                let touContainer = document.querySelectorAll('.ih-terms-content');
                touContainer.forEach(function(element) {
                    element.innerHTML = data;
                });
            })
        }

        let acceptanceCheckbox = document.querySelectorAll('.ih-terms-acceptance .accept-terms');

        acceptanceCheckbox.forEach(function(checkbox) {
            checkbox.onclick = function() {
                document.querySelectorAll('.ih-modal-text-content.ih-terms').forEach(function(modalContent) { 
                    modalContent.style.display = "none";
                });

                document.querySelectorAll('.ih-modal-confirmation').forEach(function(modalContent) {
                    modalContent.style.display = "block";
                });
            };
        });
    }
});

/**
 * Error Log instance confirmation
 */
var hasLoggedOnce = false;

window.onerror = function(eventOrMessage, url, lineNumber, colNumber, error) {
    if (hasLoggedOnce || !eventOrMessage) {
        // It does not make sense to report an error if:
        // 1. another one has already been reported -- the page has an invalid state and may produce way too many errors.
        // 2. the provided information does not make sense (!eventOrMessage -- the browser didn't supply information for some reason.)
        return;
    }
    hasLoggedOnce = true;
    if (typeof eventOrMessage !== 'string') {
        error = eventOrMessage.error;
        url = eventOrMessage.filename || eventOrMessage.fileName;
        lineNumber = eventOrMessage.lineno || eventOrMessage.lineNumber;
        colNumber = eventOrMessage.colno || eventOrMessage.columnNumber;
        eventOrMessage = eventOrMessage.message || eventOrMessage.name || error.message || error.name;
    }
    if (error && error.stack) {
        eventOrMessage = [eventOrMessage, '; Stack: ', error.stack, '.'].join('');
    }
    $.ajax({
        'type' : 'GET',
        'url' : '/errors/frontend',
        'data' : {
            'msg'   : eventOrMessage,
            'url'   : url,
            'lineNo': lineNumber,
            'columnNo': colNumber,
            'error' : JSON.stringify(error)
        },
        'contentType': 'application/json',
        success: function() {
            if (console && console.log) {
                console.log('JS error reported.');
            }
        },
        error: function() {
            if (console && console.error) {
                console.error('JS error report submission failed!');
            }
        }
    });

    // Prevent firing of default error handler.
    return true;
}

formNav();
initDate();
submitForm();
loginModal();
resizeTables();
touchBootStrap();
setEventSchedule();
captureFromModal();
fieldsetComplete();
adjustDaysOfMonth();
scheduleIndicators();
prepRetrieveSchedule();
confirmationProfession();
displayDateAndDuration();
captureHiddenDateFields();