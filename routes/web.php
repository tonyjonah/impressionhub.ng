<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ErrorLogger;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LegalController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\RoomOptionController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\RoomScheduleController;
use App\Http\Controllers\SubscriptionPlanController;
// use App\Http\Controllers\SubscriptionPlanController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/about', function () {
    return view('about');
});
// Route::get('/register', function () {
//     return view('register');
// })->name('register')->middleware('guest');
Route::get('/login', function () {
    return view('login');
})->name('login')->middleware('guest');
Route::get('/contact', function () {
    return view('contact');
});

Route::get('/faq', function () {
    return view('faq');
});
Route::get('/events', function () {
    return view('event-listing');
});
Route::get('/coming', function () {
    return view('coming-soon');
});
Route::get('/showcase', function () {
    return view('showcase');
});

Route::get('/legal/terms', [LegalController::class, 'getAgreement']);

Route::get('/errors/frontend', [ErrorLogger::class, 'getFromBrowser']);

Route::get('/services', [ServiceController::class, 'index']);

Route::get('/user/profile', [ProfileController::class, 'profile'])->name('viewProfile')->middleware('auth');
Route::get('/user/services', [ProfileController::class, 'accountInfo'])->name('serviceListing')->middleware('auth');
Route::get('/user/profile/create', [ProfileController::class, 'showForm'])->middleware('auth');
Route::put('/user/profile', [ProfileController::class, 'updateProfile'])->middleware('auth');
Route::post('/user/profile/complete', [ProfileController::class, 'completeProfile'])
    ->name('completeProfile')->middleware('auth');

Route::get('/subscription/{subscription}', [SubscriptionController::class, 'create'])->name('subscribe');
Route::post('/subscription/{subscription}', [SubscriptionController::class, 'store'])->name('spaceSubscription');
// Route::get('/subscription/{subscription}/auth', [SubscriptionController::class, 'createForUser'])
//     ->name('createSubForUser')->middleware('auth');
Route::get('/subscription/confirmation/{paymentId}', [SubscriptionController::class, 'subscriptionConfirmed'])
    ->middleware('auth');
Route::post('/subscription/auth/login', [LoginController::class, 'login']);    

Route::get('/room/{option}', [RoomOptionController::class, 'create'])->name('createBooking');
Route::post('/room/{option}', [RoomScheduleController::class, 'store'])->name('storeBooking');
Route::get('/room/confirmation/{paymentId}', [RoomScheduleController::class, 'bookingConfirmed'])
    ->middleware('auth');
Route::get('/room/{option}/booking/{date}', [RoomScheduleController::class, 'show'])->name('retrieveBookingSchedule');
Route::post('/room/auth/login', [LoginController::class, 'login']);

Route::get('/payment/callback', [PaymentController::class, 'handleGatewayCallback']);
Route::get('/payment/event', [PaymentController::class, 'handleGatewayEventNotification']);

Route::post('/contact', [ContactUsController::class, 'store']);
Auth::routes();

Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');


Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/user/services');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');

/** Blog related Routes **/
Route::get('/blog', [ArticleController::class, 'index']);
Route::get('/blog/{slug}', [ArticleController::class, 'show']);